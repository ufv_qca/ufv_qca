//////////////////////////////////////////////////////////
// qlyt2qca                                          	//
// Copyright 2017 Frank Sill Torres			//
// Email: franksill@ufmg.br	                        //
//////////////////////////////////////////////////////////
// Contents:                                            //
//                                                      //
// header file for Parser 				//
//                                                      //
//////////////////////////////////////////////////////////


#ifndef _FILEIO_H_
#define _FILEIO_H_

static char *HEADER = "[VERSION]\nqcadesigner_version=2.000000\n[#VERSION]\n[TYPE:DESIGN]\n[TYPE:QCADLayer]\ntype=3\nstatus=1\npszDescription=Drawing Layer\n[#TYPE:QCADLayer]\n[TYPE:QCADLayer]\ntype=0\nstatus=1\npszDescription=Substrate\n[TYPE:QCADSubstrate]\n[TYPE:QCADStretchyObject]\n[TYPE:QCADDesignObject]\nx=3000.000000\ny=1500.000000\nbSelected=FALSE\nclr.red=65535\nclr.green=65535\nclr.blue=65535\nbounding_box.xWorld=0.000000\nbounding_box.yWorld=0.000000\nbounding_box.cxWorld=6000.000000\nbounding_box.cyWorld=3000.000000\n[#TYPE:QCADDesignObject]\n[#TYPE:QCADStretchyObject]\ngrid_spacing=20.000000\n[#TYPE:QCADSubstrate]\n[#TYPE:QCADLayer]\n";

static char* concat(const char *s1, const char *s2);
static void read_cell(FILE *qlytfile, FILE *qcafile);
static char* readline(FILE *qlytfile);

/////////////////////////////////////////////////////////////////////////////////////
#endif /* _FILEIO_H_ */


