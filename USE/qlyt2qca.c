//////////////////////////////////////////////////////////
// qlyt2qca                                          	//
// Copyright 2017 Frank Sill Torres			//
// Email: franksill@ufmg.br	                        //
// Version: 1.3 (07/apr/2017)				//
//////////////////////////////////////////////////////////
// Contents:                                            //
//                                                      //
// Parser for qlyt file to qca format			//
//  	                                                //
//                                                      //
//////////////////////////////////////////////////////////
// Comments:					
// 1.3: 
//	- correction of fixed -1/+1 bug
//	- insertion of spin for inputs (impact not sure)
//	- modification of ordering of dot placement (now equal do QCAD)
// 1.2: - removal of case sensitivity

// -- includes -- //
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "qlyt2qca.h"

// -- DEFINES -- //
#define CELL_DISTANCE 20   
#define CELL_SIZE 18   
#define DOT_SIZE 5
#define X_Y_OFFSET 100

// Debug level
#define DEBUG 1
// Variables



// ------ START -------- //
// SYNTAX: qlyt2qca [QLYT FILE]

int main(int argc, char **argv) {
  char *line;
  char buffer[81];
  
  FILE *qlytfile;
  FILE *qcafile;
  char *qcaFName;
  
  qlytfile = NULL;
  qcafile = NULL;
  
  // open files
  if ((qlytfile = fopen (argv[1], "r")) == NULL) {
    printf("%s cann't be open!\n", argv[1]);
    exit(1);
  }
  //adds qca to output file (and removes old .extension
  qcaFName = concat(strtok(argv[1],"."),".qca");
  if (qcaFName == NULL) 
    strcpy(qcaFName,argv[1]);
  if ((qcafile = fopen (qcaFName, "w+")) == NULL) {
    printf("%s cann't be open!\n", qcaFName);
    exit(1);
  }
  
  // write header
  fprintf(qcafile,"%s",HEADER);
  
  // readlines of qlytfil
  while (line = readline(qlytfile)) {
    
    // Layers
    if (strcasestr(line, "[TYPE:QCADLayer]") != NULL)
      fprintf(qcafile,"[TYPE:QCADLayer]\ntype=1\nstatus=0\n");    
    if (strcasestr(line, "[#TYPE:QCADLayer]") != NULL)
      fprintf(qcafile,"[#TYPE:QCADLayer]\n");
    
    if(strcasestr(line, "name=") != NULL) {
      strcpy(buffer, line+5);
      fprintf(qcafile,"pszDescription=%s",buffer);
    }
    
    // Cell
    if (strcasestr(line, "[TYPE:QCADCell]") != NULL)		      
      read_cell(qlytfile,qcafile); 
    
  }
	  
  fprintf(qcafile,"[#TYPE:DESIGN]\n");
  fclose(qlytfile);
  fclose(qcafile);
}

//read QCADCell
static void read_cell(FILE *qlytfile, FILE *qcafile){
  double x=-1;
  double y=-1;
  char *line = NULL;
  char buffer[81];
  int clk_zone = -1;
  char mode[81] = {0};
  char function[81] = {0};
  int red = 0;	   //cell colores
  int green = 0; 
  int blue = 0;
  int fixed = -2; // fixed 0,1 or not fixed?
  int i,j, j2; 
  char name[81] = {0};
  
  fprintf(qcafile,"[TYPE:QCADCell]\n");
  
  //loop until end of QCADCell description is reached
  do {
    if ((line = readline(qlytfile)) == NULL){
      printf("Error: EOF before QCADCell ready.\n");
      exit(1);
    }
    
    //find variables and options
    if (strcasestr(line, "x=") != NULL){
      strcpy(buffer, line+2);
      x = strtod(buffer, NULL) * CELL_DISTANCE + X_Y_OFFSET;      
    }
    if (strcasestr(line, "y=") != NULL){
      strcpy(buffer, line+2);
      y = strtod(buffer, NULL) * CELL_DISTANCE + X_Y_OFFSET;      
    }
    
    if (strcasestr(line, "cell_options.clock=") != NULL) {
	strcpy(buffer, line+19);
	clk_zone = strtol(buffer,NULL,10);
    }
    if (strcasestr(line, "cell_options.mode=") != NULL) 
      strcpy(mode, line+18);
    
    //if input: determine input type (0 or 1)
    if (strcasestr(line, "cell_function=") != NULL) { 
      strcpy(function, line+14);
      if (strcasestr(function, "FIXED")) {
	fixed = function[15] - '0'; //char to int
	if (fixed == 0) 
	   strcpy(name,"-1.00\n");
	else
	  strcpy(name,"1.00\n");
	strcpy(function,"QCAD_CELL_FIXED\n");
      }      
    }
    
     if (strcasestr(line, "cell_name=") != NULL) {
      strcpy(name, line+10);
     }
    
    
  } while (!(strcasestr(line, "[#TYPE:QCADCell]")));
  
  //check all variables
  if ( (x==-1) || (y==-1) || (clk_zone == -1) || (mode[0] == '\0') || (function[0] == '\0')) {
    printf("Missing parameters. Verify: x=%f, y=%f, clk_zone=%d, mode=%s, function=%s\n",x,y,clk_zone,mode,function);
    exit(1);
  }
  
  //write QCADCell header
  fprintf(qcafile,"[TYPE:QCADDesignObject]\n");
  fprintf(qcafile,"x=%f\n",x);
  fprintf(qcafile,"y=%f\n",y);
  fprintf(qcafile,"bSelected=FALSE\n");
  
  //colors
  if (strcasestr(function,"INPUT")) {
    red = 0; green = 0; blue = 65535;}
  else if (strcasestr(function,"OUTPUT")) {
    red = 65535; green = 65535; blue = 0;}
  else if (fixed != -2) {
    red = 65535; green = 32768; blue = 0;}    
  else switch(clk_zone) { 
    case 0: red = 0; green = 65535; blue = 0; break;
    case 1: red = 65535; green = 0; blue = 65535; break;
    case 2: red = 0; green = 65535; blue = 65535; break;
    case 3: red = 65535; green = 65535; blue = 65535; break;
    default: break;
  }
  fprintf(qcafile,"clr.red=%d\nclr.green=%d\nclr.blue=%d\n",red,green,blue);
  
  //bounding boxes
  fprintf(qcafile,"bounding_box.xWorld=%f\n",x-CELL_SIZE/2.0);
  fprintf(qcafile,"bounding_box.yWorld=%f\n",y-CELL_SIZE/2.0);
  fprintf(qcafile,"bounding_box.cxWorld=%f\n",1.0*CELL_SIZE);
  fprintf(qcafile,"bounding_box.cyWorld=%f\n",1.0*CELL_SIZE);
  
  fprintf(qcafile,"[#TYPE:QCADDesignObject]\n");
  
  //options
  fprintf(qcafile,"cell_options.cxCell=%f\n",1.0*CELL_SIZE);
  fprintf(qcafile,"cell_options.cyCell=%f\n",1.0*CELL_SIZE);
  fprintf(qcafile,"cell_options.dot_diameter=%f\n",1.0*DOT_SIZE);
  fprintf(qcafile,"cell_options.clock=%d\n",clk_zone);
  fprintf(qcafile,"cell_options.mode=%s",mode);
  fprintf(qcafile,"cell_function=%s",function);
  fprintf(qcafile,"number_of_dots=4\n");
  
  //write dots
  for (i=1; i>-2;i-=2)
    for (j2=1;j2>-2;j2-=2) {
      
      //j2 help variable to repeat QCAD placing behhavior
      if (i==1)
	j = (-1)*j2;
      else
	j = j2;
      
      fprintf(qcafile,"[TYPE:CELL_DOT]\n");
      fprintf(qcafile,"x=%f\n",x+(CELL_SIZE/4.0)*i);
      fprintf(qcafile,"y=%f\n",y+(CELL_SIZE/4.0)*j);
      fprintf(qcafile,"diameter=%f\n",1.0*DOT_SIZE);
      
      //charge 
      // cell grid is [-1,-1];[1,-1];[1,-1],[1,1] => sum diagonal is always = |2| or [0]
      if (( (fixed==0) && (abs(i+j) == 2) ) || 
	  ( (fixed==1) && (abs(i+j) == 0) ) )
	fprintf(qcafile,"charge=1.602176e-19\n");
      if (( (fixed==0) && (abs(i+j) == 0) ) || 
	  ( (fixed==1) && (abs(i+j) == 2) ) )
	fprintf(qcafile,"charge=0.000000e+00\n");
      if (fixed == -2)
	fprintf(qcafile,"charge=8.010882e-20\n");
      
      if ((strcasestr(function,"INPUT")) || (strcasestr(function,"OUTPUT")))
	fprintf(qcafile,"spin=-319472243083548083355648.000000\n");
      else
	fprintf(qcafile,"spin=0.000000\n");
      
      fprintf(qcafile,"potential=0.000000\n"); 
      fprintf(qcafile,"[#TYPE:CELL_DOT]\n");
    }
    
    // NAME: if input,output or fixed
    if ( (strcasestr(function, "INPUT")) || (strcasestr(function, "OUTPUT")) || (fixed != -2) ) {
      if (name[0] == '\0'){
	printf("Missing pin name. Verify: x=%f, y=%f\n",x,y);
	exit(1);
      }
      
      fprintf(qcafile,"[TYPE:QCADLabel]\n[TYPE:QCADStretchyObject]\n[TYPE:QCADDesignObject]\n");
      fprintf(qcafile,"x=%f\n",x);
      fprintf(qcafile,"x=%f\n",y-21.5);
      fprintf(qcafile,"bSelected=FALSE\n");
      fprintf(qcafile,"clr.red=%d\nclr.green=%d\nclr.blue=%d\n",red,green,blue);
      fprintf(qcafile,"bounding_box.xWorld=%f\n",x-9.0);
      fprintf(qcafile,"bounding_box.yWorld=%f\n",y-33.0);
      fprintf(qcafile,"bounding_box.cxWorld=%f\n",strlen(name)*10.0+4.0);
      fprintf(qcafile,"bounding_box.cyWorld=23.000000\n");
      fprintf(qcafile,"[#TYPE:QCADDesignObject]\n[#TYPE:QCADStretchyObject]\n");
      fprintf(qcafile,"psz=%s",name);
      fprintf(qcafile,"[#TYPE:QCADLabel]\n");
    }//name
    
  fprintf(qcafile,"[#TYPE:QCADCell]\n");

}

//read line and debug
char* readline(FILE *qlytfile) {
  char *line = malloc(82);
  int i;
  char tmp;
  
  if (fgets(line, 82, qlytfile)) {
    if (line[0] == '#')  // ignore comments
      line[0] = '\0';
    
    if (strlen(line) > 2)  //remove ^M
      if (line[strlen(line)-2] == '\r'){
	line[strlen(line)-2] = '\n';
	line[strlen(line)-1] = '\0';	
      }
    for (i=0;i<strlen(line);i++) {
      if(line[i] == '\r') 
	if (DEBUG==2) printf("now %s (length = %d,pos = %d\n",line,strlen(line),i);
    }
      
    if (strlen(line) > 80) {
      printf("Error: line to long (>81):\n%s\n",line);
      exit(1);
    }
    return line;
  } else {
    // EOF
    return NULL;
  }
}

// Concatenate two strings 
static char* concat(const char *s1, const char *s2)
{
    const size_t len1 = strlen(s1);
    const size_t len2 = strlen(s2);
    char *result = malloc(len1+len2+1);//+1 for the zero-terminator
    if (result == NULL) {
      printf("Error: Memory fault at concat\n");
      exit(1);
    } 
    memcpy(result, s1, len1);
    memcpy(result+len1, s2, len2+1);//+1 to copy the null-terminator
    return result;
}

