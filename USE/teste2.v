module top( x, y, z);
input x, y, z;
output A, D , E;
wire B, C;

assign B = ( x & y ) | ( y & z ) | ( x & z );
assign C = ( x & y ) | ( y & z ) | ( x & z );
assign A = ( x & B ) | ( x & C ) | ( B & C );

assign D = ( y & B ) | ( y & z ) | ( z & B );
assign E = ( y & B ) | ( y & z ) | ( z & B );



endmodule