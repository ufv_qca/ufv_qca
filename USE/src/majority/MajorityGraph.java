/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package majority;

import static java.lang.Math.abs;
import java.util.Vector;
import use.Point;

/**
 *
 * @author geraldo
 */
public class MajorityGraph {
    //private majorityNode majorityGraph[];
    
    private String Name;
    private Vector<MajorityNode> majorityGraph;
    private Vector<Integer> levelWeight;
    private int maxLevel;
    
    
    public MajorityGraph(MajorityGraph mig){
        majorityGraph= new Vector<MajorityNode>();
        levelWeight=new Vector<Integer>();
        this.levelWeight.addAll(mig.getLevelWeight());
        this.maxLevel=mig.getMaxLevel();
        
        for(int i=0;i<mig.getMajorityGraph().size();i++)
            this.majorityGraph.add(new MajorityNode(mig.getMajorityGraph().get(i)));
        
    }
    
    public MajorityGraph() {
        majorityGraph= new Vector<MajorityNode>();
        levelWeight=new Vector<Integer>();
    }
    
    public Vector<Integer> getLevelWeight() {
        return levelWeight;
    }

    public void setLevelWeight(Vector<Integer> levelWeight) {
        this.levelWeight = levelWeight;
    }
    
    //adiciona o no somente se ele ainda nao foi adicionado
    public boolean addNode(MajorityNode node) {
        if (getNode(node.getName()) == null) { // se o node nao existe entao adiciona
                majorityGraph.add(node);
                return true;
            }
        return false;
    }
    
    public MajorityNode getNode(String name){
        for(int i=0;i<majorityGraph.size();i++)
            if(majorityGraph.get(i).getName().equals(name))
                return majorityGraph.get(i);
        return null;
    }

    //seta os niveis abaixo do root para todos gates
    public void initGateLevel(String root,int level){
        MajorityNode node = this.getNode(root);
        if(node!=null)
            if(this.getNode(root).getType()!="input"){
                if(level>node.getLevel())
                    node.setLevel(level);
                level++;
                for(int i=0; i<node.getInput().size();i++)
                    initGateLevel(node.getInput().get(i),level);
                level--;
            }
    }
    

    //inicializa leel de todos nos que podem ser alcaçados a partir de inp no sentido in->out
    public void initLevelInToOut(String inp, int levelValue){
         if(this.getNode(inp)!=null){
             if(this.getNode(inp).getLevel()<=levelValue)
                this.getNode(inp).setLevel(levelValue);
             for(int i=0;i<this.getNode(inp).getOutput().size();i++)
                 this.initLevelInToOut(this.getNode(inp).getOutput().get(i), levelValue+1);
         }
    }
    
    
//inicia os leveis de qualquer grafo conexo
    // !!! não detecta grafos desconexos tratar futuramente
    public void initLevel(){
        
        Vector<String> inputs = new Vector<String>();
        boolean b;
        
        for(int i=0; i<this.getMajorityGraph().size();i++){
            b=true;
            for(int j=0; j<this.getMajorityGraph().get(i).getInput().size();j++){
                if( this.getNode(this.getMajorityGraph().get(i).getInput().get(j))!=null ){
                    b=false;
                }
            }
            if(b){
                inputs.add(this.getMajorityGraph().get(i).getName());
            }
        }
        
        for(int i=0; i<this.getMajorityGraph().size();i++)
            this.getMajorityGraph().get(i).setLevel(0);
        
        
        for(int i=0; i<inputs.size();i++)
            initLevelInToOut(inputs.get(i),0);
        
        //atualiza o maxlevel
        for(int i=0;i<this.getMajorityGraph().size();i++){
            if(this.getMajorityGraph().get(i).getLevel()> this.getMaxLevel())
                this.setMaxLevel(this.getMajorityGraph().get(i).getLevel());
        }
        
        for(int i=0; i<this.getMajorityGraph().size();i++)
            this.getMajorityGraph().get(i).setLevel(
                    abs(this.getMajorityGraph().get(i).getLevel() - this.getMaxLevel()));

        for(int i=0; i<this.getMajorityGraph().size();i++)
            this.getMajorityGraph().get(i).setLevel( this.getMajorityGraph().get(i).getLevel()+1 );

        
        //atualiza o maxlevel
        for(int i=0;i<this.getMajorityGraph().size();i++){
            if(this.getMajorityGraph().get(i).getLevel()> this.getMaxLevel())
                this.setMaxLevel(this.getMajorityGraph().get(i).getLevel());
        }
        //inicializa vetor de pesos com 2
        this.levelWeight = new Vector<Integer>();
        for(int i=0;i<this.getMaxLevel()-1;i++)
            this.getLevelWeight().add(2);
        
    }
    
  
    
    //inicia os niveis no grafo unindo todas saidas a um nó artificial Root
    public void setLevel(){
        boolean b;
        for(int i=0; i<this.getMajorityGraph().size();i++)
            this.getMajorityGraph().get(i).setLevel(0);
        
        
        MajorityNode root = new MajorityNode("Root","output");
        for(int i=0;i<this.getMajorityGraph().size();i++){
            b=true;
            for(int j=0;j<this.getMajorityGraph().get(i).getOutput().size();j++){
                if(this.getNode(this.getMajorityGraph().get(i).getOutput().get(j))!=null){
                    b=false;
                    continue;
                }
            }
            if(b){
                this.getMajorityGraph().get(i).addOutput("Root");
                root.addInput(this.getMajorityGraph().get(i).getName());
            }
            
        }
        this.getMajorityGraph().add(root);
        
        this.initLevel("Root", 1);
    
        
        // remove o no R inserido artificialmente
        for(int i=0;i<this.getMajorityGraph().size();i++){
            this.getMajorityGraph().get(i).setLevel(this.getMajorityGraph().get(i).getLevel()-1);
            if(this.getMajorityGraph().get(i).getOutput().contains("Root")){
                this.getMajorityGraph().get(i).getOutput().remove("Root");
            }
        }
        this.setMaxLevel(maxLevel-1);
        this.majorityGraph.remove(root);
        
        
        //atualiza o maxlevel
        this.setMaxLevel(0);
        for(int i=0;i<this.getMajorityGraph().size();i++){
            if(this.getMajorityGraph().get(i).getLevel()> this.getMaxLevel())
                this.setMaxLevel(this.getMajorityGraph().get(i).getLevel());
        }
        //inicializa vetor de pesos com 1
        this.levelWeight = new Vector<Integer>();
        for(int i=0;i<this.getMaxLevel()-1;i++)
            this.getLevelWeight().add(2);
        
    }
     
    //seta os niveis abaixo do root para todos gates
    public void initLevel(String root,int level){
        int maxLevel=0;
        initGateLevel(root,level);
            for(int i=0;i<majorityGraph.size();i++){
                if(majorityGraph.get(i)!=null)
                    if(majorityGraph.get(i).getLevel()>maxLevel)
                        maxLevel=majorityGraph.get(i).getLevel();
            }
        
            for(int i=0;i<majorityGraph.size();i++)
                if(majorityGraph.get(i)!=null)
                    if(majorityGraph.get(i).getType().equals("input"))
                        majorityGraph.get(i).setLevel(maxLevel+1);
            this.setMaxLevel(maxLevel+1);
    }
    
    public void print() {
        
        String s="";
        for (int i = 0; i < majorityGraph.size(); i++) {
            System.out.printf("node: %-6s[%-6s][%-4s](%-2d) :", majorityGraph.get(i).getName(), majorityGraph.get(i).getType(), majorityGraph.get(i).getSubType(),majorityGraph.get(i).getLevel());
            s= s + " input{";
            for (int j = 0; j < majorityGraph.get(i).getInput().size(); j++) {
                if (this.getNode(majorityGraph.get(i).getInput().get(j)) != null) {
                    s=s+this.getNode(majorityGraph.get(i).getInput().get(j)).getName();
                    if(j < majorityGraph.get(i).getInput().size()-1)
                        s=s+",";
                }
            }
            s=s+"}";
            System.out.printf("%-20s",s);
            s="";
            
            
            s=s+" output{";
            for (int j = 0; j < majorityGraph.get(i).getOutput().size(); j++) {
                if (this.getNode(majorityGraph.get(i).getOutput().get(j)) != null) {
                   s=s+this.getNode(majorityGraph.get(i).getOutput().get(j)).getName();
                   if(j < majorityGraph.get(i).getOutput().size()-1)
                       s=s+",";
                }
            }
            s=s+"}";
            System.out.printf("%-20s \n",s);
            s="";
        }
        System.out.println();
    }

    
    public boolean migIsPlaced(){
        for(int j=0;j<this.majorityGraph.size();j++)
            if(!this.getNode(this.getMajorityGraph().get(j).getName()).isPlaced())
                return false;
        return true;
    } 
    
   public int nodesDistance(MajorityNode n1, MajorityNode n2){
       int d=0;
       if(n2==null)
           System.out.println("NULL");
       if(n1.getLevel() < n2.getLevel()){
         for(int n=0;n<(n2.getLevel()-n1.getLevel()) ;n++)
           d+= this.getLevelWeight().get(n1.getLevel()+(n)-1);
       }else{
           for(int n=0;n<(n1.getLevel()-n2.getLevel()) ;n++)
            d+= this.getLevelWeight().get(n2.getLevel()+(n)-1);
       }
       return d;
   }
   
   
   // TRATAR A EXCEPTION DE AS COORDENADAS SAIREM DO FIELD
    public void transmove(int x, int y) {
        
        
        for (int i = 0; i < this.getMajorityGraph().size(); i++) {
            //translada o posicionamento
            this.getMajorityGraph().get(i).setPlaceX(this.getMajorityGraph().get(i).getPlaceX() + x);
            this.getMajorityGraph().get(i).setPlaceY(this.getMajorityGraph().get(i).getPlaceY() + y);
            //translada as rotas atuais
            for (int j = 0; j < this.getMajorityGraph().get(i).getRoutes().size(); j++) {
                for (int k = 0; k < this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size(); k++) {
                    this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).setX(this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX()+x);
                    this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).setY(this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY()+y);
                }
            }
            //translada as rotas alternativas
            for (int j = 0; j < this.getMajorityGraph().get(i).getAllRoutes().size(); j++) {
                for (int k = 0; k < this.getMajorityGraph().get(i).getAllRoutes().get(j).size(); k++) {
                    for (int l = 0; l < this.getMajorityGraph().get(i).getAllRoutes().get(j).get(k).getRoute().size(); l++) {
                        this.getMajorityGraph().get(i).getAllRoutes().get(j).get(k).getRoute().get(l).setX( this.getMajorityGraph().get(i).getAllRoutes().get(j).get(k).getRoute().get(l).getX()+x );
                        this.getMajorityGraph().get(i).getAllRoutes().get(j).get(k).getRoute().get(l).setY( this.getMajorityGraph().get(i).getAllRoutes().get(j).get(k).getRoute().get(l).getY()+y );
                    }
                }
            }
            
        }
        
    }  


    public Vector<MajorityNode> getMajorityGraph() {
        return majorityGraph;
    }

    public void setMajorityGraph(Vector<MajorityNode> majorityGraph) {
        this.majorityGraph = majorityGraph;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

   
    /*public void printRoutes(){
        for(int i=0;i<majorityGraph.size();i++){
            System.out.println(this.getMajorityGraph().get(i).getName()+":");
            for(int j=0;j<this.getMajorityGraph().get(i).getRoutes().size();j++){
                System.out.print("  ");
                System.out.print(this.getMajorityGraph().get(i).getRoutes().get(j).toString());
            }
        }
    }*/
    public void printRoutes(){
        for(int i=0;i<majorityGraph.size();i++){
            System.out.println(this.getMajorityGraph().get(i).getName()+":");
            for(int j=0;j<this.getMajorityGraph().get(i).getRoutes().size();j++){
                System.out.println("    "+this.getMajorityGraph().get(i).getRoutes().get(j).toString()+" ");
            }
        }
    }
    
    
    public Vector<String> nodesInLevel(int level) {
        Vector<String> levelNodes = new Vector<String>();

        for (int i = 0; i < majorityGraph.size(); i++) {
            if (this.getMajorityGraph().get(i).getLevel() == level) {
                levelNodes.add(this.getMajorityGraph().get(i).getName());
            }
        }

        return levelNodes;
    }
    
    public boolean levelIsPlaced(int level){
        boolean b=true;
        
        for (int i = 0; i < majorityGraph.size(); i++) {
            if (this.getMajorityGraph().get(i).getLevel() == level) {
                if(!this.getMajorityGraph().get(i).isPlaced())
                    b=false;
            }
        }
        return b;
    }   
    
    // retorna ponto minimo que limita inferiormente o grafo
    public Point minPoint(){
        int x0 = 0;
        int x1 = 0;
        int y0 = 0;
        int y1 = 0;

 
        if(this.getMajorityGraph()!=null)
            if (this.getMajorityGraph().size() > 0) {
                x0=this.getMajorityGraph().get(0).getPlaceX();
                x1=this.getMajorityGraph().get(0).getPlaceX();
                y0=this.getMajorityGraph().get(0).getPlaceY();
                y1=this.getMajorityGraph().get(0).getPlaceY();
                
                for (int i = 0; i < this.getMajorityGraph().size(); i++) {
                    for(int j=0;j<this.getMajorityGraph().get(i).getRoutes().size();j++){
                        for(int k=0;k<this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size();k++){

                                    if(x0>this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x0=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(x1<this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x1=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(y0>this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y0=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                                    if(y1<this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y1=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                        }
                    }
                }
            }
    
        
    return new Point(x0,y0);
    }
    
    
    // retorna ponto maximo que limita superiormente o grafo
    public Point maxPoint(){
        int x0 = 0;
        int x1 = 0;
        int y0 = 0;
        int y1 = 0;

 
        if(this.getMajorityGraph()!=null)
            if (this.getMajorityGraph().size() > 0) {
                x0=this.getMajorityGraph().get(0).getPlaceX();
                x1=this.getMajorityGraph().get(0).getPlaceX();
                y0=this.getMajorityGraph().get(0).getPlaceY();
                y1=this.getMajorityGraph().get(0).getPlaceY();
                
                for (int i = 0; i < this.getMajorityGraph().size(); i++) {
                    for(int j=0;j<this.getMajorityGraph().get(i).getRoutes().size();j++){
                        for(int k=0;k<this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size();k++){

                                    if(x0>this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x0=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(x1<this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x1=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(y0>this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y0=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                                    if(y1<this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y1=this.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                        }
                    }
                }
            }
    
        
    return new Point(x1,y1);
    }
    
    public int totalArea(){
        return( (this.maxPoint().getX()-this.minPoint().getX()+1)*(this.maxPoint().getY()-this.minPoint().getY()+1) );
    }
    
    //retorna o no posicionado em x,y se existir e null caso contrario 
    public boolean hasNodePlacedIn(Point p){
        for(int i=0;i<this.getMajorityGraph().size();i++){
            if(this.getMajorityGraph().get(i).getPlaceX()==p.getX() && this.getMajorityGraph().get(i).getPlaceY()==p.getY()){
                return true;
            }
        }
        return false;
    }
    
    
    //nodes para os quais existe um caminho a partir do node passado
    public Vector<String> reachableNodes(String node){
        Vector<String> f1= new Vector<String>();
        f1.add(node);
        f1.addAll(this.getNode(node).getOutput());
        for (int m = 0; m < f1.size(); m++) {
            for (int n = 0; n < this.getNode(f1.get(m)).getOutput().size(); n++) {
                if (!f1.contains(this.getNode(f1.get(m)).getOutput().get(n))) {
                    f1.add(this.getNode(f1.get(m)).getOutput().get(n));
                }
            }
        }
        return f1;
    }
    
    //nodes para os quais existe um caminho a partir do node passado
    public Vector<String> reachableNodesOI(String node){
        Vector<String> f1= new Vector<String>();
        f1.add(node);
        f1.addAll(this.getNode(node).getInput());
        for (int m = 0; m < f1.size(); m++) {
            for (int n = 0; n < this.getNode(f1.get(m)).getInput().size(); n++) {
                if (!f1.contains(this.getNode(f1.get(m)).getInput().get(n))) {
                    f1.add(this.getNode(f1.get(m)).getInput().get(n));
                }
            }
        }
        return f1;
    }
    
    public Vector<String> reconvergences() {
        Vector<String> r = new Vector<String>();// guarda os 2 nodes de converngencia e a origem(3a3)
        Vector<String> f1 = new Vector<String>();// todos nodes que podem ser alcancados a partir de um node
        Vector<String> f2 = new Vector<String>();
        Vector<String> reconv = new Vector<String>();

        for (int i = 0; i < this.getMajorityGraph().size(); i++) {
            for (int j = 0; j < this.getMajorityGraph().size(); j++) {
                if (i != j) {
                    for (int m = 0; m < this.getMajorityGraph().get(i).getInput().size(); m++) {
                        if (this.getMajorityGraph().get(j).getInput().contains(this.getMajorityGraph().get(i).getInput().get(m))) {
                            r.add(this.getMajorityGraph().get(i).getName());
                            r.add(this.getMajorityGraph().get(j).getName());
                            r.add(this.getMajorityGraph().get(i).getInput().get(m));
                        }

                    }
                }
            }
        }

        for (int i = 0; i < r.size(); i += 3) {
            f1 = this.reachableNodes(r.get(i));
            f2 = this.reachableNodes(r.get(i+1));
            
            for (int j = 0; j < f1.size(); j++) {
                if (f2.contains(f1.get(j))) {
                    reconv.add(r.get(i + 2));
                    reconv.add(r.get(i));
                    reconv.add(r.get(i + 1));
                    reconv.add(f1.get(j));
                }
            }

        }

        //remove repeticoes
        for (int j = 0; j < reconv.size() - 3; j += 4) {
            for (int k = j + 4; k < reconv.size() - 3; k += 4) {
                if (reconv.get(j).equals(reconv.get(k)) && reconv.get(j + 3).equals(reconv.get(k + 3))) {
                    if ((reconv.get(j + 1).equals(reconv.get(k + 1)) && (reconv.get(j + 2).equals(reconv.get(k + 2)))) || (reconv.get(j + 1).equals(reconv.get(k + 2)) && (reconv.get(j + 2).equals(reconv.get(k + 1))))) {
                        reconv.remove(k + 3);
                        reconv.remove(k + 2);
                        reconv.remove(k + 1);
                        reconv.remove(k);
                    }
                }
            }
        }

        for (int j = 0; j < reconv.size() - 3; j += 4) {
            for (int k = j + 4; k < reconv.size() - 3; k += 4) {
                if (reconv.get(j).equals(reconv.get(k)) && !reconv.get(j + 3).equals(reconv.get(k + 3))) {
                    if ((reconv.get(j + 1).equals(reconv.get(k + 1)) && (reconv.get(j + 2).equals(reconv.get(k + 2)))) || (reconv.get(j + 1).equals(reconv.get(k + 2)) && (reconv.get(j + 2).equals(reconv.get(k + 1))))) {
                        
                        if(this.reachableNodes(reconv.get(k + 3)).contains(reconv.get(j + 3))){
                            reconv.remove(j + 3);
                            reconv.remove(j + 2);
                            reconv.remove(j + 1);
                            reconv.remove(j);
                            j-=4;
                        }else{
                            reconv.remove(k + 3);
                            reconv.remove(k + 2);
                            reconv.remove(k + 1);
                            reconv.remove(k);
                            k-=4;
                        }
                    
                    }
                }
            }
        }
        
        for (int j = 0; j < reconv.size() - 3; j += 4) {
            //System.out.println(reconv.get(j) + "-" + reconv.get(j + 1) + "-" + reconv.get(j + 2) + "-" + reconv.get(j + 3));
        }
        
        return reconv;

    }
    
    public void clear(){
        for(int i=0;i<this.majorityGraph.size();i++){
            this.majorityGraph.get(i).clear();
        }
    }
    
   
    
    
    public void migInfo(){
        int numNodes=0;
        int numInput=0;
        int numOutput=0;
        int numReconv=0;
        int numVert=0;
        int maxFanout=0;
        int[] numNodes4Level = new int[this.getMaxLevel()];

        for(int i=0;i<this.getMaxLevel();i++){
            numNodes4Level[i]=0;
        }
        
        numReconv=this.reconvergences().size()/4;
        
        for(int i=0;i<this.getMajorityGraph().size();i++){
            
            numNodes4Level[this.getMajorityGraph().get(i).getLevel()-1]++;
            numNodes++;
            numVert+=this.getMajorityGraph().get(i).getInput().size()+this.getMajorityGraph().get(i).getOutput().size();
            
            if(this.getMajorityGraph().get(i).getType().equals("input")){
                numInput++;
            }
            if(this.getMajorityGraph().get(i).getType().equals("output")){
                numOutput++;
            }
            if(this.getMajorityGraph().get(i).getOutput().size()>maxFanout){
                maxFanout=this.getMajorityGraph().get(i).getOutput().size();
            }
            
        }
        
        System.out.printf("nodes vertex in out level reconv fanoutMax\n");
        System.out.printf("%-5s %-6s %-2s %-3s %-5s %-6s %-9s\n", numNodes,numVert/2,numInput,numOutput,this.getMaxLevel(),numReconv,maxFanout);
        
        /*for(int i=0;i<this.getMaxLevel();i++){
            System.out.print(numNodes4Level[i]+" ");
        } */   
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
    
    
}