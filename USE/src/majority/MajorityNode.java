/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package majority;

import java.util.Objects;
import java.util.Vector;
import use.Point;
import use.Route;


/**
 *
 * @author geraldo
 */
public class MajorityNode {
    private String subType;
    private String type;
    private String name;
    private Vector<String> input;
    private Vector<String> output;
    private int level;
    private boolean placed;
    private int placeX;
    private int placeY;
    private Vector<Route> routes;
    private Vector<Vector<Route>> allRoutes;

    public MajorityNode(MajorityNode mn) {
        
        input = new Vector<String>(); 
        output = new Vector<String>(); 
        routes = new Vector<Route>(); 
        allRoutes = new Vector<Vector<Route>>(); 
        
        this.type = mn.getType();
        this.subType = mn.getSubType();
        this.name = mn.getName();
        
        for(int i=0;i<mn.output.size();i++)
            this.output.add(mn.output.get(i));
        
        for(int i=0;i<mn.input.size();i++)
            this.input.add(mn.input.get(i));
        
        
        //this.input.addAll(mn.getInput());
        //this.output.addAll(mn.getOutput());
        this.level = mn.getLevel();
        this.placed = mn.isPlaced();
        this.placeX = mn.getPlaceX();
        this.placeY = mn.getPlaceY();
        
        for(int i=0;i<mn.getRoutes().size();i++)
            this.routes.add(new Route(mn.getRoutes().get(i)));
        
        /*
        Vector<Route> aux;
        for(int i=0;i<mn.getAllRoutes().size();i++){
            aux = new Vector<Route>();
            for(int j=0;j<mn.getAllRoutes().get(i).size();j++){
               aux.add( new Route( mn.getAllRoutes().get(i).get(j) ));
               //aux.add( mn.getAllRoutes().get(i).get(j));
            }
            this.allRoutes.add(aux);
        }
        */
    }
 
    public MajorityNode() {
        this.input= new Vector<String>();
        this.output= new Vector<String>();
        this.level=0;
        this.placed = false;
        
    }
    
    public void clear(){
        if(routes!=null)
        routes.clear();
        placed=false;
    }
    
    public MajorityNode(String name, String type) {
        
        this.type = type;
        this.name = name;
        this.input= new Vector<String>();
        this.output= new Vector<String>();
        this.level=0;
        this.placed = false;
        }
    
    public MajorityNode(String name, String type, String subType) {
        
        this.type = type;
        this.subType = subType;
        this.name = name;
        this.input= new Vector<String>();
        this.output= new Vector<String>();
        this.level=0;
        this.placed = false;
        }    
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector<String> getInput() {
        return this.input;
    }

    public void addInput(String inputName) {
        this.input.add(inputName);
    }

    public Vector<String> getOutput() {
        return this.output;
    }

    public void addOutput(String outputName) {
        this.output.add(outputName);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isPlaced() {
        return placed;
    }

    public void setPlaced(boolean placed) {
        this.placed = placed;
    }

    public int getPlaceX() {
        return placeX;
    }

    public void setPlaceX(int placeX) {
        this.placeX = placeX;
    }

    public int getPlaceY() {
        return placeY;
    }

    public void setPlaceY(int placeY) {
        this.placeY = placeY;
    }

    public Vector<Route> getRoutes() {
        if(this.routes!=null)
            return routes;
        else
            return new Vector<Route>();
    }

    public void setRoutes(Vector<Route> routes) {
        this.routes = routes;
    }
    
    public void resetRoutes() {
        this.routes = new Vector<Route>();
    
    }
    
    public boolean addRoute(Route route) {
        if(this.routes==null){
            this.routes=new Vector<Route>();
        }
        //não adiciona rotas repetidas
        //if(!this.hasRoute(route)){
        //   this.routes.add(route);
        //    return true;
        //}
        // não adiciona rotas com origem e desitino iguais
        for(int i=0;i<this.getRoutes().size();i++){
            if(this.getRoutes().get(i).getRoute().get(0).equal(route.getRoute().get(0)) && this.getRoutes().get(i).getRoute().lastElement().equal(route.getRoute().lastElement()) ){
                return false;
            }
        }
        this.routes.add(route);
        return true;
    }

    public Vector<Vector<Route>> getAllRoutes() {
        if(allRoutes!=null)
            return allRoutes;
        else 
            return new Vector<Vector<Route>>();
    }

    public void setAllRoutes(Vector<Vector<Route>> allRoutes) {
        this.allRoutes = allRoutes;
    }
    
    public String toString(){
    
        return this.name+" "+this.type +" "+ this.input.toString()+" "+ this.output.toString()+"\n";
    }
    
    public void removeRedundancyInRoutes(){// remove redundancy points in route

        Point pAux = new Point(0, 0);
        if(this.routes!=null)
        for (int i = 0; i < this.routes.size() - 1; i++) {
            pAux = this.routes.get(i).getRoute().get(0);
            for (int j = i + 1; j < this.routes.size(); j++) {
                if (this.routes.get(j).getRoute().get(0).equal(pAux)) {
                    for (int k = 0; k < this.routes.get(j).getRoute().size()-1; k++) {
                        if (this.routes.get(i).contain(this.routes.get(j).getRoute().get(k))) {
                            if (this.routes.get(i).contain(this.routes.get(j).getRoute().get(k+1)))     
                                if( (this.routes.get(i).indexOf(this.routes.get(j).getRoute().get(k)) - this.routes.get(i).indexOf(this.routes.get(j).getRoute().get(k+1)))==1 || (this.routes.get(i).indexOf(this.routes.get(j).getRoute().get(k)) - this.routes.get(i).indexOf(this.routes.get(j).getRoute().get(k+1)))==(-1) ){
                                    this.routes.get(j).getRoute().get(k).setUsable(false);
                                    this.routes.get(j).getRoute().get(k+1).setUsable(false);
                                }
                        }
                    }
                }
            }
        }
    }

    public Vector<Route> allSegmentRoutes(){
        Vector<Route> all = new Vector<Route>();
        Vector<Route> all2 = new Vector<Route>();
        boolean b=true;
        
        for(int i=0; i<this.getRoutes().size();i++){
            for(int j=0; j<this.getRoutes().get(i).segmentRoute().size();j++){
                all.add(this.getRoutes().get(i).segmentRoute().get(j));
                all2.add(this.getRoutes().get(i).segmentRoute().get(j));
            }
        }
                            
        for(int i=0; i<all.size();i++){
            for(int j=0; j<all2.size();j++){
                if( all.get(i).hasSubRoute(all2.get(j)) && !all.get(i).equal(all2.get(j))){
                    all2.remove(j);
                    j--;
                }
            } 
        }
                            
        return all2;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
    
    public boolean hasRoute( Route r){
        for (int i=0;i<this.routes.size();i++){
            if(r.equal(this.routes.get(i))){
                return true;
            }
        }
        return false;
    }
    
}
