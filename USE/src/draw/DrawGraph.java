/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draw;

/**
 *
 * @author dpi
 */

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import javax.swing.JFrame;
import javax.swing.JPanel;
import majority.MajorityGraph;

class Surface extends JPanel {
    
    
    MajorityGraph mig;

    public void setMig(MajorityGraph m){
        mig=m;
    }
    
    
    private void doDrawing(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        
        int scale = 40;
        
        
        for(int i=0;i<mig.getMajorityGraph().size();i++)
            for(int j=0;j<mig.getMajorityGraph().get(i).getRoutes().size();j++) 
                for(int k=0;k<mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size()-1;k++) 
                g2d.drawLine(mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY()*scale,mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX()*scale,mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k+1).getY()*scale,mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k+1).getX()*scale);
        
        
        
        g2d.setColor(Color.cyan);
        for(int i=0;i<mig.getMajorityGraph().size();i++)
            for(int j=0;j<mig.getMajorityGraph().get(i).getRoutes().size();j++){
                
                    g2d.fillRect(
                            mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0).getY() *scale - 18,
                            mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0).getX() *scale -18
                            , 35, 35);
                    
                    g2d.fillRect(
                        mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size()-1).getY() *scale - 18,
                        mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size()-1).getX() *scale -18
                        , 35, 35);
            }
        
        for(int i=0;i<mig.getMajorityGraph().size();i++){
                g2d.setColor(Color.blue);
                g2d.setFont(new Font("Courier New", Font.PLAIN, 18));
                g2d.drawString(mig.getMajorityGraph().get(i).getName(), mig.getMajorityGraph().get(i).getPlaceY()*scale -15, mig.getMajorityGraph().get(i).getPlaceX()*scale+5 );
                g2d.setColor(Color.red);
        } 
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }
}

public class DrawGraph extends JFrame {

    public DrawGraph(MajorityGraph m) {

        initUI(m);
    }

    private void initUI(MajorityGraph m) {

        Surface sf = new Surface();
        sf.setMig(m);
        add(sf);

        
        setTitle("Simple Java 2D example");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void draw(final MajorityGraph m) {

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                DrawGraph ex = new DrawGraph(m);
                ex.setVisible(true);
            }
            
        });
    }
}