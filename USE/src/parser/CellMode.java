package parser;

public enum CellMode {
    NORMAL(1),CROSSOVER(2),VERTICAL(3);
    private int id;
    
    CellMode(int id){
        this. id = id;
    }
    
    public int getId(){
        return this.id;
    }
}
