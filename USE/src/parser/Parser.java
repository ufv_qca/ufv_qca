/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.Vector;
import use.Point;
import use.Route;
import use.UseField;

/**
 *
 * @author geraldo
 */
public class Parser {

    private static final int SQUARE_SIZE = 5;
    private static final int WIRE = 1;
    private static final int FIXED0 = 3;
    private static final int FIXED1 = 4;
    private static final int INPUT = 5;
    private static final int VERTICAL = 6;
    private static final int CROSSOVER = 7;
    private static final int OUTPUT = 8;

    private UseField use;

    int line;
    int col;

    public int[][] use2qca(UseField use) {

        int useCellSize = 5; // quantidade de celulas qca por linha ou coluna de uma celula USE
        line = (use.getMig().maxPoint().getX() - use.getMig().minPoint().getX() + 1) * useCellSize;
        col = (use.getMig().maxPoint().getY() - use.getMig().minPoint().getY() + 1) * useCellSize;

        int x0 = 0;
        int x1 = 0;
        int x2 = 0;
        int x3 = 0;
        int y0 = 0;
        int y1 = 0;
        int y2 = 0;
        int y3 = 0;

        this.use = use;
        boolean bool = false;
        Point[][] qcaField = new Point[line][col];
        int[][] occupancyField = new int[line][col];

        use.clear();
        use.placeAndRouteMig();

        initGates(use, qcaField, useCellSize);

        for (int i = 0; i < line; i += 5) {
            for (int j = 0; j < col; j += 5) {
                occupancyField[i][j] = 0;
            }
        }
        //---------------------------------------------------------------------------------------------------------------
        // roteamento interno de um gates e outputs
        for (int k = 0; k < use.getMig().getMajorityGraph().size(); k++) {
            if (use.getMig().getMajorityGraph().get(k).getType().equals("gate") || use.getMig().getMajorityGraph().get(k).getType().equals("output")) {
                if (use.getMig().getMajorityGraph().get(k).getSubType().equals("and")) {
                    if (qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null && qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 2;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 2;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 2;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 2;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 2;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 2;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 2;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 1) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 2;
                    }
                } else if (use.getMig().getMajorityGraph().get(k).getSubType().equals("or")) {
                    if (qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null && qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 3;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 3;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 3;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 3;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 3;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 3;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 3;
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 1) == null) {
                        qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                        occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 3;
                    }
                }
            }
            if (use.getMig().getMajorityGraph().get(k).getType().equals("output")) {
                if (qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null && qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 7;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 7;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 7;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 7;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 7;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 7;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 7;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 1) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(k).getPlaceX(), use.getMig().getMajorityGraph().get(k).getPlaceY());
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 7;
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------
        //adiciona a estrutura do majority
        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            if (use.getMig().getMajorityGraph().get(i).getType().equals("gate") || use.getMig().getMajorityGraph().get(i).getType().equals("output")) {
                if (!use.getMig().getMajorityGraph().get(i).getSubType().equals("inv")) {
                    qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2] = new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------
        //adiciona a estrutura do inversores
        Vector<Point> invCells = new Vector<Point>();
        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            if (use.getMig().getMajorityGraph().get(i).getType().equals("gate") && use.getMig().getMajorityGraph().get(i).getSubType().equals("inv")) {

                if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null) {
                    if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) { //subindo
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null) {
                    if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) { //subindo
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null) {
                    if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) { //subindo
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null) {
                    if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) { //subindo
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                }

                // inversores de quina
                // entrando a partir da porta 01
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // entrando a partir da porta 03
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // a partir da porta 14
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // a partir da porta 34
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // a partir da porta 43
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // a partir da porta 41
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // a partir da porta 30
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // a partir da porta 10
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null) {
                        ;//nao faz nada porque ja esta pronto
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }

                // porta 01 43
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // porta 03 41
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // porta 43 01
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // porta 41 03
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // porta 10 34
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // porta 30 14
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // porta 14 30
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }
                // porta 34 10
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 1) {
                    if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null) {
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 2)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 2), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                        invCells.add(new Point(((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1), ((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1)));// new Point(use.getMig().getMajorityGraph().get(i).getPlaceX(), use.getMig().getMajorityGraph().get(i).getPlaceY());
                    }
                }

                for (int iv = 0; iv < invCells.size(); iv++) {
                    qcaField[invCells.get(iv).getX()][invCells.get(iv).getY()] = new Point(0, 0);
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------------
        for (int k = 0; k < use.getMig().getMajorityGraph().size(); k++) {
            if (use.getMig().getMajorityGraph().get(k).getType().equals("gate") || use.getMig().getMajorityGraph().get(k).getType().equals("output")) {
                for (int l = 0; l < 5; l++) {
                    for (int c = 0; c < 5; c++) {
                        if (qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + l][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + c] != null) {
                            occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + l][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + c]++;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            for (int j = 0; j < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size(); j++) {
                //variaçao no x ou vertical
                if (use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() == 0) {
                    //baixo para cima
                    if ((use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX()) > 0) {
                        x3 = 4;
                        y3 = 1;
                        x2 = 4;
                        y2 = 3;
                        x1 = 0;
                        y1 = 1;
                        x0 = 0;
                        y0 = 3;
                        if (!use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                            if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            } else if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            }
                        }

                    } else {//cima para baixo
                        x3 = 0;
                        y3 = 1;
                        x2 = 0;
                        y2 = 3;
                        x1 = 4;
                        y1 = 1;
                        x0 = 4;
                        y0 = 3;
                        if (!use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                            if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            } else if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            }
                        }

                    }
                } else {//variaçao no y ou horizontal
                    //direita para esquerda
                    if ((use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY()) > 0) {
                        x3 = 1;
                        y3 = 4;
                        x2 = 3;
                        y2 = 4;
                        x1 = 1;
                        y1 = 0;
                        x0 = 3;
                        y0 = 0;
                        if (!use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                            if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            } else if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            }
                        }
                    } //esquerda para direita
                    else if ((use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY()) < 0) {
                        x3 = 1;
                        y3 = 0;
                        x2 = 3;
                        y2 = 0;
                        x1 = 1;
                        y1 = 4;
                        x0 = 3;
                        y0 = 4;
                        if (!use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                            if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            } else if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) {
                                if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                } else if (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n + 1).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                }
                            }
                        }
                    } else { //variação de quinas
                    }
                }
            }
        }

        //---------------------------------------------------------------------------------------------------------------
        // roteamento interno de um input
        for (int k = 0; k < use.getMig().getMajorityGraph().size(); k++) {
            if (use.getMig().getMajorityGraph().get(k).getType().equals("input")) {

                if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 5;

                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 5;

                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 1) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 0) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 5;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = 5;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = 5;
                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 0) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 1) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = 5;

                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 4, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 3, 4) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 5;

                } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 0) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 0, 3) == null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(k), 1, 4) == null) {
                    qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = qcaField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0];
                    occupancyField[(use.getMig().getMajorityGraph().get(k).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(k).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = 5;

                }
            }
        }
        //---------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------------------
        //roteamento interno de fios
        Vector<Point> UCP = new Vector<Point>();// para rotear internamente as portas em uma USE cell
        Route rAux = new Route();
        for (int i = 0; i < line; i += 5) {
            for (int j = 0; j < col; j += 5) {
                if (use.field[i / 5 + use.getMig().minPoint().getX()][j / 5 + use.getMig().minPoint().getY()].getBusy() < 5 || (use.field[i / 5 + use.getMig().minPoint().getX()][j / 5 + use.getMig().minPoint().getY()].getMajorityNode() != null && use.field[i / 5 + use.getMig().minPoint().getX()][j / 5 + use.getMig().minPoint().getY()].getMajorityNode().getType().equals("input"))) { // apenas fios
                    if (qcaField[i + 0][j + 1] != null) {
                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 0, j + 1));

                        if (qcaField[i + 0][j + 1].equal(qcaField[i + 1][j + 4])) {
                            UCP.add(new Point(i + 1, j + 4));
                        }
                        if (qcaField[i + 0][j + 1].equal(qcaField[i + 3][j + 4])) {
                            UCP.add(new Point(i + 3, j + 4));
                        }
                        if (qcaField[i + 0][j + 1].equal(qcaField[i + 4][j + 1])) {
                            UCP.add(new Point(i + 4, j + 1));
                        }
                        if (qcaField[i + 0][j + 1].equal(qcaField[i + 4][j + 3])) {
                            UCP.add(new Point(i + 4, j + 3));
                        }
                        if (qcaField[i + 0][j + 1].equal(qcaField[i + 3][j + 0])) {
                            UCP.add(new Point(i + 3, j + 0));
                        }
                        if (qcaField[i + 0][j + 1].equal(qcaField[i + 1][j + 0])) {
                            UCP.add(new Point(i + 1, j + 0));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                    if (qcaField[i + 0][j + 3] != null) {

                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 0, j + 3));

                        if (qcaField[i + 0][j + 3].equal(qcaField[i + 1][j + 4])) {
                            UCP.add(new Point(i + 1, j + 4));
                        }
                        if (qcaField[i + 0][j + 3].equal(qcaField[i + 3][j + 4])) {
                            UCP.add(new Point(i + 3, j + 4));
                        }
                        if (qcaField[i + 0][j + 3].equal(qcaField[i + 4][j + 1])) {
                            UCP.add(new Point(i + 4, j + 1));
                        }
                        if (qcaField[i + 0][j + 3].equal(qcaField[i + 4][j + 3])) {
                            UCP.add(new Point(i + 4, j + 3));
                        }
                        if (qcaField[i + 0][j + 3].equal(qcaField[i + 3][j + 0])) {
                            UCP.add(new Point(i + 3, j + 0));
                        }
                        if (qcaField[i + 0][j + 3].equal(qcaField[i + 1][j + 0])) {
                            UCP.add(new Point(i + 1, j + 0));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                    if (qcaField[i + 1][j + 4] != null) {
                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 1, j + 4));

                        if (qcaField[i + 1][j + 4].equal(qcaField[i + 1][j + 0])) {
                            UCP.add(new Point(i + 1, j + 0));
                        }
                        if (qcaField[i + 1][j + 4].equal(qcaField[i + 3][j + 0])) {
                            UCP.add(new Point(i + 3, j + 0));
                        }
                        if (qcaField[i + 1][j + 4].equal(qcaField[i + 0][j + 1])) {
                            UCP.add(new Point(i + 0, j + 1));
                        }
                        if (qcaField[i + 1][j + 4].equal(qcaField[i + 0][j + 3])) {
                            UCP.add(new Point(i + 0, j + 3));
                        }
                        if (qcaField[i + 1][j + 4].equal(qcaField[i + 4][j + 3])) {
                            UCP.add(new Point(i + 4, j + 3));
                        }
                        if (qcaField[i + 1][j + 4].equal(qcaField[i + 4][j + 1])) {
                            UCP.add(new Point(i + 4, j + 1));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                    if (qcaField[i + 3][j + 4] != null) {
                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 3, j + 4));

                        if (qcaField[i + 3][j + 4].equal(qcaField[i + 1][j + 0])) {
                            UCP.add(new Point(i + 1, j + 0));
                        }
                        if (qcaField[i + 3][j + 4].equal(qcaField[i + 3][j + 0])) {
                            UCP.add(new Point(i + 3, j + 0));
                        }
                        if (qcaField[i + 3][j + 4].equal(qcaField[i + 0][j + 1])) {
                            UCP.add(new Point(i + 0, j + 1));
                        }
                        if (qcaField[i + 3][j + 4].equal(qcaField[i + 0][j + 3])) {
                            UCP.add(new Point(i + 0, j + 3));
                        }
                        if (qcaField[i + 3][j + 4].equal(qcaField[i + 4][j + 3])) {
                            UCP.add(new Point(i + 4, j + 3));
                        }
                        if (qcaField[i + 3][j + 4].equal(qcaField[i + 4][j + 1])) {
                            UCP.add(new Point(i + 4, j + 1));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                    if (qcaField[i + 4][j + 3] != null) {
                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 4, j + 3));

                        if (qcaField[i + 4][j + 3].equal(qcaField[i + 0][j + 1])) {
                            UCP.add(new Point(i + 0, j + 1));
                        }
                        if (qcaField[i + 4][j + 3].equal(qcaField[i + 0][j + 3])) {
                            UCP.add(new Point(i + 0, j + 3));
                        }
                        if (qcaField[i + 4][j + 3].equal(qcaField[i + 3][j + 4])) {
                            UCP.add(new Point(i + 3, j + 4));
                        }
                        if (qcaField[i + 4][j + 3].equal(qcaField[i + 1][j + 4])) {
                            UCP.add(new Point(i + 1, j + 4));
                        }
                        if (qcaField[i + 4][j + 3].equal(qcaField[i + 3][j + 0])) {
                            UCP.add(new Point(i + 3, j + 0));
                        }
                        if (qcaField[i + 4][j + 3].equal(qcaField[i + 1][j + 0])) {
                            UCP.add(new Point(i + 1, j + 0));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                    if (qcaField[i + 4][j + 1] != null) {
                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 4, j + 1));

                        if (qcaField[i + 4][j + 1].equal(qcaField[i + 0][j + 1])) {
                            UCP.add(new Point(i + 0, j + 1));
                        }
                        if (qcaField[i + 4][j + 1].equal(qcaField[i + 0][j + 3])) {
                            UCP.add(new Point(i + 0, j + 3));
                        }
                        if (qcaField[i + 4][j + 1].equal(qcaField[i + 1][j + 0])) {
                            UCP.add(new Point(i + 1, j + 0));
                        }
                        if (qcaField[i + 4][j + 1].equal(qcaField[i + 3][j + 0])) {
                            UCP.add(new Point(i + 3, j + 0));
                        }
                        if (qcaField[i + 4][j + 1].equal(qcaField[i + 1][j + 4])) {
                            UCP.add(new Point(i + 1, j + 4));
                        }
                        if (qcaField[i + 4][j + 1].equal(qcaField[i + 3][j + 4])) {
                            UCP.add(new Point(i + 3, j + 4));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                    if (qcaField[i + 3][j + 0] != null) {
                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 3, j + 0));

                        if (qcaField[i + 3][j + 0].equal(qcaField[i + 1][j + 4])) {
                            UCP.add(new Point(i + 1, j + 4));
                        }
                        if (qcaField[i + 3][j + 0].equal(qcaField[i + 3][j + 4])) {
                            UCP.add(new Point(i + 3, j + 4));
                        }
                        if (qcaField[i + 3][j + 0].equal(qcaField[i + 0][j + 1])) {
                            UCP.add(new Point(i + 0, j + 1));
                        }
                        if (qcaField[i + 3][j + 0].equal(qcaField[i + 0][j + 3])) {
                            UCP.add(new Point(i + 0, j + 3));
                        }
                        if (qcaField[i + 3][j + 0].equal(qcaField[i + 4][j + 1])) {
                            UCP.add(new Point(i + 4, j + 1));
                        }
                        if (qcaField[i + 3][j + 0].equal(qcaField[i + 4][j + 3])) {
                            UCP.add(new Point(i + 4, j + 3));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                    if (qcaField[i + 1][j + 0] != null) {
                        UCP = new Vector<Point>();
                        rAux = new Route();
                        UCP.add(new Point(i + 1, j + 0));

                        if (qcaField[i + 1][j + 0].equal(qcaField[i + 1][j + 4])) {
                            UCP.add(new Point(i + 1, j + 4));
                        }
                        if (qcaField[i + 1][j + 0].equal(qcaField[i + 3][j + 4])) {
                            UCP.add(new Point(i + 3, j + 4));
                        }
                        if (qcaField[i + 1][j + 0].equal(qcaField[i + 0][j + 1])) {
                            UCP.add(new Point(i + 0, j + 1));
                        }
                        if (qcaField[i + 1][j + 0].equal(qcaField[i + 0][j + 3])) {
                            UCP.add(new Point(i + 0, j + 3));
                        }
                        if (qcaField[i + 1][j + 0].equal(qcaField[i + 4][j + 1])) {
                            UCP.add(new Point(i + 4, j + 1));
                        }
                        if (qcaField[i + 1][j + 0].equal(qcaField[i + 4][j + 3])) {
                            UCP.add(new Point(i + 4, j + 3));
                        }

                        for (int k = 0; k < UCP.size() - 1; k++) {
                            rAux.merge(rAux.UseRouter(UCP.get(k), UCP.get(k + 1)));
                        }

                        for (int k = 0; k < rAux.getRoute().size(); k++) {
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                occupancyField[rAux.getRoute().get(k - 1).getX()][rAux.getRoute().get(k - 1).getY()] = 6;
                                k++;
                                while (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 1) {
                                    occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 7;
                                    k++;
                                }
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] = 6;
                            }
                            if (occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()] == 0) {
                                occupancyField[rAux.getRoute().get(k).getX()][rAux.getRoute().get(k).getY()]++;
                            }
                        }

                        for (int k = 0; k < UCP.size(); k++) {
                            qcaField[UCP.get(k).getX()][UCP.get(k).getY()] = null;
                        }

                    }
                }
            }
        }
        //---------------------------------------------------------------------------------------------------------------------

        //cross
        //---------------------------------------------------------------------------------------------------------------------
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < col; j++) {
                if (occupancyField[i][j] == 1) {
                    if (i > 0 && occupancyField[i - 1][j] == 6) {
                        if (i < (line - 1) && occupancyField[i + 1][j] == 6) {
                            occupancyField[i - 1][j] = 7;
                            occupancyField[i + 1][j] = 7;
                            occupancyField[i][j] = 6;
                        }
                        if (j > 0 && occupancyField[i][j - 1] == 6) {
                            occupancyField[i - 1][j] = 7;
                            occupancyField[i][j - 1] = 7;
                            occupancyField[i][j] = 6;
                        }
                        if (j < (col - 1) && occupancyField[i][j + 1] == 6) {
                            occupancyField[i - 1][j] = 7;
                            occupancyField[i][j + 1] = 7;
                            occupancyField[i][j] = 6;
                        }
                    }
                    if (i < (line - 1) && occupancyField[i + 1][j] == 6) {
                        if (i > 0 && occupancyField[i - 1][j] == 6) {
                            occupancyField[i + 1][j] = 7;
                            occupancyField[i - 1][j] = 7;
                            occupancyField[i][j] = 6;
                        }
                        if (j > 0 && occupancyField[i][j - 1] == 6) {
                            occupancyField[i + 1][j] = 7;
                            occupancyField[i][j - 1] = 7;
                            occupancyField[i][j] = 6;
                        }
                        if (j < (col - 1) && occupancyField[i][j + 1] == 6) {
                            occupancyField[i + 1][j] = 7;
                            occupancyField[i][j + 1] = 7;
                            occupancyField[i][j] = 6;
                        }
                    }
                    if (j > 0 && occupancyField[i][j - 1] == 6) {
                        if (j < (col - 1) && occupancyField[i][j + 1] == 6) {
                            occupancyField[i][j - 1] = 7;
                            occupancyField[i][j + 1] = 7;
                            occupancyField[i][j] = 9;
                        }
                        if (i > 0 && occupancyField[i - 1][j] == 6) {
                            occupancyField[i][j - 1] = 7;
                            occupancyField[i - 1][j] = 7;
                            occupancyField[i][j] = 9;
                        }
                        if (i < (line - 1) && occupancyField[i + 1][j] == 6) {
                            occupancyField[i][j - 1] = 7;
                            occupancyField[i + 1][j] = 7;
                            occupancyField[i][j] = 6;
                        }
                    }
                    if (j < (col - 1) && occupancyField[i][j + 1] == 6) {
                        if (j > 0 && occupancyField[i][j - 1] == 6) {
                            occupancyField[i][j + 1] = 7;
                            occupancyField[i][j - 1] = 7;
                            occupancyField[i][j] = 6;
                        }
                        if (i > 0 && occupancyField[i - 1][j] == 6) {
                            occupancyField[i][j + 1] = 7;
                            occupancyField[i - 1][j] = 7;
                            occupancyField[i][j] = 6;
                        }
                        if (i < (line - 1) && occupancyField[i + 1][j] == 6) {
                            occupancyField[i][j + 1] = 7;
                            occupancyField[i + 1][j] = 7;
                            occupancyField[i][j] = 6;
                        }
                    }
                }
            }
        }

        // expande inicio do cruzamento em 1 celula
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < col; j++) {
                if (occupancyField[i][j] == 6) {
                    if (occupancyField[i - 1][j] == 1) {
                        occupancyField[i][j] = 7;
                        occupancyField[i - 1][j] = 9;
                    }
                    if (occupancyField[i + 1][j] == 1) {
                        occupancyField[i][j] = 7;
                        occupancyField[i + 1][j] = 9;
                    }
                    if (occupancyField[i][j - 1] == 1) {
                        occupancyField[i][j] = 7;
                        occupancyField[i][j - 1] = 9;
                    }
                    if (occupancyField[i][j + 1] == 1) {
                        occupancyField[i][j] = 7;
                        occupancyField[i][j + 1] = 9;
                    }
                }
            }
        }
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < col; j++) {
                if (occupancyField[i][j] == 9) {
                    occupancyField[i][j] = 6;
                }
            }
        }

        //dois cruzamentos seguidos
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < col; j++) {
                if (occupancyField[i][j] == 6) {
                    if (occupancyField[i - 1][j] == 7 && occupancyField[i + 1][j] == 7) {
                        occupancyField[i][j] = 7;
                    }
                    if (occupancyField[i + 1][j] == 6) {
                        occupancyField[i][j] = 7;
                        occupancyField[i + 1][j] = 7;
                    }
                    if (occupancyField[i - 1][j] == 6) {
                        occupancyField[i][j] = 7;
                        occupancyField[i - 1][j] = 7;
                    }
                    if (occupancyField[i][j + 1] == 6) {
                        occupancyField[i][j] = 7;
                        occupancyField[i][j + 1] = 7;
                    }

                    if (occupancyField[i][j - 1] == 6) {
                        occupancyField[i][j] = 7;
                        occupancyField[i][j - 1] = 7;
                    }

                }
            }
        }

        //---------------------------------------------------------------------------------------------------------------------
        /*
         //imprime-occupancy----------------------------------
         for (int i = 0; i < line; i++) {
         if (i % 5 == 0) {
         System.out.println();
         }
         for (int j = 0; j < col; j++) {
         if (j % 5 == 0) {
         System.out.printf(" ");
         }
         if(occupancyField[i][j]==0)
         System.out.printf("%s","-");
         else
         System.out.printf("%s",occupancyField[i][j]);
         }
         System.out.println();
         }
         //--------------------------------------------
         */
        /*
         //imprime------------------------------------
         for (int i = 0; i < line; i++) {
         if (i % 5 == 0) {
         System.out.println();
         }
         for (int j = 0; j < col; j++) {
         if (j % 5 == 0) {
         System.out.printf(" | ");
         }
         if(qcaField[i][j]==null)
         System.out.printf("%-7s","   - ");
         else
         System.out.printf("%-7s",qcaField[i][j]);
         }
         System.out.println();
         }
         //--------------------------------------------
         */
        QCADesignerParser(occupancyField);
        return occupancyField;
    }

    //Gera matriz de células e transforma no formato do QCADesigner
    private void QCADesignerParser(int occupancyField[][]) {
        Cell matriz[][] = new Cell[occupancyField.length][occupancyField[0].length];
        Point minPoint = this.use.getMig().minPoint();

        //Cria matriz de células
        for (int i = 0; i < occupancyField.length; i++) {
            for (int j = 0; j < occupancyField[0].length; j++) {

                switch (occupancyField[i][j]) {
                    case WIRE:
                        matriz[i][j] = new Cell(CellMode.NORMAL, CellFunction.NORMAL, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getClockZone() - 1, 0, "");
                        break;
                    case FIXED0:
                        matriz[i][j] = new Cell(CellMode.NORMAL, CellFunction.FIXED, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getClockZone() - 1, -1, "");
                        break;
                    case FIXED1:
                        matriz[i][j] = new Cell(CellMode.NORMAL, CellFunction.FIXED, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getClockZone() - 1, 1, "");
                        break;
                    case CROSSOVER:
                        matriz[i][j] = new Cell(CellMode.CROSSOVER, CellFunction.NORMAL, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getClockZone() - 1, 0, "");
                        break;
                    case INPUT:
                        matriz[i][j] = new Cell(CellMode.NORMAL, CellFunction.INPUT, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getClockZone() - 1, 0, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getMajorityNode().getName());
                        break;
                    case OUTPUT:
                        matriz[i][j] = new Cell(CellMode.NORMAL, CellFunction.OUTPUT, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getClockZone() - 1, 0, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getMajorityNode().getName());
                        break;
                    case VERTICAL:
                        matriz[i][j] = new Cell(CellMode.VERTICAL, CellFunction.NORMAL, this.use.field[minPoint.getX() + (i / 5)][minPoint.getY() + (j / 5)].getClockZone() - 1, 0, "");
                        break;
                    default:
                        matriz[i][j] = null;
                        break;
                }

            }

        }

        QCAConverter cell2qca = new QCAConverter();
        cell2qca.convertToQCA(matriz);
    }

    public void initGates(UseField use, Point[][] qcaField, int useCellSize) {

        //adiciona entrada em todos gates com base nas rotas de cada node
        //----------------------------------------------------------------------------------------------------
        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            //if ((use.getMig().getMajorityGraph().get(i).getType().equals("gate")) ) {
            for (int j = 0; j < use.getMig().getMajorityGraph().get(i).getRoutes().size(); j++) {
                if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() > 1) {

                    //entra por cima
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() > 0) {
                        //entra pela esquerda
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() > 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        //entra pela direita
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() < 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                    }
                    //entra por baixo
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() < 0) {
                        //entra pela esquerda
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() > 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        //entra pela direita
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() < 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                    }
                    //entra pela esquerda
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() > 0) {
                        //entra por cima
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() > 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        //entra por baixo
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() < 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                    }
                    //entra pela direita
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() < 0) {
                        //entra por cima
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() > 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                        //entra por baixo
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() < 0) {
                            if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            } else {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            }
                        }
                    }
                } else {
                    //entra por cima
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() > 0) {
                        if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        } else {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        }
                    }
                    //entra por baixo
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() < 0) {
                        if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        } else {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        }
                    }
                    //entra pela esquerda
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() > 0) {
                        if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        } else {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        }
                    }
                    //entra pela direita
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() < 0) {
                        if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] == null) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        } else {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                        }
                    }

                }
                //}

            }
        }
        //----------------------------------------------------------------------------------------------------
/*
         //adiciona entrada em todos gates com base nas rotas de cada input
         //----------------------------------------------------------------------------------------------------
         for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
         if ((use.getMig().getMajorityGraph().get(i).getType().equals("input"))) {
         for (int j = 0; j < use.getMig().getMajorityGraph().get(i).getRoutes().size(); j++) {
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() > 1) {

         //entra por cima
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() > 0) {
         //entra pela esquerda
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() > 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         //entra pela direita
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() < 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         }
         //entra por baixo
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() < 0) {
         //entra pela esquerda
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() > 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         //entra pela direita
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getY() < 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         }
         //entra pela esquerda
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() > 0) {
         //entra por cima
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() > 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         //entra por baixo
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() < 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         }
         //entra pela direita
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() < 0) {
         //entra por cima
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() > 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         //entra por baixo
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().size() - 2).getRoute().size() - 2).getX() < 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         }
         } else {
         //entra por cima
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() > 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         //entra por baixo
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getX() < 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         //entra pela esquerda
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() > 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }
         //entra pela direita
         if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 1).getY() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().get(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().size() - 2).getY() < 0) {
         if (qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] == null) {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         } else {
         qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(j).segmentRoute().lastElement().getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
         }
         }

         }
         }

         }
         }
         //----------------------------------------------------------------------------------------------------
         */
        //adiciona saidas aos gates e caso a rota tenha apenas um segmento, adiciona a entrada no gate que esta no fim da rota
        //----------------------------------------------------------------------------------------------------
        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            //if (use.getMig().getMajorityGraph().get(i).getType().equals("output") || (use.getMig().getMajorityGraph().get(i).getType().equals("gate") && !use.getMig().getMajorityGraph().get(i).getSubType().equals("inv"))) {
            for (int j = 0; j < use.getMig().getMajorityGraph().get(i).getRoutes().size(); j++) {
                if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) {
                    if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) {//entra por baixo e esquerda
                        //entra pela esquerda/sai pela direita
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(1).getX() == 0) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
				//if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //    qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        } else {//entra por baixo/sai por cima
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
				//if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //    qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        }
                    } else { // entra por baixo e direita
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(1).getX() == 0) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
			        //if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //    qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        } else {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
			        //if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //    qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        }
                    }
                } else {
                    if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) {//entra por cima e esquerda
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(1).getX() == 0) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
				//if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //   qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        } else {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
			        //if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //    qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        }
                    } else { // entra por cima e direita
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0).getX() - use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(1).getX() == 0) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
				//if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //    qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 4] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        } else {
                            qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
				//if(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size()==1){
                            //    qcaField[(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + 1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(0);
                            //}
                        }
                    }
                }
            }
            // }
        }
        //----------------------------------------------------------------------------------------------------

        //corrige entradas adjacentes
        //----------------------------------------------------------------------------------------------------
        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            if (use.getMig().getMajorityGraph().get(i).getType().equals("output") || (use.getMig().getMajorityGraph().get(i).getType().equals("gate"))) {
                if (use.getMig().getMajorityGraph().get(i).getPlaceX() % 2 == 0) {
                    //entra por baixo esquerda
                    if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) {
                        if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) != null) {
                            if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = null;
                            } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = null;
                            }
                        }
                    } else {//entra por cima esquerda
                        if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 0) != null) {
                            if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = null;
                            } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 0) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0] = null;
                            }
                        }
                    }
                } else {
                    //entra por baixo direita
                    if (use.getMig().getMajorityGraph().get(i).getPlaceY() % 2 == 0) {
                        if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) != null) {
                            if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 4, 1) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = null;
                            } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = null;
                            }
                        }
                    } else {//entra por cima direita
                        if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 3) != null && use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 1, 4) != null) {
                            if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 0, 1) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = null;
                            } else if (use.UsePort(qcaField, use.getMig().getMajorityGraph().get(i), 3, 4) == null) {
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4];
                                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4] = null;
                            }
                        }
                    }
                }
            }
        }
        //----------------------------------------------------------------------------------------------------

	//estende todas portas dos gates a 1 celula a mais
        //----------------------------------------------------------------------------------------------------
        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            //if ( (use.getMig().getMajorityGraph().get(i).getType().equals("output") || use.getMig().getMajorityGraph().get(i).getType().equals("gate")) &&  use.getMig().getMajorityGraph().get(i).getSubType().equals("inv")){		
            if (((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0 - 1) > 0) {
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0 - 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1];
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0 - 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 0][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3];
            }
            if (((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4 + 1) < col) {
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4 + 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4];
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4 + 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 4];
            }
            if (((use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4 + 1) < line) {
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4 + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 3];
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4 + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 4][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 1];
            }
            if (((use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0 - 1) > 0) {
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0 - 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 3][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0];
                qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0 - 1] = qcaField[(use.getMig().getMajorityGraph().get(i).getPlaceX() - use.getMig().minPoint().getX()) * useCellSize + 1][(use.getMig().getMajorityGraph().get(i).getPlaceY() - use.getMig().minPoint().getY()) * useCellSize + 0];
            }
            //}        
        }

        //----------------------------------------------------------------------------------------------------
        //
    }


    //roteamento de segmentos q fazem curva
    public void wireRouter(UseField use, Point[][] qcaField, int useCellSize) {
        int x0 = 0;
        int x1 = 0;
        int x2 = 0;
        int x3 = 0;
        int y0 = 0;
        int y1 = 0;
        int y2 = 0;
        int y3 = 0;

        for (int i = 0; i < use.getMig().getMajorityGraph().size(); i++) {
            for (int j = 0; j < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().size(); j++) {

                if ((use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() == use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX()) && (use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() < 0)) {

                    x3 = 1;
                    y3 = 0;
                    x2 = 3;
                    y2 = 0;
                    x1 = 3;
                    y1 = 4;
                    x0 = 1;
                    y0 = 4;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    x3 = 3;
                    y3 = 0;
                    x2 = 1;
                    y2 = 0;
                    x1 = 1;
                    y1 = 4;
                    x0 = 3;
                    y0 = 4;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if ((use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() == use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX()) && (use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY() > 0)) {
                    x3 = 1;
                    y3 = 4;
                    x2 = 3;
                    y2 = 4;
                    x1 = 3;
                    y1 = 0;
                    x0 = 1;
                    y0 = 0;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    x3 = 3;
                    y3 = 4;
                    x2 = 1;
                    y2 = 4;
                    x1 = 1;
                    y1 = 0;
                    x0 = 3;
                    y0 = 0;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if ((use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() == use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY()) && (use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() < 0)) {

                    x3 = 0;
                    y3 = 1;
                    x2 = 0;
                    y2 = 3;
                    x1 = 4;
                    y1 = 3;
                    x0 = 4;
                    y0 = 1;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    x3 = 0;
                    y3 = 3;
                    x2 = 0;
                    y2 = 1;
                    x1 = 4;
                    y1 = 1;
                    x0 = 4;
                    y0 = 3;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if ((use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() == use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getY()) && (use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(1).getX() > 0)) {

                    x3 = 4;
                    y3 = 1;
                    x2 = 4;
                    y2 = 3;
                    x1 = 0;
                    y1 = 3;
                    x0 = 0;
                    y0 = 1;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    x3 = 4;
                    y3 = 3;
                    x2 = 4;
                    y2 = 1;
                    x1 = 0;
                    y1 = 1;
                    x0 = 0;
                    y0 = 3;
                    if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))) {
                        if (use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))) {
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().lastElement().getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(0).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                            for (int n = 1; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {
                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3]))))) {
                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    if (use.field[use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX()][use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY()].getBusy() == 1) {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0]))))) {
                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                    } else {
                                        if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                            qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                            n++;
                                            for (; n < use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().size() - 1; n++) {

                                                if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x2][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y2]))))) {
                                                    qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x3][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y3] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    if ((((use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0])) || (qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] == null && !use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0).equal(qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x1][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y1]))))) {
                                                        qcaField[(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getX() - use.getMig().minPoint().getX()) * useCellSize + x0][(use.getMig().getMajorityGraph().get(i).allSegmentRoutes().get(j).getRoute().get(n).getY() - use.getMig().minPoint().getY()) * useCellSize + y0] = use.getMig().getMajorityGraph().get(i).getRoutes().get(0).getRoute().get(0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }


}