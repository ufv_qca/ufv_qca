package parser;

public class Cell {
    private CellMode mode;
    private CellFunction function;
    private int clockZone;
    private double polarization;
    private String label;
    private boolean usable;
    
    public Cell(){
        mode = CellMode.NORMAL;
        function = CellFunction.NORMAL;
        clockZone = 0;
        polarization = 0.0;
        label = "";
        usable = false;
        
    }

    public Cell(CellMode mode,CellFunction type, int clockZone, double polarization, String label) {
        this.mode = mode;
        this.function = type;
        this.clockZone = clockZone;
        this.polarization = polarization;
        this.label = label;
        //usable = false;
    }

    public CellMode getMode(){
        return this.mode;
    }
    
    public void setMode(CellMode mode){
        this.mode = mode;
    }
    
    public CellFunction getFunction() {
        return function;
    }

    public void setFunction(CellFunction type) {
        this.function = type;
    }

    public int getClockZone() {
        return clockZone;
    }

    public void setClockZone(int clockZone) {
        this.clockZone = clockZone;
    }

    public double getPolarization() {
        return polarization;
    }

    public void setPolarization(double polarization) {
        this.polarization = polarization;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isUsable() {
        return usable;
    }

    public void setUsable(boolean usable) {
        this.usable = usable;
    }
    
    
}
