/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

/**
 *
 * @author juju
 */
public enum CellFunction {
    NORMAL(1),FIXED(2),INPUT(5),OUTPUT(8);
    
    private int id;
    
    CellFunction(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    
}
