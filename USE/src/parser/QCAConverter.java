package parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class QCAConverter {

    private String qlytFile;
    private static String destinyPath;
    private static boolean hasChosenDirectory;
    private static int fileID;

    static {
        fileID = 0;
        destinyPath = "/home";
        hasChosenDirectory = false;
    }

    public QCAConverter() {
        qlytFile = "solution" + fileID + ".qlyt";
        if (!hasChosenDirectory) {
            destinyPath = chooseDirectory();
        }

    }

    public boolean convertToQCA(Cell[][] cells) {
        if (cells != null) {
            //Precisamos de três camadas por causa dos Crossover
            File result = new File(this.qlytFile);
            File via = new File("via");
            File layer2 = new File("layer2");

            try {
                FileWriter fwMain = new FileWriter(result);
                FileWriter fwVia = new FileWriter(via);
                FileWriter fwLayer2 = new FileWriter(layer2);

                BufferedWriter bwMain = new BufferedWriter(fwMain);
                BufferedWriter bwVia = new BufferedWriter(fwVia);
                BufferedWriter bwlayer2 = new BufferedWriter(fwLayer2);

                bwMain.write("[TYPE:QCADLayer]\n");
                bwMain.write("name=layer1\n");

                bwVia.write("[TYPE:QCADLayer]\n");
                bwVia.write("name=via\n");

                bwlayer2.write("[TYPE:QCADLayer]\n");
                bwlayer2.write("name=layer2\n");

                for (int i = 0; i < cells.length; i++) {
                    for (int j = 0; j < cells[i].length; j++) {
                        if (cells[i][j] != null) {
                            switch (cells[i][j].getMode()) {
                                case NORMAL:
                                    bwMain.write("[TYPE:QCADCell]\n");
                                    bwMain.write("x=" + j + "\n");
                                    bwMain.write("y=" + i + "\n");
                                    bwMain.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                    bwMain.write("cell_options.mode=QCAD_CELL_MODE_NORMAL\n");

                                    break;

                                case CROSSOVER:

                                    if (crossOver(cells, i, j)) 
                                    {
                                        bwMain.write("[TYPE:QCADCell]\n");
                                        bwMain.write("x=" + j + "\n");
                                        bwMain.write("y=" + i + "\n");
                                        bwMain.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                        bwMain.write("cell_options.mode=QCAD_CELL_MODE_NORMAL\n");
                                    } else {
                                        if ((i > 0) && (i < (cells.length - 1)) && (j > 0) && (j < cells[i].length)) {
                                            if ((cells[i - 1][j] != null) && (cells[i + 1][j] != null)) {
                                                if (cells[i - 1][j].getMode().equals(CellMode.NORMAL)) {
                                                    if (cells[i + 1][j].getMode().equals(CellMode.NORMAL)) {
                                                        bwMain.write("[TYPE:QCADCell]\n");
                                                        bwMain.write("x=" + j + "\n");
                                                        bwMain.write("y=" + i + "\n");
                                                        bwMain.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                                        bwMain.write("cell_options.mode=QCAD_CELL_MODE_NORMAL\n");
                                                    }
                                                } else if ((cells[i][j - 1] != null) && (cells[i][j + 1] != null)) {
                                                    if (cells[i][j - 1].getMode().equals(CellMode.NORMAL)) {
                                                        if (cells[i][j + 1].getMode().equals(CellMode.NORMAL)) {
                                                            bwMain.write("[TYPE:QCADCell]\n");
                                                            bwMain.write("x=" + j + "\n");
                                                            bwMain.write("y=" + i + "\n");
                                                            bwMain.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                                            bwMain.write("cell_options.mode=QCAD_CELL_MODE_NORMAL\n");
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                    bwlayer2.write("[TYPE:QCADCell]\n");
                                    bwlayer2.write("x=" + j + "\n");
                                    bwlayer2.write("y=" + i + "\n");
                                    bwlayer2.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                    bwlayer2.write("cell_options.mode=QCAD_CELL_MODE_CROSSOVER\n");
                                    bwlayer2.write("cell_function=QCA_CELL_NORMAL\n");
                                    bwlayer2.write("[#TYPE:QCADCell]\n");

                                    break;

                                case VERTICAL:
                                    bwMain.write("[TYPE:QCADCell]\n");
                                    bwMain.write("x=" + j + "\n");
                                    bwMain.write("y=" + i + "\n");
                                    bwMain.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                    bwMain.write("cell_options.mode=QCAD_CELL_MODE_VERTICAL\n");

                                    bwVia.write("[TYPE:QCADCell]\n");
                                    bwVia.write("x=" + j + "\n");
                                    bwVia.write("y=" + i + "\n");
                                    bwVia.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                    bwVia.write("cell_options.mode=QCAD_CELL_MODE_VERTICAL\n");
                                    bwVia.write("cell_function=QCA_CELL_NORMAL\n");
                                    bwVia.write("[#TYPE:QCADCell]\n");

                                    bwlayer2.write("[TYPE:QCADCell]\n");
                                    bwlayer2.write("x=" + j + "\n");
                                    bwlayer2.write("y=" + i + "\n");
                                    bwlayer2.write("cell_options.clock=" + cells[i][j].getClockZone() + "\n");
                                    bwlayer2.write("cell_options.mode=QCAD_CELL_MODE_VERTICAL\n");
                                    bwlayer2.write("cell_function=QCA_CELL_NORMAL\n");
                                    bwlayer2.write("[#TYPE:QCADCell]\n");

                                    break;

                            }

                            switch (cells[i][j].getFunction()) {
                                case NORMAL:
                                    bwMain.write("cell_function=QCAD_CELL_NORMAL\n");
                                    bwMain.write("[#TYPE:QCADCell]\n");
                                    break;

                                case INPUT:
                                    bwMain.write("cell_function=QCAD_CELL_INPUT\n");
                                    bwMain.write("cell_name=" + cells[i][j].getLabel() + "\n");
                                    bwMain.write("[#TYPE:QCADCell]\n");
                                    break;

                                case OUTPUT:
                                    bwMain.write("cell_function=QCAD_CELL_OUTPUT\n");
                                    bwMain.write("cell_name=" + cells[i][j].getLabel() + "\n");
                                    bwMain.write("[#TYPE:QCADCell]\n");
                                    break;

                                case FIXED:
                                    if (cells[i][j].getPolarization() < 0) {
                                        bwMain.write("cell_function=QCAD_CELL_FIXED0\n");
                                    } else if (cells[i][j].getPolarization() > 0) {
                                        bwMain.write("cell_function=QCAD_CELL_FIXED1\n");
                                    }

                                    bwMain.write("[#TYPE:QCADCell]\n");
                                    break;
                            }
                        }
                    }
                }

                bwlayer2.write("[#TYPE:QCADLayer]");
                bwVia.write("[#TYPE:QCADLayer]");
                bwMain.write("[#TYPE:QCADLayer]");

                bwlayer2.close();
                bwVia.close();
                bwMain.close();

                fwLayer2.close();
                fwVia.close();
                fwMain.close();

                //Concatena todas as camadas em um só arquivo
                concatFile(this.qlytFile, "via");
                concatFile(this.qlytFile, "layer2");

                via.delete();
                layer2.delete();
                runQCAParser();

            } catch (IOException ex) {
                System.out.println("Não conseguiu abrir o arquivo para escrita");
                ex.printStackTrace();
            }

            return true;
        } else {
            return false;
        }
    }

    private boolean crossOver(Cell[][] cells, int i, int j) {
        int contagemSetes = 0, k = 0, celulasVerticais;
        if (cells != null) {
            if (cells[i][j] != null) {
                //verifica se tem pelo menos dois 7 ao lado
                //verifica o lado de cima
                if ((i > 0) && (cells[i - 1][j]!= null)  && (cells[i - 1][j].getMode().equals(CellMode.CROSSOVER))) {
                    contagemSetes++;
                    
                }

                //verifica o lado de baixo
                if ((i < cells.length) && (cells[i + 1][j] != null) && (cells[i + 1][j].getMode().equals(CellMode.CROSSOVER))) {
                    contagemSetes++;
                }

                //verifica o lado esquerdo
                if ((j > 0) && (cells[i][j - 1] != null) && (cells[i][j - 1].getMode().equals(CellMode.CROSSOVER))) {
                    contagemSetes++;
                }

                //verifica o lado direito
                if ((j < cells[i].length) && (cells[i][j + 1] != null) && (cells[i][j + 1].getMode().equals(CellMode.CROSSOVER))) {
                    contagemSetes++;
                }
                
               
                if (contagemSetes >= 2) {
                    //Verifica se tem duas células verticais na coluna

                    //percorre a parte de cima
                    k = i;
                    celulasVerticais = 0;

                    while (k > 0) {

                        if (cells[k][j] == null) 
                        {
                            break;
                        }
                        else if (cells[k][j].getMode().equals(CellMode.CROSSOVER))
                        {
                            k--;
                            continue;
                        } 
                        else if (cells[k][j].getMode().equals(CellMode.VERTICAL)) 
                        {
                            celulasVerticais++;
                            break;
                        } 
                        else 
                        {
                            break;
                        }
                    }

                    //percorre a parte de baixo
                    k = i;
                    while (k < cells.length) {
                        if (cells[k][j] == null) {
                            break;
                        } else if (cells[k][j].getMode().equals(CellMode.CROSSOVER)) {
                            k++;
                            continue;
                        } else if (cells[k][j].getMode().equals(CellMode.VERTICAL)) {
                            celulasVerticais++;
                            break;
                        } else {
                            break;
                        }
                    }

                    //Se não encontrou duas células verticais, então tem célula na camada principal 
                    if (celulasVerticais <= 1) {
                      
                        return true;
                    }

                    //percorre a parte da esquerda
                    k = j;
                    celulasVerticais = 0;

                    while (k > 0) {
                        if (cells[i][k] == null) {
                            break;
                        } else if (cells[i][k].getMode().equals(CellMode.CROSSOVER)) {
                            k--;
                            continue;
                        } else if (cells[i][k].getMode().equals(CellMode.VERTICAL)) {
                            celulasVerticais++;
                            break;
                        } else {
                            break;
                        }
                    }

                    //percorre a parte da direita
                    k = j;
                    while (k < cells[i].length) {
                        if (cells[i][k] == null) {
                            break;
                        } else if (cells[i][k].getMode().equals(CellMode.CROSSOVER)) {
                            k++;
                            continue;
                        } else if (cells[i][k].getMode().equals(CellMode.VERTICAL)) {
                            celulasVerticais++;
                            break;
                        } else {
                            break;
                        }
                    }

                    //Se não encontrou duas células verticais, então tem célula na camada principal ta
                    if (celulasVerticais <= 1) {
                        
                        return true;
                    }

                }

            } else {
                return false;
            }

        } else {
            return false;
        }
        return false;
    }

    private void runQCAParser() {

        compileCFile("qlyt2qca.c");
        runCFile("qlyt2qca.c");
    }

    private void compileCFile(String cFile) {
        String compileFileCommand = "gcc -w " + cFile + " -o " + cFile.substring(0, cFile.length() - 2);

        String resultString = "";
        try {

            Process processCompile = Runtime.getRuntime().exec(compileFileCommand);

            BufferedReader brCompileError = new BufferedReader(new InputStreamReader(processCompile.getErrorStream()));
            String errorCompile = brCompileError.readLine();
            if (errorCompile != null) {
                System.out.println("Error Compiler = " + errorCompile);
            }

            resultString += errorCompile + "\n";

            BufferedReader brCompileRun = new BufferedReader(new InputStreamReader(processCompile.getErrorStream()));
            String outputCompile = brCompileRun.readLine();
            if (outputCompile != null) {
                System.out.println("Output Compiler = " + outputCompile);
            }

            resultString += outputCompile + "\n";

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Exception ");
            System.out.println(e.getMessage());
        }
    }

    private void runCFile(String cFile) {

        try {

            //System.out.println("Running C File");
            String runFileCommand = "./" + cFile.substring(0, cFile.length() - 2) + " " + this.qlytFile;
            Process processRun = Runtime.getRuntime().exec(runFileCommand);

            BufferedReader brRun = new BufferedReader(new InputStreamReader(processRun.getErrorStream()));
            String errorRun = brRun.readLine();
            if (errorRun != null) {
                System.out.println("Error Run = " + errorRun);
            }

            BufferedReader brResult = new BufferedReader(new InputStreamReader(processRun.getInputStream()));
            String outputRun = brResult.readLine();
            if (outputRun != null) {
                System.out.println("Output Run = " + outputRun);
            }
            System.out.println("SOLUTION " + fileID);
//            runFileCommand = "cp " + this.qlytFile.substring(0, this.qlytFile.length() - 5) + ".qca " + destinyPath;
//            processRun = Runtime.getRuntime().exec(runFileCommand);
            String source = this.qlytFile.substring(0, this.qlytFile.length() - 5) + ".qca";
            copyToDirectory(source, destinyPath);
            File f = new File(this.qlytFile);//Deleta o arquivo .qlyt
            f.delete();

            while (processRun.isAlive());//espera o processo acabar de copiar o arquivo, só depois pode apagar o arquivo

            f = new File(this.qlytFile.substring(0, this.qlytFile.length() - 5) + ".qca");//Deleta o arquivo .qca
            f.delete();
            fileID++;

            brRun = new BufferedReader(new InputStreamReader(processRun.getErrorStream()));
            errorRun = brRun.readLine();

            if (errorRun != null) {
                System.out.println("Error Run = " + errorRun);
            }

            brResult = new BufferedReader(new InputStreamReader(processRun.getInputStream()));
            outputRun = brResult.readLine();

            if (outputRun != null) {
                System.out.println("Output Run = " + outputRun);
            }

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Exception ");
            System.out.println(e.getMessage());
        }
    }

    public String chooseDirectory() {

        JButton open = new JButton();
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Select the directory to save the qca files");
        File f = new File("/home");
        fc.setCurrentDirectory(f);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (fc.showOpenDialog(open) == JFileChooser.APPROVE_OPTION) {
            hasChosenDirectory = true;
            this.fileID = 0;

            String path = fc.getSelectedFile().getAbsolutePath();

            return (path);

        }

        return destinyPath;

    }

    private void concatFile(String file1, String file2) {
        File firstFile = new File(file1);
        File secondFile = new File(file2);

        try {

            FileWriter fwFirstFile = new FileWriter(firstFile, true);
            FileReader frSecondFile = new FileReader(secondFile);

            BufferedWriter bwFirstFile = new BufferedWriter(fwFirstFile);
            BufferedReader brSecondFile = new BufferedReader(frSecondFile);

            String line = "";

            bwFirstFile.write("\n");
            while ((line = brSecondFile.readLine()) != null) {
                bwFirstFile.write(line + "\n");
            }

            brSecondFile.close();
            bwFirstFile.close();

            frSecondFile.close();
            fwFirstFile.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private boolean copyToDirectory(String source, String destiny) {

        if (source == null || source.isEmpty() || destiny == null || destiny.isEmpty()) {
            return false;
        }

        String nomeShell = "copy.sh";
        File shell = new File(nomeShell);

        source = source.replace(" ", "\\ ");
        destiny = destiny.replace(" ", "\\ ");
        try {
            FileWriter fw = new FileWriter(shell);
            fw.write("#!/bin/bash\n");
            fw.write("cp " + source + " " + destiny);
            fw.close();
            String runFileCommand = "chmod +x " + nomeShell;
            Process processRun = Runtime.getRuntime().exec(runFileCommand);

            runFileCommand = "./" + nomeShell;
            processRun = Runtime.getRuntime().exec(runFileCommand);

            while (processRun.isAlive());//espera o processo acabar de copiar o arquivo.

            BufferedReader brRun = new BufferedReader(new InputStreamReader(processRun.getErrorStream()));
            String errorRun = brRun.readLine();
            if (errorRun != null) {
                System.out.println("Error Run = " + errorRun);
            }

            BufferedReader brResult = new BufferedReader(new InputStreamReader(processRun.getInputStream()));
            String outputRun = brResult.readLine();
            if (outputRun != null) {
                System.out.println("Output Run = " + outputRun);
            }

            return true;

        } catch (IOException ex) {

            return false;
        }

    }
}
