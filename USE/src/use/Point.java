/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package use;

/**
 *
 * @author geraldo
 */
public class Point {
    private int x;
    private int y;
    private boolean usable;// para validade na rota

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        this.usable=true;
    }
    
    public Point(Point p) {
        this.x = p.x;
        this.y = p.y;
        this.usable=p.usable;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public boolean isUsable() {
        return usable;
    }

    public void setUsable(boolean usable) {
        this.usable = usable;
    }
    
    public void print(){
        System.out.printf("(%d.%d)",x,y);
    }
    
    public String toString(){
        return "("+x+","+y+")";
    }
    
    public boolean equal(Point p){
        if(p==null)
            return false;
        if(p.getX()==this.x && p.getY()==this.y)
            return true;
        else
            return false;
    }
    
    
    
    
}
