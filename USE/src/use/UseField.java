/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package use;

import static java.lang.Math.abs;
import java.util.Vector;
import majority.MajorityGraph;
import majority.MajorityNode;

/**
 *
 * @author geraldo
 * 
* Uma linha i possui o sentido da esquerda para direita se i é par, caso
 * contrario o sentido e o oposto Uma coluna j possui o sentido de baixo para
 * cima se j é par, caso contrario o sentido e o oposto
 */
public class UseField {

    public UseCell[][] field;
    private int size;
    private int maxWire; // numero maximo de fios passando por cada celula
    private MajorityGraph mig;
    
    
    
    private int pattern_pp_out[]= {2,1,4,1,2,3,4,3};
    private int pattern_pi_out[]= {4,3,2,1,4,1,2,3};
    private int pattern_ip_out[]= {4,1,2,3,4,3,2,1};
    private int pattern_ii_out[]= {2,3,4,3,2,1,4,1};
    
    private int pattern_pp_in[]= {2,3,4,3,2,1,4,1};
    private int pattern_pi_in[]= {4,1,2,3,4,3,2,1};
    private int pattern_ip_in[]= {4,3,2,1,4,1,2,3};
    private int pattern_ii_in[]= {2,1,4,1,2,3,4,3};

    
//------------------------------------construtor---------------------------------------------------------------
    
    //construtor do use com a zonas de clock
    public UseField(int size) {
        this.size = size;
        this.field = new UseCell[size][size];
        maxWire=2;
        
        for (int i = 0; i < size; i++) 
            for (int j = 0; j < size; j++) 
                field[i][j]=new UseCell();
        
        //inicializa primeiro elemento do use
        field[0][0].setClockZone(0);
        //inicializa primeira coluna do use
        for (int i = 1; i < size; i++) {
            field[i][0].setClockZone((field[i - 1][0].getClockZone() + 3) % 4);
        }
        //inicializa primeira linha do use
        for (int j = 1; j < size; j++) {
            field[0][j].setClockZone((field[0][j - 1].getClockZone() + 1) % 4);
        }

        for (int i = 1; i < size; i++) {
            for (int j = 1; j < size; j++) {
                if (i % 2 == 0)
                    field[i][j].setClockZone((field[i][j-1].getClockZone() + 1) % 4);
                else
                    field[i][j].setClockZone((field[i][j-1].getClockZone() + 3) % 4);
            }
        }
        
        for (int i = 0; i < size; i++) 
           for (int j = 0; j < size; j++)
               field[i][j].setClockZone(field[i][j].getClockZone()+1);
    }

//------------------------------------END-----------------------------------------------------------------------
    
    
//------------------------------------Get/Set---------------------------------------------------------------
    public int getMaxWire() {
        return maxWire;
    }

    public void setMaxWire(int maxWire) {
        this.maxWire = maxWire;
    }

    public MajorityGraph getMig() {
        return mig;
    }

    public void setMig(MajorityGraph mig) {
        this.mig = mig;
    }
   

    
//----------------------------------------END----------------------------------------------------------------

//------------------------------------pseudo Get/Set---------------------------------------------------------------

    public int getBusy(int x, int y){  
        return field[x][y].getBusy();
    }
    
    public void setBusy(int x, int y, int busy){
        field[x][y].setBusy(busy);
    }

    public int getRelativeDistance(int x, int y){
        if(x<size && y<size)
            return field[x][y].getRelativeDistance();
        else
            System.out.printf("\nvalor (%d,%d) invalido\n",x,y);
        return 0;
    }

//------------------------------------END---------------------------------------------------------------------------
    
//--------------------------reset------------------------------------------------------------------------------------

    //limpa todas celulas do campo
    public void clear(){
        for(int i=0;i<size;i++)
            for(int j=0;j<size;j++){
                this.field[i][j].setBusy(0);
                this.field[i][j].setMajorityNode(null);
                this.field[i][j].setRelativeDistance(0);
                this.field[i][j].setRelativeDistance2(0);
                this.field[i][j].setRelativeLevel(0);
                this.field[i][j].getWiresHere().clear();
            }
    }
    
    //seta em 0 toda relativeDistance no mapa
    public void resetRelativeDistance(){
        for(int i=0;i<size;i++)
            for(int j=0;j<size;j++){
                this.field[i][j].setRelativeDistance(0);
                this.field[i][j].setRelativeDistance2(0);
            }
    }

//----------------------END-reset------------------------------------------------------------------------------------    

    
    public void print() {
        
        System.out.print("   ");
        for (int i = 0; i < size; i++)
            System.out.printf("%-2d ",i);
        System.out.println();
        
        for (int i = 0; i < size; i++) {
            System.out.printf("%-2d|",i);
            for (int j = 0; j < size; j++) {
                if(field[i][j].getBusy()>0){
                    if(field[i][j].getBusy()==5){
                        System.out.printf("%-2s|",field[i][j].getMajorityNode().getName());
                    } else{
                        System.out.printf("%-2s|",field[i][j].getBusy());
                    }
                } else{
                    System.out.printf("%-2s|", "");
                }
                
            }
            System.out.println();
        }
        System.out.println();
    
    }
    
    public String toString() {
        String st=new String();
        
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                    System.out.printf("%-2d|",field[i][j].getRelativeDistance());
            }
            System.out.println();
        }
        System.out.println();
        return st;
    }   

    public double migAreaUsage(){
        
        int x0 = 0;
        int x1 = 0;
        int y0 = 0;
        int y1 = 0;
        double notUse = 0.0;
        double usage =0.0;

 
        if(this.getMig().getMajorityGraph()!=null)
            if (this.getMig().getMajorityGraph().size() > 0) {
                x0=this.getMig().getMajorityGraph().get(0).getPlaceX();
                x1=this.getMig().getMajorityGraph().get(0).getPlaceX();
                y0=this.getMig().getMajorityGraph().get(0).getPlaceY();
                y1=this.getMig().getMajorityGraph().get(0).getPlaceY();
                
                for (int i = 0; i < this.getMig().getMajorityGraph().size(); i++) {
                    for(int j=0;j<this.getMig().getMajorityGraph().get(i).getRoutes().size();j++){
                        for(int k=0;k<this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().size();k++){

                                    if(x0>this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x0=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(x1<this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x1=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(y0>this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y0=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                                    if(y1<this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y1=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                        }
                    }
                }
                    
                for(int i=x0;i<=x1;i++)
                    for(int j=y0;j<=y1;j++)
                        if(this.field[i][j].getBusy()==0)
                            notUse++;
                
               usage = (x1-x0+1)*(y1-y0+1) - notUse;
               
            }
    
    
        return usage;
    }
    
    
    // posiciona e rotea no use com base no MIG passado usando suas as rotas de cada node
    public boolean placeAndRouteMig(){
        
        for(int i=0;i<this.getMig().getMajorityGraph().size();i++)  
            this.getMig().getMajorityGraph().get(i).setPlaced(false);
        
        
        for(int i=0;i<this.getMig().getMajorityGraph().size();i++)  
            if(!this.placeNode(this.getMig().getMajorityGraph().get(i).getName())){
                for(int j=0;j<i;j++)
                    this.unplace(this.getMig().getMajorityGraph().get(j).getName());
                return false;
            }
        
        for(int i=0;i<this.getMig().getMajorityGraph().size();i++)  
            if(!this.routeNode(this.getMig().getMajorityGraph().get(i).getName())){
                for(int j=0;j<i;j++)
                    this.unrouteNode(this.getMig().getMajorityGraph().get(j).getName());
                return false;
            }
        return true;
    }
    
    public void unplaceAndUnrouteMig(){
            for(int i=0;i<this.getMig().getMajorityGraph().size();i++)  
            this.unplaceAndUnrouteNode(this.getMig().getMajorityGraph().get(i).getName());
    }
    
    // retorna o nivel relativo da celula (i,j) para a celula (i0,j0)
    public int RelativeLevel(int i0, int j0, int i, int j) {
        if (abs(i - i0)>=abs(j - j0)) {
            return abs(i - i0);
        } else {
            return abs(j - j0);
        }
    }

    
    // retorna todos os pontos que estao a distancia dist0 de i0,j0 e dist1 de i1,j1
    // orientation indica se os pontos devem ter caminhos saindo ou chegando na celula
    public Vector<Point> sameDistance(int i0, int j0, int dist0, String orientation0, int i1, int j1, int dist1, String orientation1){
       Vector<Point> points = new Vector();
       
       this.resetRelativeDistance();
       this.startRelativeDistance(i1, j1, dist1, orientation1);
       
       for(int i=0;i<size;i++)
        for(int j=0;j<size;j++)
            field[i][j].setRelativeDistance2(field[i][j].getRelativeDistance());
       
        this.startRelativeDistance(i0, j0, dist0, orientation0);
       
        for(int i=0;i<size;i++)
            for(int j=0;j<size;j++)
                if(field[i][j].getRelativeDistance()==dist0 && field[i][j].getRelativeDistance2()==dist1)
                 if(field[i][j].getBusy()==0){
                    points.add(new Point(i,j));
                }
        
        return points;
    }

    // retorna todas as rotas validas do destino para origem respeirando o limite maximo de fios por celula
    public Vector<Route> validRoutes(Point source, Point target) {

        Route path = new Route();
        Vector<Route> validRoute = new Vector<Route>();

        // terceiro parametro alterado de size para isso, para otimizar as operacoes
        startRelativeDistance(target.getX(), target.getY(), 2+this.RelativeLevel(target.getX(), target.getY(), source.getX(), source.getY()), "in" );
        
        path.getRoute().add(source);
        hasRoute(source, source, target, path, validRoute, false);
        return validRoute;
    }
    
        // retorna todos os pontos possiveis para posicionamento baseado nos vizinhos ja posicionados e nas celulas ja ocupadas
    public Vector<Point> validPlace(String node){
    
        Vector<String> placed = new Vector<String>(); // vizinhos posicionados
        Vector<Point> points = new Vector(); // possibilidades de posicao valida
        if(mig.getNode(node)!=null)
        if(!mig.getNode(node).isPlaced()){
            placed=this.placedNeighbors(node);
            
            //acha todos pontos possiveis para o posicionamento do node
            int lw1=0; //distancia que o no deve estar do primeiro vizinho ja posicionado
            int lw2=0; //distancia que o no deve estar do segundo vizinho ja posicionado
            int subSetCount=0; // conta o numero de combinacao de vizinhos posicionados 2 a 2, para posicionar o node é necessario satisfazer as distancias de todas combinacoes de vizinhos
                
            
            
            if(placed.size()==1){
                
                    lw1=0;
                    
                    if(this.getMig().getNode(placed.get(0)).getInput().contains(node) )
                        for(int n=0;n<abs(mig.getNode(placed.get(0)).getLevel() - mig.getNode(node).getLevel());n++)
                            lw1=lw1+mig.getLevelWeight().get(mig.getNode(node).getLevel()-(n)  -2 );
                    else
                        for(int n=0;n<abs(mig.getNode(placed.get(0)).getLevel() - mig.getNode(node).getLevel());n++)
                            lw1=lw1+mig.getLevelWeight().get(mig.getNode(placed.get(0)).getLevel()-(n)  -2 );
                        
                    //adiciona todos pontos respeitando a distancia
                    if( mig.getNode(placed.get(0)).getInput().contains(node) ){ // se é um input
                        points.addAll( sameDistance( mig.getNode(placed.get(0)).getPlaceX(), mig.getNode(placed.get(0)).getPlaceY(), lw1, "in", mig.getNode(placed.get(0)).getPlaceX(), mig.getNode(placed.get(0)).getPlaceY(), lw1, "in"));
                    }else{ // se é uma output de node
                        points.addAll( sameDistance( mig.getNode(placed.get(0)).getPlaceX(), mig.getNode(placed.get(0)).getPlaceY(), lw1, "out", mig.getNode(placed.get(0)).getPlaceX(), mig.getNode(placed.get(0)).getPlaceY(), lw1, "out"));
                    }
            }
            else if(placed.size()>0){
                // para todas combinacoes 2a2 de vizinhos posicionados seleciona os pontos que atendem as restricoes de distancia da combinacao 2 a 2
                for(int i=0;i<placed.size();i++)
                    for(int j=i+1;j<placed.size();j++){
                        subSetCount++;
                        lw1=0;
                        if(this.getMig().getNode(placed.get(i)).getInput().contains(node) )
                            for(int n=0;n<abs(mig.getNode(placed.get(i)).getLevel() - mig.getNode(node).getLevel());n++)
                                lw1=lw1+mig.getLevelWeight().get(mig.getNode(node).getLevel()-(n)  -2 );
                        else
                            for(int n=0;n<abs(mig.getNode(placed.get(i)).getLevel() - mig.getNode(node).getLevel());n++)
                                lw1=lw1+mig.getLevelWeight().get(mig.getNode(placed.get(i)).getLevel()-(n)  -2 );
                       
                        lw2=0;
                        if(this.getMig().getNode(placed.get(j)).getInput().contains(node) )
                            for(int n=0;n<abs(mig.getNode(placed.get(j)).getLevel() - mig.getNode(node).getLevel());n++)
                                lw2=lw2+mig.getLevelWeight().get(mig.getNode(node).getLevel()-(n)  -2 );
                        else
                            for(int n=0;n<abs(mig.getNode(placed.get(j)).getLevel() - mig.getNode(node).getLevel());n++)
                                lw2=lw2+mig.getLevelWeight().get(mig.getNode(placed.get(j)).getLevel()-(n)  -2 );
                        if( mig.getNode(placed.get(i)).getInput().contains(node) ){ // se é um input
                            if(mig.getNode(placed.get(j)).getInput().contains(node) )
                                points.addAll(sameDistance( mig.getNode(placed.get(i)).getPlaceX(), mig.getNode(placed.get(i)).getPlaceY(), lw1 ,"in",mig.getNode(placed.get(j)).getPlaceX(), mig.getNode(placed.get(j)).getPlaceY(), lw2,"in"));
                            else
                                points.addAll(sameDistance( mig.getNode(placed.get(i)).getPlaceX(), mig.getNode(placed.get(i)).getPlaceY(), lw1 ,"in",mig.getNode(placed.get(j)).getPlaceX(), mig.getNode(placed.get(j)).getPlaceY(), lw2,"out"));
                        }else{
                            if(mig.getNode(placed.get(j)).getInput().contains(node) )
                                points.addAll(sameDistance( mig.getNode(placed.get(i)).getPlaceX(), mig.getNode(placed.get(i)).getPlaceY(), lw1 ,"out",mig.getNode(placed.get(j)).getPlaceX(), mig.getNode(placed.get(j)).getPlaceY(), lw2,"in"));
                            else
                                points.addAll(sameDistance( mig.getNode(placed.get(i)).getPlaceX(), mig.getNode(placed.get(i)).getPlaceY(), lw1 ,"out",mig.getNode(placed.get(j)).getPlaceX(), mig.getNode(placed.get(j)).getPlaceY(), lw2,"out"));
                        }
                    }

                   
                
                    Vector<Point> aux=new Vector<Point>();
                    int rpt=0;
                    for(int i=0;i<points.size();i++){
                        rpt=0;
                        for(int j=0;j<points.size();j++){
                            if(points.get(i).getX()==points.get(j).getX() && points.get(i).getY()==points.get(j).getY())
                                rpt++;
                            }
                        // garante que o ponto atende as restricoes de todos pontos ja posicionados que sao vizinhos
                        if(rpt>=subSetCount){
                            aux.add(points.get(i));
                        }

                        //remove repeticoes em aux
                        for(int u=0;u<aux.size();u++)
                            for(int v=u+1;v<aux.size();v++)
                                if(aux.get(u).equal(aux.get(v)))
                                    aux.remove(v);
                    }
                    points.clear();
                    points.addAll(aux);

            }
        }
        return points; 
    }
    
    
//------------------(un)PLACE-(un)ROUTE-------------------------------------------------------------------------------------------------------
    //retorna true se a posicao x,y esta livre e o node ainda nao foi posicionado
    public boolean place(String node, int x, int y){
       if(mig.getNode(node)!=null)// se o node existe no grafo
           if( (this.getBusy(x, y)==0 && !this.getMig().getNode(node).isPlaced()) || (this.field[x][y].getMajorityNode()!=null && this.field[x][y].getMajorityNode().getName().equals(node))  ){ // se a posicao esta livre para posicionar
                mig.getNode(node).setPlaced(true);
                mig.getNode(node).setPlaceX(x);
                mig.getNode(node).setPlaceY(y);
                field[x][y].setMajorityNode(mig.getNode(node));
                setBusy(x, y, 5);
                this.field[x][y].getWiresHere().clear();
                return true;
           }
       return false;
    }
    
    public boolean placeNode(String node){
        if(mig.getNode(node)!=null)
            if(this.place(node, this.getMig().getNode(node).getPlaceX(), this.getMig().getNode(node).getPlaceY()) )
                return true;
        return false;
    }
    
    public boolean routeNode(String node){
        if(mig.getNode(node)!=null)
            for(int i=0;i<this.getMig().getNode(node).getRoutes().size(); i++)
                if(!this.route(this.getMig().getNode(node).getRoutes().get(i))){
                    for(int j=0;j<i;j++)
                        this.unroute(this.getMig().getNode(node).getRoutes().get(j));
                    return false;        
                }
        return true;
    }
    
    public void unrouteNode(String node){
        if(mig.getNode(node)!=null)
            for(int i=0;i<this.getMig().getNode(node).getRoutes().size(); i++)
                this.unroute(this.getMig().getNode(node).getRoutes().get(i));
    }
    
    public void unplace(String node){
        
      if(mig.getNode(node)!=null)// se o node existe no grafo{
        if(this.mig.getNode(node).isPlaced()){//se o no esta posicionado     
            setBusy(mig.getNode(node).getPlaceX(), mig.getNode(node).getPlaceY(), 0);
            mig.getNode(node).setPlaced(false);
            field[mig.getNode(node).getPlaceX()][mig.getNode(node).getPlaceY()].setMajorityNode(null);
            
        }
    }
    
    //retorna true se o roteamento foi realizado respeitando o limite maximo de fios por celula e false se nada foi feito
    public boolean route(Route path){
        boolean b=true;
        if(path!=null){
            for(int i=1;i<path.getRoute().size()-1;i++){
                if( this.getBusy(path.getRoute().get(i).getX(), path.getRoute().get(i).getY())>=maxWire){
                    if(this.field[path.getRoute().get(i).getX()][path.getRoute().get(i).getY()].getMajorityNode()==null){
                        if(!this.field[path.getRoute().get(i).getX()][path.getRoute().get(i).getY()].isWireHere(path.getRoute().get(0)) )
                                b=false;
                    }else{
                        b=false;
                    }
                }
            }
            if(b){
                for(int i=1;i<path.getRoute().size()-1;i++){
                    if(!this.field[path.getRoute().get(i).getX()][path.getRoute().get(i).getY()].isWireHere(path.getRoute().get(0))){
                        this.setBusy(path.getRoute().get(i).getX(),path.getRoute().get(i).getY(),getBusy(path.getRoute().get(i).getX(),path.getRoute().get(i).getY())+1);
                        this.field[path.getRoute().get(i).getX()][path.getRoute().get(i).getY()].getWiresHere().add(new Point(path.getRoute().get(0)));
                    }
                }
            }
            return b;
        }
        return false;
    }
    
    
    
    
    public void unroute(Route path){
        if(path!=null)
            for(int i=1;i<path.getRoute().size()-1;i++){
                if(this.field[path.getRoute().get(i).getX()][path.getRoute().get(i).getY()].isWireHere(path.getRoute().get(0))){
                    setBusy(path.getRoute().get(i).getX(),path.getRoute().get(i).getY(),getBusy(path.getRoute().get(i).getX(),path.getRoute().get(i).getY())-1);
                    this.field[path.getRoute().get(i).getX()][path.getRoute().get(i).getY()].removeWireHere(path.getRoute().get(0));
                }
            }
    }
//----------------------------END-----------------------------------------------------------------------------------------------------
    

    //inicia allroutes com todas solucoes possiveis e routes com a primeira solucao possivel
    public void initNodeRoutes(String node){
        this.getMig().getNode(node).setAllRoutes(this.allValidRoutes(node));
            if(this.getMig().getNode(node).getAllRoutes().size()>0){
                this.getMig().getNode(node).getRoutes().clear();
                for(int n=0;n<this.placedNeighbors(node).size();n++)
                    this.getMig().getNode(node).addRoute(this.getMig().getNode(node).getAllRoutes().get(n).get(0));
            }
            else{
                this.getMig().getNode(node).getRoutes().removeAllElements();
            
            }
    }
   
    public void hasRoute(Point source, Point current, Point target, Route path, Vector<Route> validRoute , boolean has) {
        
        Route aux;
        if (!has) {
            // se chegar a 1 celula de distancia do target entao a rota foi encontrada
            if (getRelativeDistance(current.getX(), current.getY()) == 1) {
                has = true;
                aux = new Route();
                aux.addAll(path);
                aux.getRoute().add(target);
                validRoute.add(aux);
            } else { // senao, procura se nos vizinhos por algum que diminua a distancia ate o target

                if (current.getX() > 0) {
                    if (getRelativeDistance(current.getX(), current.getY()) == getRelativeDistance(current.getX() - 1, current.getY()) + 1   ) { // se a distancia do vizinho e 1 unidade menor entao existe caminho 
                            if (getBusy(current.getX() - 1, current.getY()) < maxWire) { // se a celula nao esta ocupada com o maximo de fios
                                path.getRoute().add(new Point(current.getX() - 1, current.getY()));
                                hasRoute(source, new Point(current.getX() - 1, current.getY()), target,path, validRoute , false);                                                    //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }
                            }else if(this.field[current.getX() - 1][ current.getY()].isWireHere(source)){
                                path.getRoute().add(new Point(current.getX() - 1, current.getY()));
                                hasRoute(source, new Point(current.getX() - 1, current.getY()), target,path, validRoute , false);                                                    //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }
                            }
                    }
                }

                if (current.getY() > 0) {
                    if (getRelativeDistance(current.getX(), current.getY()) == getRelativeDistance(current.getX(), current.getY() - 1) + 1) {

                        if (getRelativeDistance(current.getX(), current.getY() - 1) != 1) {
                            if ( (getBusy(current.getX(), current.getY() - 1) < maxWire || this.field[current.getX()][ current.getY()-1].isWireHere(source) ) &&  this.field[current.getX()][ current.getY()-1].getMajorityNode()==null) {
                                path.getRoute().add(new Point(current.getX(), current.getY() - 1));
                                hasRoute(source, new Point(current.getX(), current.getY() - 1), target, path, validRoute , false);                                                    //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }

                            }
                        } else {
                            if ((getBusy(current.getX(), current.getY() - 1) < maxWire || this.field[current.getX()][ current.getY()-1].isWireHere(source) ) &&  this.field[current.getX() ][ current.getY()-1].getMajorityNode()==null) {
                                path.getRoute().add(new Point(current.getX(), current.getY() - 1));
                                hasRoute(source, new Point(current.getX(), current.getY() - 1), target, path, validRoute , false);                                                    //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }
                            }
                        }

                    }
                }

                if (current.getX() < size - 1) {
                    if (getRelativeDistance(current.getX(), current.getY()) == getRelativeDistance(current.getX() + 1, current.getY()) + 1) {

                        if (getRelativeDistance(current.getX() + 1, current.getY()) != 1) {
                            if ((getBusy(current.getX() + 1, current.getY()) < maxWire || this.field[current.getX() + 1][ current.getY()].isWireHere(source)) &&  this.field[current.getX() + 1][ current.getY()].getMajorityNode()==null) {
                                path.getRoute().add(new Point(current.getX() + 1, current.getY()));
                                hasRoute(source, new Point(current.getX() + 1, current.getY()), target, path, validRoute , false);                                                    //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }

                            }
                        } else {
                            if ((getBusy(current.getX() + 1, current.getY()) < maxWire || this.field[current.getX() + 1][ current.getY()].isWireHere(source)) &&  this.field[current.getX() + 1][ current.getY()].getMajorityNode()==null) {
                                path.getRoute().add(new Point(current.getX() + 1, current.getY()));
                                hasRoute(source, new Point(current.getX() + 1, current.getY()), target, path, validRoute , false);                                                    //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }

                            }

                        }
                    }
                }

                if (current.getY() < size - 1) {
                    if (getRelativeDistance(current.getX(), current.getY()) == getRelativeDistance(current.getX(), current.getY() + 1) + 1) {

                        if (getRelativeDistance(current.getX(), current.getY() + 1) != 1) {
                            if ((getBusy(current.getX(), current.getY() + 1) < maxWire || this.field[current.getX()][ current.getY()+1].isWireHere(source)) &&  this.field[current.getX()][ current.getY()+1].getMajorityNode()==null) {
                                path.getRoute().add(new Point(current.getX(), current.getY() + 1));
                                hasRoute(source, new Point(current.getX(), current.getY() + 1), target, path, validRoute , false);
                                //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }
                            }
                        } else {
                            if ((getBusy(current.getX(), current.getY() + 1) < maxWire|| this.field[current.getX() ][ current.getY()+1].isWireHere(source)) &&  this.field[current.getX()][ current.getY()+1].getMajorityNode()==null) {
                                path.getRoute().add(new Point(current.getX(), current.getY() + 1));
                                hasRoute(source, new Point(current.getX(), current.getY() + 1), target, path, validRoute , false);
                                //se nao houver caminho ele retira as posicoes de caminho invalidas
                                if (!path.getRoute().isEmpty()) {
                                    path.getRoute().remove(path.getRoute().lastElement());
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    
    
    
    
    
    
    //altera as rotas do node no mig para a proxima solucao valida desde que haja uma solucao em routes
    public void nextSolution(String node){
        if(mig.getNode(node)!=null){// se o node existe no grafo
            //altera a rota sem alterar o posicionamento
            if(this.getMig().getNode(node).getAllRoutes().size()>=this.placedNeighbors(node).size()){
                for (int i = 0; i < this.placedNeighbors(node).size(); i++) {
                    if(this.getMig().getNode(node).getAllRoutes().get(i).size()>1)
                        for (int j = 0; j < this.getMig().getNode(node).getAllRoutes().get(i).size(); j++) {
                            for (int n = 0; n < this.getMig().getNode(node).getRoutes().size(); n++)
                                if(this.getMig().getNode(node).getRoutes().get(n).equal(this.getMig().getNode(node).getAllRoutes().get(i).get(j))){
                                    this.getMig().getNode(node).getRoutes().clear();
                                    this.getMig().getNode(node).getAllRoutes().get(i).remove(j);
                                    if(this.getMig().getNode(node).getAllRoutes().size()>=this.placedNeighbors(node).size())
                                        for(int k=0;k<this.placedNeighbors(node).size();k++)
                                            this.getMig().getNode(node).addRoute(this.getMig().getNode(node).getAllRoutes().get(k).get(0));
                                    if(this.getMig().getNode(node).getRoutes().get(0).getRouteOrientation().equals("io")){
                                        this.getMig().getNode(node).setPlaceX(this.getMig().getNode(node).getRoutes().get(0).getRoute().get(0).getX());
                                        this.getMig().getNode(node).setPlaceY(this.getMig().getNode(node).getRoutes().get(0).getRoute().get(0).getY());
                                        return;
                                    }else{
                                        this.getMig().getNode(node).setPlaceX(this.getMig().getNode(node).getRoutes().get(0).reverse().getRoute().get(0).getX());
                                        this.getMig().getNode(node).setPlaceY(this.getMig().getNode(node).getRoutes().get(0).reverse().getRoute().get(0).getY());
                                        return;
                                    }
                                }
                        }
                }
            }
            
            
            
                                        
            //altera a rota e o posicionamento
            for (int n = 0; n < this.getMig().getNode(node).getRoutes().size(); n++) {
                for (int i = 0; i < this.getMig().getNode(node).getAllRoutes().size(); i++) {
                    for (int j = 0; j < this.getMig().getNode(node).getAllRoutes().get(i).size(); j++) {
                        if(this.getMig().getNode(node).getRoutes().get(n).equal(this.getMig().getNode(node).getAllRoutes().get(i).get(j))){ // se achar um caminho em allrotes igual ao caminho ja roteado
                            this.getMig().getNode(node).getAllRoutes().get(i).remove(j);
                            //remove colecao de rotas vazia
                            if(this.getMig().getNode(node).getAllRoutes().get(i).size()==0)
                                this.getMig().getNode(node).getAllRoutes().remove(i);
                            if(this.placedNeighbors(node).size()>0)
                            for(int k=0;k<this.getMig().getNode(node).getAllRoutes().size()%this.placedNeighbors(node).size();k++)
                                this.getMig().getNode(node).getAllRoutes().remove(k);
                            //impede o for mais interno de acessar posicoes invalidas
                            if(i>=this.getMig().getNode(node).getAllRoutes().size())
                                break;
                        }
                    }
                }
            }
            
            this.getMig().getNode(node).getRoutes().clear();
            if(this.getMig().getNode(node).getAllRoutes().size()>=this.placedNeighbors(node).size()){
                for(int i=0;i<this.placedNeighbors(node).size();i++)
                    this.getMig().getNode(node).addRoute(this.getMig().getNode(node).getAllRoutes().get(i).get(0));
            
                
                if(this.getMig().getNode(node).getRoutes().get(0).getRouteOrientation().equals("io")){
                    this.getMig().getNode(node).setPlaceX(this.getMig().getNode(node).getRoutes().get(0).getRoute().get(0).getX());
                    this.getMig().getNode(node).setPlaceY(this.getMig().getNode(node).getRoutes().get(0).getRoute().get(0).getY());
                }else{
                    this.getMig().getNode(node).setPlaceX(this.getMig().getNode(node).getRoutes().get(0).reverse().getRoute().get(0).getX());
                    this.getMig().getNode(node).setPlaceY(this.getMig().getNode(node).getRoutes().get(0).reverse().getRoute().get(0).getY());
                }
            }
            
            //while(this.placedInputs(node).size()>0)
            //    this.recursiveUnplace(this.placedInputs(node).get(0));
        }
    }
    
    
    //faz o unplace e unroute do no e todos os nos abaixo dele
    public void recursiveUnplace(String node){
        
      boolean aux=true;  
        
      if(mig.getNode(node)!=null)// se o node existe no grafo{
        if(this.mig.getNode(node).isPlaced()){//se o no esta posicionado    
            
            
            for(int i=0;i<this.getMig().getNode(node).getRoutes().size();i++)
                for(int j=1;j<this.getMig().getNode(node).getRoutes().get(i).getRoute().size()-1;j++)
                    setBusy(this.getMig().getNode(node).getRoutes().get(i).getRoute().get(j).getX(),this.getMig().getNode(node).getRoutes().get(i).getRoute().get(j).getY(),getBusy(this.getMig().getNode(node).getRoutes().get(i).getRoute().get(j).getX(),this.getMig().getNode(node).getRoutes().get(i).getRoute().get(j).getY())-1);
            
            this.getMig().getNode(node).getRoutes().clear();
            
            while(this.placedNeighbors(node).size()>0){
                recursiveUnplace(placedNeighbors(node).get(0));
            }

            
            mig.getNode(node).setPlaced(false);
            setBusy(mig.getNode(node).getPlaceX(), mig.getNode(node).getPlaceY(), 0);
            field[mig.getNode(node).getPlaceX()][mig.getNode(node).getPlaceY()].setMajorityNode(null);
            
        }
    }
    
    
    
    
    

    
    // rotea um node e o posiciona no inicio da rota caso nao consiga desfaz o roteamento
    public boolean placeAndRouteNode(String node){
        
        if(this.getMig().getNode(node)!=null){
            
            
            if(this.getMig().getNode(node).getRoutes().size()==0)//nao existe rotas entao nao se pode rotear e posicionar
                return false;
            
            // posiciona segundo a orientação da rota -------------
            if(this.getMig().getNode(node).getRoutes().get(0).getRouteOrientation().equals("io")){
                if(!this.place(node, this.getMig().getNode(node).getRoutes().get(0).getRoute().get(0).getX(), this.getMig().getNode(node).getRoutes().get(0).getRoute().get(0).getY())){
                    return false;
                }
            }else{
                if(!this.place(node, this.getMig().getNode(node).getRoutes().get(0).reverse().getRoute().get(0).getX(), this.getMig().getNode(node).getRoutes().get(0).reverse().getRoute().get(0).getY()))
                    return false;
            }
            // ------------------------------------------------------
            
            for(int i=0;i<this.getMig().getNode(node).getRoutes().size();i++)
                if(!this.route(this.getMig().getNode(node).getRoutes().get(i))){
                    for(int j=0;j<i;j++)
                        this.unroute(this.getMig().getNode(node).getRoutes().get(j));
                    this.unplace(node);
                    return false;
                }
            return true;
        }
        return false;
    }
    
    //desposicioo rotena e desrotea se o no esta posicionado
    public void unplaceAndUnrouteNode(String node){
        
        if(this.getMig().getNode(node)!=null){
           if(this.getMig().getNode(node).isPlaced()){ 
            
               /*
            Route aux= new Route();
            for(int i=0;i<this.getMig().getNode(node).getRoutes().size();i++)
                aux.merge(this.getMig().getNode(node).getRoutes().get(i));
            if(this.getMig().getNode(node).getRoutes().size()>0)
                aux.getRoute().add(this.getMig().getNode(node).getRoutes().get(0).getRoute().get(0));
               */
            for(int i=0;i<this.getMig().getNode(node).getRoutes().size();i++)
                this.unroute(this.getMig().getNode(node).getRoutes().get(i));
            this.unplace(node);
           }
        }
    }
    
    


    
    
    
//----------------------------------Placed Unplaced Neighbors-----------------------------------------------------------------------------
    //retorna todos vizinhos posicionados
    public Vector<String> placedNeighbors(String node) {
        Vector<String> placed = new Vector<String>();
        //todos vizinhos do node que estao posicionados tanto inputs quanto outputs----------------------------------------------
        for (int i = 0; i < mig.getNode(node).getInput().size(); i++) {
            if (mig.getNode(mig.getNode(node).getInput().get(i)) != null) {
                if (mig.getNode(mig.getNode(node).getInput().get(i)).isPlaced()) {
                    if (!placed.contains(mig.getNode(mig.getNode(node).getInput().get(i)).getLevel())) {
                        placed.add(mig.getNode(mig.getNode(node).getInput().get(i)).getName());
                    }
                }
            }
        }

        for (int i = 0; i < mig.getNode(node).getOutput().size(); i++) {
            if (mig.getNode(mig.getNode(node).getOutput().get(i)) != null) {
                if (mig.getNode(mig.getNode(node).getOutput().get(i)).isPlaced()) {
                    if (!placed.contains(mig.getNode(mig.getNode(node).getOutput().get(i)).getLevel())) {
                        placed.add(mig.getNode(mig.getNode(node).getOutput().get(i)).getName());
                    }
                }
            }
        }
        return placed;
    }
    
    public Vector<String> placedOutputs(String node) {
        Vector<String> placed = new Vector<String>();
        //todos vizinhos do node que estao posicionados tanto inputs quanto outputs----------------------------------------------
        for (int i = 0; i < mig.getNode(node).getOutput().size(); i++) {
            if (mig.getNode(mig.getNode(node).getOutput().get(i)) != null) {
                if (mig.getNode(mig.getNode(node).getOutput().get(i)).isPlaced()) {
                    if (!placed.contains(mig.getNode(mig.getNode(node).getOutput().get(i)).getLevel())) {
                        placed.add(mig.getNode(mig.getNode(node).getOutput().get(i)).getName());
                    }
                }
            }
        }
        return placed;
    }
    
    public Vector<String> placedInputs(String node) {
        Vector<String> placed = new Vector<String>();
        //todos vizinhos do node que estao posicionados tanto inputs quanto outputs----------------------------------------------
        for (int i = 0; i < mig.getNode(node).getInput().size() ; i++) {
            if (mig.getNode(mig.getNode(node).getInput().get(i)) != null) {
                if (mig.getNode(mig.getNode(node).getInput().get(i)).isPlaced()) {
                    if (!placed.contains(mig.getNode(mig.getNode(node).getInput().get(i)).getLevel())) {
                        placed.add(mig.getNode(mig.getNode(node).getInput().get(i)).getName());
                    }
                }
            }
        }
        return placed;
    }
    
    public Vector<String> unplacedNeighbors(String node) {
        Vector<String> unplaced = new Vector<String>();
        //todos vizinhos do node que estao posicionados tanto inputs quanto outputs----------------------------------------------
        for (int i = 0; i < mig.getNode(node).getInput().size(); i++) {
            if (mig.getNode(mig.getNode(node).getInput().get(i)) != null) {
                if (!mig.getNode(mig.getNode(node).getInput().get(i)).isPlaced()) {
                    if (!unplaced.contains(mig.getNode(mig.getNode(node).getInput().get(i)).getLevel())) {
                        unplaced.add(mig.getNode(mig.getNode(node).getInput().get(i)).getName());
                    }
                }
            }
        }

        for (int i = 0; i < mig.getNode(node).getOutput().size(); i++) {
            if (mig.getNode(mig.getNode(node).getOutput().get(i)) != null) {
                if (!mig.getNode(mig.getNode(node).getOutput().get(i)).isPlaced()) {
                    if (!unplaced.contains(mig.getNode(mig.getNode(node).getOutput().get(i)).getLevel())) {
                        unplaced.add(mig.getNode(mig.getNode(node).getOutput().get(i)).getName());
                    }
                }
            }
        }
        return unplaced;
    }
    
    public Vector<String> unplacedOutputs(String node) {
        Vector<String> unplaced = new Vector<String>();
        //todos vizinhos do node que estao posicionados tanto inputs quanto outputs----------------------------------------------
        
        for (int i = 0; i < mig.getNode(node).getOutput().size(); i++) {
            if (mig.getNode(mig.getNode(node).getOutput().get(i)) != null) {
                if (!mig.getNode(mig.getNode(node).getOutput().get(i)).isPlaced()) {
                    if (!unplaced.contains(mig.getNode(mig.getNode(node).getOutput().get(i)).getLevel())) {
                        unplaced.add(mig.getNode(mig.getNode(node).getOutput().get(i)).getName());
                    }
                }
            }
        }
        return unplaced;
    }
    
    public Vector<String> unplacedInputs(String node) {
        Vector<String> unplaced = new Vector<String>();
        //todos vizinhos do node que estao posicionados tanto inputs quanto outputs----------------------------------------------
        for (int i = 0; i < mig.getNode(node).getInput().size(); i++) {
            if (mig.getNode(mig.getNode(node).getInput().get(i)) != null) {
                if (!mig.getNode(mig.getNode(node).getInput().get(i)).isPlaced()) {
                    if (!unplaced.contains(mig.getNode(mig.getNode(node).getInput().get(i)).getLevel())) {
                        unplaced.add(mig.getNode(mig.getNode(node).getInput().get(i)).getName());
                    }
                }
            }
        }

        return unplaced;
    }
//----------------------------------END-----------------------------------------------------------------------------    

    
    
    
    
    
    //retorna todas rotas paras quais ha caminho para todos vizinhos ja posicionados
    public Vector<Vector<Route>> allValidRoutes(String node){
        Vector<Vector<Route>> allValidRoutes = new Vector<Vector<Route>>();
        Vector<String> placed=this.placedNeighbors(node);
        Vector<Point> validPoints=this.validPlace(node);       
        Vector<Vector<Route>> auxRoutes;
        
        Vector<Route> reverse= new Vector <Route>();
        
        
        
        for(int i=0;i<validPoints.size();i++){
            auxRoutes = new Vector<Vector<Route>>();
            for(int j=0;j<placed.size();j++){
                if(this.getBusy(validPoints.get(i).getX(), validPoints.get(i).getY())==0){
                    
                    if(this.getMig().getNode(placed.get(j)).getInput().contains(node)){
                        if(this.validRoutes(new Point(validPoints.get(i).getX(), validPoints.get(i).getY()), new Point (mig.getNode(placed.get(j)).getPlaceX(), mig.getNode(placed.get(j)).getPlaceY())).size()>0)
                            auxRoutes.add(this.validRoutes(new Point(validPoints.get(i).getX(), validPoints.get(i).getY()), new Point (mig.getNode(placed.get(j)).getPlaceX(), mig.getNode(placed.get(j)).getPlaceY())));
                    }else{
                            reverse=this.validRoutes( new Point (mig.getNode(placed.get(j)).getPlaceX(), mig.getNode(placed.get(j)).getPlaceY()), new Point(validPoints.get(i).getX(), validPoints.get(i).getY())  );
                            for(int k=0;k<reverse.size();k++)
                                reverse.get(k).setRouteOrientation("oi");
                            if(reverse.size()>0){
                              auxRoutes.add(reverse);
                            }
                    }
                    
                    
                }
            }
            if(auxRoutes.size()==placed.size())
                allValidRoutes.addAll(auxRoutes);
        }
        
        return allValidRoutes;
    }
    
    // numero de rotas possiveis 
    public int possibilitiesNumber(String node) {
        int mult = 1;
        int sum = 0;
        for (int i = 0; i < this.allValidRoutes(node).size(); i++) {
            mult = mult * this.allValidRoutes(node).get(i).size();
            if ((i + 1) % (this.placedNeighbors(node).size()) == 0) {
                sum = sum + mult;
                mult = 1;
            }
        }
        return sum;
    }
    
    // passando v3 com a quantidade de caminhos para cada no ex[3,1,2], essa funcao gera todas combinacoes possiveis
    public void combination(Vector<Vector<Vector<Integer>>> v3){
        
        Vector<Vector<Integer>> v2 = new Vector<Vector<Integer>>();
        Vector v1=new Vector();
        
        //if(v3.get(0).size()>100000)
        //   return;
        
        
        if(v3.size()>1){
          if(v3.get(0).size()>0){
            if (v3.get(0).get(0).size() == 1) {
                v2 = new Vector<Vector<Integer>>();
                for (int i = 0; i < v3.get(0).get(0).get(0); i++) {
                    for (int j = 0; j < v3.get(1).get(0).get(0); j++) {
                        v1=new Vector();
                        v1.add(i);
                        v1.add(j);
                        v2.add(v1);
                    }
                }
                v3.set(0, v2);
            }
            else if (v3.get(0).get(0).size() > 1){
                v2 = new Vector<Vector<Integer>>();
                for (int i = 0; i < v3.get(0).size(); i++) {
                    for (int j = 0; j < v3.get(1).get(0).get(0); j++) {
                        v1=new Vector();
                        v1.addAll(v3.get(0).get(i));
                        v1.add(j);
                        v2.add(v1);
                    }
                }
                v3.set(0, v2);
            }
        
           v3.remove(1);
           combination(v3);
          }
        }
        
        
        if(v3.size()==1){
            if(v3.get(0).size()==1){
            
                v2 = new Vector<Vector<Integer>>();
                for (int j = 0; j < v3.get(0).get(0).get(0); j++) {
                        v1=new Vector();
                        v1.add(j);
                        v2.add(v1);
                }
                v3.set(0, v2);
            }
        }
        
    }
    

    
//--------------------------------------RelativeDistance-------------------------------------------------------------------------------------------------------------------------    
// USA A VARIAVEL DISTANCEORIENTATION PARA CALCULAR A DISTANCIA RELATIVA PODENDO SER IN OU OUT
    
    //atualiza a distancia relativa, ate o level passado, ao redor de i0,j0
    public void startRelativeDistance(int i0, int j0, int level, String orientation) {
        
        for (int j = 0; j < size; j++) 
           for (int i = 0; i < size; i++)
               field[i][j].setRelativeDistance(0);
        
        // atualiza o level relativo de todos elementos
        for (int l = 0; l <=level; l++) { // percorre niveis ai redor do inicial
            for (int j = -l; j <= l; j++) {
                for (int i = -l; i <= l; i++) {
                    if ((i + i0) >= 0 && (i + i0) < size && (j + j0) >= 0 && (j + j0) < size) { // garente que nao saia do field
                        if (abs(abs(i + i0) - i0) < (l + 1) && abs(abs(j + j0) - j0) < (l + 1)) { // se nao ultrapassa o nivel atual
                            if (abs(abs(i + i0) - i0) == l || abs(abs(j + j0) - j0) == l) { 
                                    field[abs(i + i0)][abs(j + j0)].setRelativeLevel(RelativeLevel(i0,j0,abs(i + i0),abs(j + j0)));
                            }
                        }
                    }
                }
            }
        }
        
        //atualiza distancias relativa de todas celulas
        for (int l = 0; l <=level; l++) { // percorre niveis ai redor do inicial
            for (int j = -l; j <= l; j++) {
                for (int i = -l; i <= l; i++) {
                    if ((i + i0) >= 0 && (i + i0) < size && (j + j0) >= 0 && (j + j0) < size) { // garente que nao saia do field
                        if (abs(abs(i + i0) - i0) < (l + 1) && abs(abs(j + j0) - j0) < (l + 1)) { // se nao ultrapassa o nivel atual
                            if (abs(abs(i + i0) - i0) == l || abs(abs(j + j0) - j0) == l) { 
                                if(orientation.equals("in"))
                                    relativeInDistance(abs(i + i0),abs(j + j0));
                                if(orientation.equals("out"))
                                    relativeOutDistance(abs(i + i0),abs(j + j0));
                            }
                        }
                    }
                }
            }
        }
        

    }

    // atualiza a distancia relativa de da celula (i0,j0) baseada nas distancias atualizadas do seu nivel e atualiza o proximo nivel na vizinhanca de (i0,j0)
    // obrigatorio o relativeLevel estar setado previamente
    private void relativeOutDistance(int i0, int j0){
            //System.out.println("("+i0+","+j0+") " + field[i0][j0].getRelativeLevel()+ " " + relativeLevel(i0,j0,i0-1,j0-1) );
            if (i0 % 2 == 1 && j0 % 2 == 1) {
                
                if(j0<size-1)
                if(field[i0][j0+1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0+1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0+1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0+1].getRelativeDistance()+1);
                
                if(i0>0)
                if(field[i0-1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0-1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0-1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0-1][j0].getRelativeDistance()+1);
                
                
                if(i0>0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_ii_out[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_ii_out[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0<size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_ii_out[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0<size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_ii_out[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_ii_out[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_ii_out[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_ii_out[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_ii_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_ii_out[7] + field[i0][j0].getRelativeDistance());
                }
              
            } else if (i0 % 2 == 0 && j0 % 2 == 1) {
                
                if(j0 >0)
                if(field[i0][j0-1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0-1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0-1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0-1].getRelativeDistance()+1);
                
                if(i0>0)
                if(field[i0-1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0-1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0-1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0-1][j0].getRelativeDistance()+1);
                
                if(i0>0 && j0 >0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_pi_out[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_pi_out[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0<size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_pi_out[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 <size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_pi_out[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_pi_out[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_pi_out[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_pi_out[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_pi_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_pi_out[7] + field[i0][j0].getRelativeDistance());
                }
                
            } else if (i0 % 2 == 1 && j0 % 2 == 0) {
                
                if(j0 <size-1)
                if(field[i0][j0+1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0+1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0+1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0+1].getRelativeDistance()+1);
                
                if(i0<size-1)
                if(field[i0+1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0+1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0+1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0+1][j0].getRelativeDistance()+1);
                
                
                if(i0>0 && j0 >0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_ip_out[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_ip_out[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0 <size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_ip_out[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 <size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_ip_out[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_ip_out[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_ip_out[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_ip_out[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_ip_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_ip_out[7] + field[i0][j0].getRelativeDistance());
                }

            } else if (i0 % 2 == 0 && j0 % 2 == 0) {
                
                if(j0 >0)
                if(field[i0][j0-1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0-1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0-1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0-1].getRelativeDistance()+1);
                
                if(i0<size-1)
                if(field[i0+1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0+1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0+1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0+1][j0].getRelativeDistance()+1);
                
                
                if(i0>0 && j0 >0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_pp_out[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_pp_out[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0 <size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_pp_out[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 <size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_pp_out[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_pp_out[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_pp_out[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_pp_out[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_pp_out[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_pp_out[7] + field[i0][j0].getRelativeDistance());
                }

            }
    }
    
    private void relativeInDistance(int i0, int j0){
            //System.out.println("("+i0+","+j0+") " + field[i0][j0].getRelativeLevel()+ " " + relativeLevel(i0,j0,i0-1,j0-1) );
            if (i0 % 2 == 1 && j0 % 2 == 1) {
                
                if(j0>0)
                if(field[i0][j0-1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0-1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0-1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0-1].getRelativeDistance()+1);
                
                if(i0<size-1)
                if(field[i0+1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0+1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0+1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0+1][j0].getRelativeDistance()+1);
                
                
                if(i0>0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_ii_in[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_ii_in[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0<size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_ii_in[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0<size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_ii_in[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_ii_in[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_ii_in[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_ii_in[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_ii_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_ii_in[7] + field[i0][j0].getRelativeDistance());
                }
              
            } else if (i0 % 2 == 0 && j0 % 2 == 1) {
                
                if(j0 <size-1)
                if(field[i0][j0+1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0+1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0+1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0+1].getRelativeDistance()+1);
                
                if(i0<size-1)
                if(field[i0+1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0+1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0+1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0+1][j0].getRelativeDistance()+1);
                    
                if(i0>0 && j0 >0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_pi_in[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_pi_in[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0<size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_pi_in[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 <size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_pi_in[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_pi_in[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_pi_in[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_pi_in[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_pi_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_pi_in[7] + field[i0][j0].getRelativeDistance());
                }
                
            } else if (i0 % 2 == 1 && j0 % 2 == 0) {
                
                if(j0 >0)
                if(field[i0][j0-1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0-1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0-1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0-1].getRelativeDistance()+1);
                
                if(i0>0)
                if(field[i0-1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0-1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0-1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0-1][j0].getRelativeDistance()+1);
                
                
                if(i0>0 && j0 >0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_ip_in[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_ip_in[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0 <size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_ip_in[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 <size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_ip_in[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_ip_in[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_ip_in[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_ip_in[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_ip_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_ip_in[7] + field[i0][j0].getRelativeDistance());
                }

            } else if (i0 % 2 == 0 && j0 % 2 == 0) {
                
                if(j0 <size-1)
                if(field[i0][j0+1].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0][j0+1].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0][j0+1].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0][j0+1].getRelativeDistance()+1);
                
                if(i0>0)
                if(field[i0-1][j0].getRelativeLevel()==field[i0][j0].getRelativeLevel() && field[i0-1][j0].getRelativeDistance()>0 && field[i0][j0].getRelativeDistance() > field[i0-1][j0].getRelativeDistance()+1)
                      field[i0][j0].setRelativeDistance(field[i0-1][j0].getRelativeDistance()+1);
                
                
                if(i0>0 && j0 >0)
                if (field[i0 - 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 - 1].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0 - 1].getRelativeDistance() ) 
                        field[i0 - 1][j0 - 1].setRelativeDistance(pattern_pp_in[0] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0)
                if (field[i0 - 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0].getRelativeDistance() ) 
                        field[i0 - 1][j0].setRelativeDistance(pattern_pp_in[1] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0>0 && j0 <size-1)
                if (field[i0 - 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                   if(field[i0 - 1][j0 +1].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 - 1][j0+1].getRelativeDistance() ) 
                        field[i0 - 1][j0 + 1].setRelativeDistance(pattern_pp_in[2] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 <size-1)
                if (field[i0][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0][j0 +1].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0][j0+1].getRelativeDistance() ) 
                        field[i0][j0 + 1].setRelativeDistance(pattern_pp_in[3] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 <size-1)
                if (field[i0 + 1][j0 + 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 +1].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0+1].getRelativeDistance() ) 
                        field[i0 + 1][j0 + 1].setRelativeDistance(pattern_pp_in[4] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1)
                if (field[i0 + 1][j0].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0].getRelativeDistance() ) 
                        field[i0 + 1][j0].setRelativeDistance(pattern_pp_in[5] + field[i0][j0].getRelativeDistance());
                }
                
                if(i0<size-1 && j0 >0)
                if (field[i0 + 1][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 + 1][j0 -1].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 + 1][j0-1].getRelativeDistance() ) 
                        field[i0 + 1][j0 - 1].setRelativeDistance(pattern_pp_in[6] + field[i0][j0].getRelativeDistance());
                }
                
                if(j0 >0)
                if (field[i0][j0 - 1].getRelativeLevel() == field[i0][j0].getRelativeLevel() + 1) {
                    if(field[i0 ][j0 -1].getRelativeDistance()==0 || (pattern_pp_in[0] + field[i0][j0].getRelativeDistance())<=field[i0 ][j0-1].getRelativeDistance() ) 
                        field[i0][j0 - 1].setRelativeDistance(pattern_pp_in[7] + field[i0][j0].getRelativeDistance());
                }

            }
    }

//----------------------------------END-RelativeDistance-------------------------------------------------------------------------------------------------------------------------    

    
    
//trasnpoe o node para o ponto p e como consequencia todo mig
public boolean transmoveMig(String node, Point p) {
    if(this.getMig().getNode(node)!=null){
        
        // garante a integridade das rotas no USE
        if( ((this.getMig().getNode(node).getPlaceX()%2)-p.getX()%2)!=0 || ((this.getMig().getNode(node).getPlaceY()%2)-p.getY()%2)!=0 )
            return false;
        
        int tx = (p.getX()-this.getMig().getNode(node).getPlaceX());
        int ty = (p.getY()-this.getMig().getNode(node).getPlaceY());
        
        int x0 = 0;
        int x1 = 0;
        int y0 = 0;
        int y1 = 0;
        if(this.getMig().getMajorityGraph()!=null)
            if (this.getMig().getMajorityGraph().size() > 0) {
                x0=this.getMig().getMajorityGraph().get(0).getPlaceX();
                x1=this.getMig().getMajorityGraph().get(0).getPlaceX();
                y0=this.getMig().getMajorityGraph().get(0).getPlaceY();
                y1=this.getMig().getMajorityGraph().get(0).getPlaceY();
                    for (int i = 0; i < this.getMig().getMajorityGraph().size(); i++) {
                        for(int j=0;j<this.getMig().getMajorityGraph().get(i).getRoutes().size();j++){
                            for(int k=0;k<this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().size();k++){
                                    if(x0>this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x0=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(x1<this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX())
                                        x1=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                                    if(y0>this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y0=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                                    if(y1<this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY())
                                        y1=this.getMig().getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                            }
                        }
                    }
            }
        
                
        if( (x0+tx)<0 || (x1+tx)>=size || (y0+ty)<0 || (y1+ty)>=size    )
            return false;
        
        this.getMig().transmove( tx , ty );
        return true;
        
        
        
        
    }
    return false;
        
}  
    
    
public int distance(Point source, Point target){

    int d=0;
    if(source.getX()>=0 && source.getX()<size && source.getY()>=0 && source.getY()<size ){
        this.startRelativeDistance(source.getX(), source.getY(), this.RelativeLevel(source.getX(), source.getY(), target.getX(), target.getY()), "out");
        d=this.field[target.getX()][target.getY()].getRelativeDistance();
    }
    return d;
}    
    
    
    
    
    
    
    
public MajorityGraph mergeIntersectMig(MajorityGraph mig1, MajorityGraph mig2){

    MajorityGraph merge = new MajorityGraph();
    Vector<String> intersectNodes = new Vector <String>();// armazenas nós(2 a 2) que devereiam ter conexoes mas pela partição nao tem
    boolean b=true;
    
    
    for(int i=0;i<mig1.getMajorityGraph().size();i++){// nodes que estão na interseção dos grafos mig1 e mig2
        if(mig2.getNode(mig1.getMajorityGraph().get(i).getName())!=null){
            intersectNodes.add(mig1.getMajorityGraph().get(i).getName());
        }
    }
    
    this.clear();
    this.setMig(mig1);
    this.placeAndRouteMig();
    
    this.setMig(mig2);
    
    
    //b=this.transmoveMig(intersectNodes.get(0), new Point(mig1.getNode(intersectNodes.get(0)).getPlaceX(), mig1.getNode(intersectNodes.get(0)).getPlaceY()) );
    
    //testa todos pontos na interseção mig1 e mig2 para ver se estão na mesma posição
    for(int i=0;i<intersectNodes.size();i++){
        if( !new Point(mig1.getNode(intersectNodes.get(i)).getPlaceX(),mig1.getNode(intersectNodes.get(i)).getPlaceY()).equal( new Point(mig2.getNode(intersectNodes.get(i)).getPlaceX(),mig2.getNode(intersectNodes.get(i)).getPlaceY()) )   ){
            b=false;
        }
    }
    
    if(this.placeAndRouteMig() && b){
        for(int i=0;i<mig1.getMajorityGraph().size();i++){
            merge.addNode(new MajorityNode(mig1.getMajorityGraph().get(i)));
        }
        for(int i=0;i<mig2.getMajorityGraph().size();i++){
            if(merge.getNode(mig2.getMajorityGraph().get(i).getName())==null){
                merge.addNode(new MajorityNode(this.getMig().getMajorityGraph().get(i)));
            }else{
                for(int j=0;j<mig2.getMajorityGraph().get(i).getRoutes().size();j++){
                    merge.getNode(mig2.getMajorityGraph().get(i).getName()).addRoute(mig2.getMajorityGraph().get(i).getRoutes().get(j));
                }
            }
        }
        
        return merge;
    }
    
    return null;
}
    
    
    
public MajorityGraph mergeMig(MajorityGraph mig1, MajorityGraph mig2){

    MajorityGraph merge = new MajorityGraph();
    Vector<String> unconectNodes = new Vector <String>();// armazenas nós(2 a 2) que devereiam ter conexoes mas pela partição nao tem
    boolean isOk=true;
    
    for(int i=0;i<mig1.getMajorityGraph().size();i++)
        merge.addNode(new MajorityNode(mig1.getMajorityGraph().get(i)));
    for(int i=0;i<mig2.getMajorityGraph().size();i++)
        merge.addNode(new MajorityNode(mig2.getMajorityGraph().get(i)));
    
    merge.setLevelWeight(mig1.getLevelWeight());
    
    
    for(int i=0;i<mig2.getMajorityGraph().size();i++){
        for(int j=0;j<mig2.getMajorityGraph().get(i).getInput().size();j++)
            if(mig2.getNode(mig2.getMajorityGraph().get(i).getInput().get(j))==null && mig1.getNode(mig2.getMajorityGraph().get(i).getInput().get(j))!=null){
                unconectNodes.add(mig2.getMajorityGraph().get(i).getInput().get(j));
                unconectNodes.add(mig2.getMajorityGraph().get(i).getName());
                //System.out.println(mig2.getMajorityGraph().get(i).getName()+" <- " + mig2.getMajorityGraph().get(i).getInput().get(j) );
            }
    }
    for(int i=0;i<mig2.getMajorityGraph().size();i++){
        for(int j=0;j<mig2.getMajorityGraph().get(i).getOutput().size();j++)
            if(mig2.getNode(mig2.getMajorityGraph().get(i).getOutput().get(j))==null && mig1.getNode(mig2.getMajorityGraph().get(i).getOutput().get(j))!=null){
                unconectNodes.add(mig2.getMajorityGraph().get(i).getName());
                unconectNodes.add(mig2.getMajorityGraph().get(i).getOutput().get(j));
                //System.out.println(mig2.getMajorityGraph().get(i).getName()+" -> " +mig2.getMajorityGraph().get(i).getOutput().get(j));
            }
    }
    
    this.clear();
    this.setMig(mig2);
    this.placeAndRouteMig();
    
    Vector<Point> pts = new Vector<Point>();
    
    if(unconectNodes.size()>1){//se existe pelo menos uma aresta entre os 2 grafos mig1 e mig2
        if(mig2.getNode(unconectNodes.get(0))!=null){
            
            // adiciona o node de mig 1 ao mig2 para encontrar os pontos candidatos a posicionamento e o remove apos
            mig1.getNode(unconectNodes.get(1)).setPlaced(false);
            mig2.addNode(new MajorityNode(mig1.getNode(unconectNodes.get(1))));
            pts=this.validPlace(unconectNodes.get(1));
            mig2.getMajorityGraph().remove(mig2.getNode(unconectNodes.get(1)));
            //------------------------------
            
            for(int i=0;i<pts.size();i++)
                if( !((pts.get(i).getX()%2 - mig2.getNode(unconectNodes.get(0)).getPlaceX()%2)==0   &&   ( pts.get(i).getY()%2 - mig2.getNode(unconectNodes.get(0)).getPlaceY()%2)==0)){
                    pts.remove(i);
                    i--;
                }
                    
            this.setMig(mig1);
            for(int i=0;i<pts.size();i++){
                this.transmoveMig(unconectNodes.get(1), pts.get(i));// transpoe o mig1 para o ponto canditado
                if(this.placeAndRouteMig()){
                    
                    isOk=true;
                    for(int j=0;j<unconectNodes.size()-1;j+=2){// checa se os demais pontos satisfazem a condicao de distancia
                        if(mig2.getNode(unconectNodes.get(j))!=null){
                            if(merge.nodesDistance( merge.getNode(unconectNodes.get(j)) , merge.getNode(unconectNodes.get(j+1)) ) != this.distance( new Point(mig2.getNode(unconectNodes.get(j)).getPlaceX(), mig2.getNode(unconectNodes.get(j)).getPlaceY() ), new Point( new Point(mig1.getNode(unconectNodes.get(j+1)).getPlaceX(), mig1.getNode(unconectNodes.get(j+1)).getPlaceY() ) ) ) )
                                    isOk=false;
                        }else{
                            if(merge.nodesDistance( merge.getNode(unconectNodes.get(j)) , merge.getNode(unconectNodes.get(j+1)) ) != this.distance( new Point(mig1.getNode(unconectNodes.get(j)).getPlaceX(), mig1.getNode(unconectNodes.get(j)).getPlaceY() ), new Point( new Point(mig2.getNode(unconectNodes.get(j+1)).getPlaceX(), mig2.getNode(unconectNodes.get(j+1)).getPlaceY() ) ) ) )
                                    isOk=false;
                        }
                    }
                    if(isOk){
                        mig1.printRoutes();
                        mig2.printRoutes();
                        this.print();
                    }
                    this.unplaceAndUnrouteMig();
                }
            }
        }
        else{
            
            // adiciona o node de mig 1 ao mig2 para encontrar os pontos candidatos a posicionamento e o remove apos
            mig1.getNode(unconectNodes.get(0)).setPlaced(false);
            mig2.addNode(new MajorityNode(mig1.getNode(unconectNodes.get(0))));
            pts=this.validPlace(unconectNodes.get(0));
            mig2.getMajorityGraph().remove(mig2.getNode(unconectNodes.get(0)));
            //------------------------------
            
            this.setMig(mig1);
            for(int i=0;i<pts.size();i++){
                this.transmoveMig(unconectNodes.get(0), pts.get(i));// transpoe o mig1 para o ponto canditado
                if(this.placeAndRouteMig()){
                    
                    isOk=true;
                    for(int j=0;j<unconectNodes.size()-1;j+=2){// checa se os demais pontos satisfazem a condicao de distancia
                        if(mig2.getNode(unconectNodes.get(j))!=null){
                            
                            if(merge.nodesDistance( merge.getNode(unconectNodes.get(j)) , merge.getNode(unconectNodes.get(j+1)) ) != this.distance( new Point(mig2.getNode(unconectNodes.get(j)).getPlaceX(), mig2.getNode(unconectNodes.get(j)).getPlaceY() ), new Point( new Point(mig1.getNode(unconectNodes.get(j+1)).getPlaceX(), mig1.getNode(unconectNodes.get(j+1)).getPlaceY() ) ) ) )
                                    isOk=false;
                        }else{
                            if(merge.nodesDistance( merge.getNode(unconectNodes.get(j)) , merge.getNode(unconectNodes.get(j+1)) ) != this.distance( new Point(mig1.getNode(unconectNodes.get(j)).getPlaceX(), mig1.getNode(unconectNodes.get(j)).getPlaceY() ), new Point( new Point(mig2.getNode(unconectNodes.get(j+1)).getPlaceX(), mig2.getNode(unconectNodes.get(j+1)).getPlaceY() ) ) ) )
                                    isOk=false;
                        }
                    }
                    if(isOk){
                        ;//this.print();
                    }
                    this.unplaceAndUnrouteMig();
                }
            }
             
        }
    
    }
        
    
    return merge;
}

//retorna o ponto da porta line,col com base na posição de posicionamento do majoritynode m
public Point UsePort(Point[][] field, MajorityNode m, int line, int col){
    return field[(m.getPlaceX() - this.getMig().minPoint().getX()) *5 + line][(m.getPlaceY() - this.getMig().minPoint().getY()) *5 + col];
}






}
