/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package use;

import java.util.Vector;

/**
 *
 * @author geraldo
 */
public class Route {

    private Vector<Point> route;
    private String routeOrientation;

    public Route(Route r) {
        this.routeOrientation = r.getRouteOrientation();
        this.route = new Vector<Point>();
        for (int i = 0; i < r.getRoute().size(); i++) {
            this.route.add(new Point(r.getRoute().get(i).getX(), r.getRoute().get(i).getY()));
        }
    }

    public Route() {
        this.route = new Vector<Point>();
        this.routeOrientation = "io";
    }

    public Vector<Point> getRoute() {
        return route;
    }

    public void setRoute(Vector<Point> route) {
        this.route = route;
    }

    public void addAll(Route r) {
        if (this.route == null) {
            this.route = new Vector<Point>();
        }
        for (int i = 0; i < r.getRoute().size(); i++) {
            this.route.add(r.getRoute().get(i));
        }
    }

    public boolean equal(Route r) {
        if (r.getRoute().size() == this.getRoute().size()) {
            for (int i = 0; i < this.getRoute().size(); i++) {
                if (!this.getRoute().get(i).equal(r.getRoute().get(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean contain(Point p) {

        for (int i = 0; i < this.route.size(); i++) {
            if (this.route.get(i).equal(p)) {
                return true;
            }
        }
        return false;
    }

    public int indexOf(Point p) {

        for (int i = 0; i < this.route.size(); i++) {
            if (this.route.get(i).equal(p)) {
                return i;
            }
        }
        return (-10);
    }

    public void merge(Route r) {

        for (int j = 0; j < r.getRoute().size(); j++) {
            if (!this.contain(r.getRoute().get(j))) {
                this.getRoute().add(r.getRoute().get(j));
            }
        }
    }

    public Route reverse() {

        Route rev = new Route();
        for (int i = (this.route.size() - 1); i >= 0; i--) {
            rev.getRoute().add(new Point(this.getRoute().get(i).getX(), this.getRoute().get(i).getY()));
        }
        return rev;
    }

    public void print() {
        for (int i = 0; i < this.route.size() - 1; i++) {
            this.route.get(i).print();
            System.out.print("->");
        }
        this.route.get(this.route.size() - 1).print();
        System.out.println();
    }
/*
    public String toString() {
        String s = "|" + this.getRouteOrientation() + "| ";
        for (int i = 0; i < this.route.size() - 1; i++) {
            s = s + this.route.get(i).toString() + "-";
        }
        if (this.route.size() > 0) {
            s = s + this.route.get(this.route.size() - 1).toString() + "\n";
        }
        return s;
    }*/
    public String toString() {
        String s = "|" + this.getRouteOrientation() + "|";
        for (int i = 0; i < this.route.size() - 1; i++) {
            s = s + this.route.get(i).toString();
        }
        if (this.route.size() > 0) {
            s = s + this.route.get(this.route.size() - 1).toString();
        }
        return s;
    }

    public String getRouteOrientation() {
        return routeOrientation;
    }

    public void setRouteOrientation(String routeOrientation) {
        this.routeOrientation = routeOrientation;
    }

    //segmenenta uma rota em sub rotas verticais e horizontais
    public Vector<Route> segmentRoute() {
        Vector<Route> seg = new Vector<Route>();
        Route r = new Route();

        //System.out.println(this.getRoute());
        if (this.getRoute().size() > 0) {
            for (int i = 0; i < this.getRoute().size() - 1; i++) {
                if (this.getRoute().get(i).getX() != this.getRoute().get(i + 1).getX()) {// se x variar
                    r = new Route();
                    while (i < (this.getRoute().size() - 1) && this.getRoute().get(i).getY() == this.getRoute().get(i + 1).getY()) {// enquanto o y nao variar
                        //System.out.print(this.getRoute().get(i) + " ");
                        r.getRoute().add(this.getRoute().get(i));
                        i++;
                    }
                    if (i < (this.getRoute().size())) {
                        r.getRoute().add(this.getRoute().get(i));
                        //System.out.print(this.getRoute().get(i) + " ");
                    }
                    //System.out.println();
                    i--;
                    seg.add(new Route(r));
                    continue;
                }

                if (this.getRoute().get(i).getY() != this.getRoute().get(i + 1).getY()) {// se x variar
                    r = new Route();
                    while (i < (this.getRoute().size() - 1) && this.getRoute().get(i).getX() == this.getRoute().get(i + 1).getX()) {// enquanto o y nao variar
                        r.getRoute().add(this.getRoute().get(i));
                        //System.out.print(this.getRoute().get(i) + " ");
                        i++;
                    }
                }
                if (i < (this.getRoute().size())) {
                    r.getRoute().add(this.getRoute().get(i));
                    //System.out.print(this.getRoute().get(i) + " ");
                }
                //System.out.println();
                i--;
                seg.add(new Route(r));
                continue;

            }
        }
        return seg;
    }

    public boolean hasSubRoute(Route sub) {

        boolean b = false;
        for (int i = 0; i < this.getRoute().size(); i++) {
            if (this.getRoute().get(i).equal(sub.getRoute().get(0))) {
                b = true;

                if (i + (sub.getRoute().size() - 1) >= (this.getRoute().size())) {
                    return false;
                }
                for (int j = 0; j < sub.getRoute().size(); j++) {
                    if (!this.getRoute().get(i + j).equal(sub.getRoute().get(j))) {
                        b = false;
                    }
                }
                if (b) {
                    return true;
                }
            }
        }

        return false;
    }

    // retorna uma rota interna para uma use cell de 5
    public Route UseRouter(Point a, Point b) {
        Route r = new Route();
        if (a.getX() % 5 == 0 && a.getY() % 5 == 1) {
            if (b.getX() % 5 == 1 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 3));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 3));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 4, a.getY() + 0));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 4, a.getY() + 2));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 1));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 1));
            }
        }
        if (a.getX() % 5 == 0 && a.getY() % 5 == 3) {
            if (b.getX() % 5 == 1 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 1));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 1));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 4, a.getY() - 2));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 4, a.getY() + 0));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 3));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 3));
            }
        }
        if (a.getX() % 5 == 1 && a.getY() % 5 == 4) {
            if (b.getX() % 5 == 0 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 3));
            }
            if (b.getX() % 5 == 0 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 1));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 1));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() - 3));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 4));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() - 4));
            }
        }
        if (a.getX() % 5 == 3 && a.getY() % 5 == 4) {
            if (b.getX() % 5 == 0 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 3));
            }
            if (b.getX() % 5 == 0 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 1));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 1));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() - 3));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 4));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() - 3));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() - 4));
            }
        }
        if (a.getX() % 5 == 4 && a.getY() % 5 == 3) {
            if (b.getX() % 5 == 0 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() - 4, a.getY() - 2));
            }
            if (b.getX() % 5 == 0 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 4, a.getY() + 0));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 1));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 1));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 3));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 1));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 2));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 3));
            }
        }
        if (a.getX() % 5 == 4 && a.getY() % 5 == 1) {
            if (b.getX() % 5 == 0 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 4, a.getY() + 0));
            }
            if (b.getX() % 5 == 0 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() - 4, a.getY() + 2));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 3));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 3));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() - 1));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 0) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() - 1));
            }
        }
        if (a.getX() % 5 == 3 && a.getY() % 5 == 0) {
            if (b.getX() % 5 == 0 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 1));
            }
            if (b.getX() % 5 == 0 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() - 3, a.getY() + 3));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() - 2, a.getY() + 4));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 4));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 3));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 1));
            }
        }
        if (a.getX() % 5 == 1 && a.getY() % 5 == 0) {
            if (b.getX() % 5 == 0 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 1));
            }
            if (b.getX() % 5 == 0 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() - 1, a.getY() + 3));
            }
            if (b.getX() % 5 == 1 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 4));
            }
            if (b.getX() % 5 == 3 && b.getY() % 5 == 4) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 4));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 3) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 0));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 2));
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 3));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 3));
            }
            if (b.getX() % 5 == 4 && b.getY() % 5 == 1) {
                r.getRoute().add(new Point(a.getX() + 0, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 1, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 2, a.getY() + 1));
                r.getRoute().add(new Point(a.getX() + 3, a.getY() + 1));
            }
        }

        return r;
    }

}
