/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package use;

import java.io.File;
import static java.lang.Math.abs;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;
import majority.*;
import parser.Parser;
import verilogGraph.*;

/**
 *
 * @author geraldo
 */
// para mudar o sentido de posicionamento de saida para entrada para entrada para saida é preciso alterar o levelWeight e a funcao startRelative level(trocando relativeInDistance() por relativeOutDistance)
public class PR {

    MajorityGraph mig = new MajorityGraph();
    MajorityNode node = null;
    static int size = 80;
    Vector<Integer> levelWeight = new Vector<Integer>(); // peso de cada nivel 
    UseField use = new UseField(size);
    String placeMap[][] = new String[size][size];
    int routeMap[][] = new int[size][size];
    Vector<MajorityGraph> solutions = new Vector<MajorityGraph>();
    Vector<MajorityGraph> solutions2 = new Vector<MajorityGraph>();
    
    int numSolutions=1000; // numero de soluçoes maximas
    int maxPossibilities=10; // numero maximo de posicoes para cada node, numero de posicoes validas testadas para cada node
    
    Vector<String> partitions= new Vector<String>();
    Vector<Integer> numSolution= new Vector<Integer>();
    Vector<Integer> maxPossibilitie= new Vector<Integer>();
    
    private static String config;
    public static void main(String[] args) {
	if(args.length>=2){
	  config=args[1];
	}else{
	  config="";
	}
        if(args.length>0){
            new PR().start(args[0]);
        }else{
            new PR().start("");
	}
    }

    public void start(String file) {

        mig = new MajorityGraph();
        VerilogGraph vg = new VerilogGraph();
        vg.init(mig,file);//inicia o MIG a partir de um arquivo verilog

        use.setMig(mig);

        MajorityGraph mig1 = new MajorityGraph();
        MajorityGraph mig2 = new MajorityGraph();
        
        Vector<MajorityGraph> merged = new Vector<MajorityGraph>();
        Vector<MajorityGraph> merged2 = new Vector<MajorityGraph>();
        Vector<MajorityGraph> merged3 = new Vector<MajorityGraph>();
        
        if (this.readConfig()) {
            if (partitions.size() > 1) {
                for (int i = 0; i < partitions.get(0).split(" ").length; i++) {
                    mig1.addNode(new MajorityNode(mig.getNode(partitions.get(0).split(" ")[i])));
                }
                for (int i = 0; i < partitions.get(1).split(" ").length; i++) {
                    mig2.addNode(new MajorityNode(mig.getNode(partitions.get(1).split(" ")[i])));
                }
                mig1.setLevelWeight(mig.getLevelWeight());
                mig1.setMaxLevel(mig.getMaxLevel());
                mig2.setLevelWeight(mig.getLevelWeight());
                mig2.setMaxLevel(mig.getMaxLevel());
                numSolutions = numSolution.get(1);
                maxPossibilities = maxPossibilitie.get(1);
                merged = merge(mig1, mig2);
                if (merged.size() > 0) {
                    //System.out.println("total1:"+merged.size());
                    for (int j = 2; j < partitions.size(); j++) {
                        mig2 = new MajorityGraph();
                        for (int i = 0; i < partitions.get(j).split(" ").length; i++) {
                            mig2.addNode(new MajorityNode(mig.getNode(partitions.get(j).split(" ")[i])));
                        }
                        mig2.setLevelWeight(mig.getLevelWeight());
                        mig2.setMaxLevel(mig.getMaxLevel());
                        numSolutions = numSolution.get(j);
                        maxPossibilities = maxPossibilitie.get(j);
                        merged2 = mergeSolution(merged, mig2);
                        if (merged2.size() > 0) {
                            //System.out.println("total"+j+":"+merged2.size());
                            merged.clear();
                            merged.addAll(merged2);
                            merged2.clear();
                        } else {
                            return;
                        }
                    }

                    int maxArea = 10000;
                    int solNum = 0;
                    for (int i = 0; i < merged.size(); i++) {
                        use.clear();
                        use.setMig(merged.get(i));
                        use.placeAndRouteMig();
                        if (merged.get(i).totalArea() < maxArea) {
                            maxArea = merged.get(i).totalArea();
                            solNum = i;
                        }
                    }
                    //System.out.println(maxArea+"****************");
                    if (merged.size() > 0) {
                        int[][] ocupancyField;
                        use.clear();
                        use.setMig(merged.get(solNum));
                        use.placeAndRouteMig();
                        use.getMig().printRoutes();
                        this.printQCARoute(use.getMig());
                        this.printPlaceMap(use.getMig());
                        ocupancyField = new Parser().use2qca(use);
                        for (int c = 0; c < ocupancyField.length; c++) {
                            for (int l = 0; l < ocupancyField[c].length; l++) {
                                if (ocupancyField[c][l] > 0) {
                                    System.out.print(ocupancyField[c][l]);
                                } else {
                                    System.out.print(" ");
                                }
                            }
                            System.out.println();
                        }
                        System.out.println();

                        for (int i = 0; i < 10; i++) {
                            use.clear();
                            use.setMig(merged.get(((merged.size() - 1) / 10) * i));
                            use.placeAndRouteMig();
                            use.getMig().printRoutes();
                            this.printQCARoute(use.getMig());
                            this.printPlaceMap(use.getMig());
                            ocupancyField = new Parser().use2qca(use);
                            for (int c = 0; c < ocupancyField.length; c++) {
                                for (int l = 0; l < ocupancyField[c].length; l++) {
                                    if (ocupancyField[c][l] > 0) {
                                        System.out.print(ocupancyField[c][l]);
                                    } else {
                                        System.out.print(" ");
                                    }
                                }
                                System.out.println();
                            }
                            System.out.println();
                        }

                    }

                }
            }
        }
    }

    // 
    public Vector<MajorityGraph> merge(MajorityGraph mig1, MajorityGraph mig2) {

        Vector<MajorityGraph> aux1 = new Vector<MajorityGraph>();
        Vector<MajorityGraph> aux2 = new Vector<MajorityGraph>();
        Vector<MajorityGraph> result = new Vector<MajorityGraph>();
        MajorityNode root = new MajorityNode();
        int[][][] maps1;
        int[][][] maps2;
        Point min= new Point(0,0);
        Point max= new Point(0,0);
        boolean b=true;
        Vector<String> tnn = new Vector<String>();// translateNodeName. nome original, nome modificado
        int count;
        
        Vector<int[][]> sol= new Vector<int[][]>();

        Vector<String> intersectNodes = new Vector<String>();// 1 node em comum entre os 2 grafos

        //lista de tradução do nomes de nodes
        count=1;
        for (int q = 0; q < mig1.getMajorityGraph().size(); q++) {
            tnn.add(mig1.getMajorityGraph().get(q).getName());
            tnn.add(Integer.toString(count<<4));
            count++;
        }
        for (int q = 0; q < mig2.getMajorityGraph().size(); q++) {
            if(!tnn.contains(mig2.getMajorityGraph().get(q).getName())){
                tnn.add(mig2.getMajorityGraph().get(q).getName());
                tnn.add(Integer.toString(count<<4));
                count++;
            }
        }

        
        for (int p = 0; p < mig1.getMajorityGraph().size(); p++) {
            if (mig2.getNode(mig1.getMajorityGraph().get(p).getName()) != null) {
                intersectNodes.add(mig1.getMajorityGraph().get(p).getName());
            }
        }
        
        
        
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    for (int l = 0; l < 2; l++) {
                        aux1 = new Vector<MajorityGraph>();
                        aux2 = new Vector<MajorityGraph>();
                        solutions.clear();
                        use.clear();
                        mig1.clear();
                        mig2.clear();
                        
                        //System.out.printf("(%d,%d)(%d,%d)\n", i, j, k, l);
                        
                        // mig1 ------------------------------------------------------
                        use.setMig(mig1);
                        root = use.getMig().getMajorityGraph().get(0);
                        for (int lv = 0; lv < use.getMig().getMajorityGraph().size(); lv++) {
                            if (root.getLevel() > use.getMig().getMajorityGraph().get(lv).getLevel()) {
                                root = use.getMig().getMajorityGraph().get(lv);
                            }
                            use.getMig().getMajorityGraph().get(lv).setPlaced(false);
                        }
                        //use.place(root.getName(), (size / 2) + 1, (size / 2) + 1);
                        use.place(root.getName(), (size / 2) + i, (size / 2) + j);
                        this.placeAndRoute(root.getLevel() + 1); // possicione e rotea a partir do level 1

                        for (int lv = 0; lv < solutions.size(); lv++) {
                            aux1.add(new MajorityGraph(solutions.get(lv)));
                        }
                        solutions.clear();
                        use.clear();
                        // mig2 ------------------------------------------------------
                        use.setMig(mig2);
                        root = use.getMig().getMajorityGraph().get(0);
                        for (int lv = 0; lv < use.getMig().getMajorityGraph().size(); lv++) {
                            if (root.getLevel() > use.getMig().getMajorityGraph().get(lv).getLevel()) {
                                root = use.getMig().getMajorityGraph().get(lv);
                            }
                            use.getMig().getMajorityGraph().get(lv).setPlaced(false);
                        }
                        //use.place(root.getName(), (size / 2) + 0, (size / 2) + 1);
                        use.place(root.getName(), (size / 2) + k, (size / 2) + l);
                        this.placeAndRoute(root.getLevel() + 1); // possicione e rotea a partir do level 1
                        for (int lv = 0; lv < solutions.size(); lv++) {
                            aux2.add(new MajorityGraph(solutions.get(lv)));
                        }
                        solutions.clear();
                        use.clear();

                        //------------------------------------------------------------------------------------------------------------
                        //transpoe os ponto intercessao dos 2 migs para a mesma posiçao respeitando a orientação do USE
                        //supondo size sempre um valor par
                        for (int p = 0; p < aux1.size(); p++) {
                            if (aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX() % 2 == 0) {
                                if (aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//par par
                                    aux1.get(p).transmove((size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                                } else {//par impar
                                    aux1.get(p).transmove((size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                                }
                            } else if (aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//impar par
                                aux1.get(p).transmove((size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                            } else {//impar impar
                                aux1.get(p).transmove((size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                            }
                        }

                        for (int p = 0; p < aux2.size(); p++) {
                            if (aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX() % 2 == 0) {
                                if (aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//par par
                                    aux2.get(p).transmove((size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                                } else {//par impar
                                    aux2.get(p).transmove((size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                                }
                            } else if (aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//impar par
                                aux2.get(p).transmove((size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                            } else {//impar impar
                                aux2.get(p).transmove((size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                            }
                        }
                        //------------------------------------------------------------------------------------------------------------
                        //System.out.println(aux1.size() + "-" + aux2.size());
                        
                        //Ponto minimo e maximo dos 2 grafos no USE. usado para reduzir o numero de comparações------------------------------------------------------------------------------------------------------------
                        min.setX(size);
                        min.setY(size);
                        max.setX(0);
                        max.setY(0);
                        for (int p = 0; p < aux1.size(); p++) {
                            for (int q = 0; q < aux1.get(p).getMajorityGraph().size(); q++) {
                                if (min.getX() > aux1.get(p).getMajorityGraph().get(q).getPlaceX()) {
                                    min.setX(aux1.get(p).getMajorityGraph().get(q).getPlaceX());
                                }
                                if (max.getX() < aux1.get(p).getMajorityGraph().get(q).getPlaceX()) {
                                    max.setX(aux1.get(p).getMajorityGraph().get(q).getPlaceX());
                                }
                                if (min.getY() > aux1.get(p).getMajorityGraph().get(q).getPlaceY()) {
                                    min.setY(aux1.get(p).getMajorityGraph().get(q).getPlaceY());
                                }
                                if (max.getY() < aux1.get(p).getMajorityGraph().get(q).getPlaceY()) {
                                    max.setY(aux1.get(p).getMajorityGraph().get(q).getPlaceY());
                                }
                            }
                        }
                        for (int p = 0; p < aux2.size(); p++) {
                            for (int q = 0; q < aux2.get(p).getMajorityGraph().size(); q++) {
                                if (min.getX() > aux2.get(p).getMajorityGraph().get(q).getPlaceX()) {
                                    min.setX(aux2.get(p).getMajorityGraph().get(q).getPlaceX());
                                }
                                if (max.getX() < aux2.get(p).getMajorityGraph().get(q).getPlaceX()) {
                                    max.setX(aux2.get(p).getMajorityGraph().get(q).getPlaceX());
                                }
                                if (min.getY() > aux2.get(p).getMajorityGraph().get(q).getPlaceY()) {
                                    min.setY(aux2.get(p).getMajorityGraph().get(q).getPlaceY());
                                }
                                if (max.getY() < aux2.get(p).getMajorityGraph().get(q).getPlaceY()) {
                                    max.setY(aux2.get(p).getMajorityGraph().get(q).getPlaceY());
                                }
                            }
                        }
                        //------------------------------------------------------------------------------------------------------------
                
                        
                        maps1= new int[aux1.size()][size][size];
                        maps2= new int[aux2.size()][size][size];
                        
                        
                        for (int p = 0; p < aux1.size(); p++) {
                            for (int q = 0; q < aux1.get(p).getMajorityGraph().size(); q++) {
                                aux1.get(p).getMajorityGraph().get(q).setName(tnn.get(tnn.indexOf(aux1.get(p).getMajorityGraph().get(q).getName())+1));
                            }
                        }
                        for (int p = 0; p < aux2.size(); p++) {
                            for (int q = 0; q < aux2.get(p).getMajorityGraph().size(); q++) {
                                aux2.get(p).getMajorityGraph().get(q).setName(tnn.get(tnn.indexOf(aux2.get(p).getMajorityGraph().get(q).getName())+1));
                            }
                        }
                        
                        //treasnforma cada mig em uma matriz------------------------------------------------------------------------------------------------------------
                        for (int p = 0; p < aux1.size(); p++) {
                            for (int lin = min.getX(); lin <= max.getX(); lin++) {
                                for (int col = min.getY(); col <=max.getY(); col++) {
                                    maps1[p][lin][col]=0;
                                }
                            }
                        }
                        for (int p = 0; p < aux2.size(); p++) {
                            for (int lin = min.getX(); lin <= max.getX(); lin++) {
                                for (int col = min.getY(); col <=max.getY(); col++) {
                                    maps2[p][lin][col]=0;
                                }
                            }
                        }
                       
                        for (int p = 0; p < aux1.size(); p++) {
                            for (int q = 0; q < aux1.get(p).getMajorityGraph().size(); q++) {
                                maps1[p][aux1.get(p).getMajorityGraph().get(q).getPlaceX()][aux1.get(p).getMajorityGraph().get(q).getPlaceY()]=Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName())+5;
                                for(int r=0;r<aux1.get(p).getMajorityGraph().get(q).getRoutes().size();r++){
                                    for(int s=1;s<aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().size()-1;s++){
                                        if( (maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15)==0 ){
                                            maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()]= Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName())+1;
                                        }
                                        else if( (maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15)==1  &&  ( maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056)!=Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName()) ){
                                                maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] =
                                                        (maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056) + (Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName())<<13)+2;
                                        }
                                    }
                                }
                            }
                        }
                        
                        for (int p = 0; p < aux2.size(); p++) {
                            for (int q = 0; q < aux2.get(p).getMajorityGraph().size(); q++) {
                                maps2[p][aux2.get(p).getMajorityGraph().get(q).getPlaceX()][aux2.get(p).getMajorityGraph().get(q).getPlaceY()]=Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName())+5;
                                for(int r=0;r<aux2.get(p).getMajorityGraph().get(q).getRoutes().size();r++){
                                    for(int s=1;s<aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().size()-1;s++){
                                        if( (maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15)==0 ){
                                            maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()]= Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName())+1;
                                        }
                                        else if( (maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15)==1  &&  ( maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056)!=Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName()) ){
                                                maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] =
                                                        (maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056) + (Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName())<<13)+2;
                                        }
                                    }
                                }
                            }
                        }
                        
                        for (int p = 0; p < aux1.size(); p++) {
                            for (int q = 0; q < aux2.size(); q++) {
                                count=0;
                                b=true;
                                for (int lin = min.getX(); lin <= max.getX(); lin++) {
                                    for (int col = min.getY(); col <=max.getY(); col++) {
                                        if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) < 3) {
                                            continue;
                                        }
                                        if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 5) {
                                            count++;
                                            continue;
                                        }
                                        if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 10) {// testa posições de gates
                                            if ((maps1[p][lin][col] & 131056) == (maps2[q][lin][col] & 131056)) {
                                                count++;
                                            } else {
                                                b = false;
                                            }
                                        } else if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 3) {
                                            if ((maps1[p][lin][col] & 131056) == (maps2[q][lin][col] & 131056)) {
                                                continue;
                                            } else if ((maps1[p][lin][col] & 131056) == ((maps2[q][lin][col] & 1073610752)>>13)) {
                                                continue;
                                            } else if (((maps1[p][lin][col] & 1073610752)>>13) == (maps2[q][lin][col] & 131056)) {
                                                continue;
                                            } else if ((maps1[p][lin][col] & 1073610752) == (maps2[q][lin][col] & 1073610752)) {
                                                continue;
                                            } else {
                                                b = false;
                                            }
                                        } else if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 4) {
                                            if ((maps1[p][lin][col] & 131056) == (maps2[q][lin][col] & 131056)) {
                                                if ((maps1[p][lin][col] & 1073610752) == (maps2[q][lin][col] & 1073610752)) {
                                                    continue;
                                                } else {
                                                    b = false;
                                                }
                                            } else if ((maps1[p][lin][col] & 131056) == ((maps2[q][lin][col] & 1073610752)>>13) ) {
                                                if ( ((maps1[p][lin][col] & 1073610752)>>13) == (maps2[q][lin][col] & 131056)) {
                                                    continue;
                                                } else {
                                                    b = false;
                                                }
                                            } else {
                                                b = false;
                                            }
                                        } else {
                                            b = false;
                                        }
                                    }
                                }
                                if (b && count==(mig1.getMajorityGraph().size()+mig2.getMajorityGraph().size()-intersectNodes.size())) {
                                    //System.out.println(p+"-"+q+"  "+result.size());
                                    //if(result.size()==0 || use.mergeIntersectMig(aux1.get(p), aux2.get(q)).totalArea()<result.lastElement().totalArea())
                                    if(result.size()<30000){
				      result.add(new MajorityGraph(use.mergeIntersectMig(aux1.get(p), aux2.get(q))));
                                    }
                                }
                            }
                        }
                    
                        if (result.size() > 0) {
                            //retorna os nomes originais
                            for (int p = 0; p < result.size(); p++) {
                                for (int q = 0; q < result.get(p).getMajorityGraph().size(); q++) {
                                    result.get(p).getMajorityGraph().get(q).setName(tnn.get(tnn.indexOf(result.get(p).getMajorityGraph().get(q).getName()) - 1));
                                }
                            }
                            return result;
                        }
                    
                    }
                }
            }
        }
        return result;
    }

    public Vector<MajorityGraph> mergeSolution(Vector<MajorityGraph> aux1, MajorityGraph mig2) {

        if (aux1.size() == 0) {
            return null;
        }
        
        
        MajorityGraph mig1 = new MajorityGraph(aux1.get(0));
        Vector<MajorityGraph> aux2 = new Vector<MajorityGraph>();
        Vector<MajorityGraph> result = new Vector<MajorityGraph>();
        MajorityNode root = new MajorityNode();
        int[][][] maps1;
        int[][][] maps2;
        boolean b = true;
        Point min= new Point(0,0);
        Point max= new Point(0,0);
        Vector<String> tnn = new Vector<String>();// translateNodeName. nome original, nome modificado
        int count;

        Vector<int[][]> sol = new Vector<int[][]>();

        Vector<String> intersectNodes = new Vector<String>();// 1 node em comum entre os 2 grafos

        //lista de tradução do nomes de nodes
        count = 1;
        for (int q = 0; q < mig1.getMajorityGraph().size(); q++) {
            tnn.add(mig1.getMajorityGraph().get(q).getName());
            tnn.add(Integer.toString(count << 4));
            count++;
        }
        for (int q = 0; q < mig2.getMajorityGraph().size(); q++) {
            if (!tnn.contains(mig2.getMajorityGraph().get(q).getName())) {
                tnn.add(mig2.getMajorityGraph().get(q).getName());
                tnn.add(Integer.toString(count << 4));
                count++;
            }
        }

        for (int p = 0; p < mig1.getMajorityGraph().size(); p++) {
            if (mig2.getNode(mig1.getMajorityGraph().get(p).getName()) != null) {
                intersectNodes.add(mig1.getMajorityGraph().get(p).getName());
            }
        }

        for (int k = 0; k < 2; k++) {
            for (int l = 0; l < 2; l++) {
                aux2 = new Vector<MajorityGraph>();
                solutions.clear();
                use.clear();
                
                
                //System.out.printf("(%d,%d)\n", k, l);
                
                // mig2 ------------------------------------------------------
                use.setMig(mig2);
                root = use.getMig().getMajorityGraph().get(0);
                for (int lv = 0; lv < use.getMig().getMajorityGraph().size(); lv++) {
                    if (root.getLevel() > use.getMig().getMajorityGraph().get(lv).getLevel()) {
                        root = use.getMig().getMajorityGraph().get(lv);
                    }
                    use.getMig().getMajorityGraph().get(lv).setPlaced(false);
                }
                use.place(root.getName(), (size / 2) + k, (size / 2) + l);
                this.placeAndRoute(root.getLevel() + 1); // possicione e rotea a partir do level 1
                for (int lv = 0; lv < solutions.size(); lv++) {
                    aux2.add(new MajorityGraph(solutions.get(lv)));
                }
                solutions.clear();
                use.clear();

                //------------------------------------------------------------------------------------------------------------
                //transpoe os ponto intercessao dos 2 migs para a mesma posiçao respeitando a orientação do USE
                //supondo size sempre um valor par
                for (int p = 0; p < aux1.size(); p++) {
                    if (aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX() % 2 == 0) {
                        if (aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//par par
                            aux1.get(p).transmove((size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                        } else {//par impar
                            aux1.get(p).transmove((size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                        }
                    } else if (aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//impar par
                        aux1.get(p).transmove((size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                    } else {//impar impar
                        aux1.get(p).transmove((size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux1.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                    }
                }

                for (int p = 0; p < aux2.size(); p++) {
                    if (aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX() % 2 == 0) {
                        if (aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//par par
                            aux2.get(p).transmove((size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                        } else {//par impar
                            aux2.get(p).transmove((size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                        }
                    } else if (aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY() % 2 == 0) {//impar par
                        aux2.get(p).transmove((size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                    } else {//impar impar
                        aux2.get(p).transmove((size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceX(), (size / 2) + 1 - aux2.get(p).getNode(intersectNodes.get(0)).getPlaceY());
                    }
                }
                //------------------------------------------------------------------------------------------------------------
               
                //System.out.println(aux1.size() + "-" + aux2.size());

                
                //Ponto minimo e maximo dos 2 grafos no USE. usado para reduzir o numero de comparações------------------------------------------------------------------------------------------------------------
                min.setX(size);
                min.setY(size);
                max.setX(0);
                max.setY(0);
                for (int p = 0; p < aux1.size(); p++) {
                    for (int q = 0; q < aux1.get(p).getMajorityGraph().size(); q++) {
                        if (min.getX() > aux1.get(p).getMajorityGraph().get(q).getPlaceX()) {
                            min.setX(aux1.get(p).getMajorityGraph().get(q).getPlaceX());
                        }
                        if (max.getX() < aux1.get(p).getMajorityGraph().get(q).getPlaceX()) {
                            max.setX(aux1.get(p).getMajorityGraph().get(q).getPlaceX());
                        }
                        if (min.getY() > aux1.get(p).getMajorityGraph().get(q).getPlaceY()) {
                            min.setY(aux1.get(p).getMajorityGraph().get(q).getPlaceY());
                        }
                        if (max.getY() < aux1.get(p).getMajorityGraph().get(q).getPlaceY()) {
                            max.setY(aux1.get(p).getMajorityGraph().get(q).getPlaceY());
                        }
                    }
                }
                for (int p = 0; p < aux2.size(); p++) {
                    for (int q = 0; q < aux2.get(p).getMajorityGraph().size(); q++) {
                        if (min.getX() > aux2.get(p).getMajorityGraph().get(q).getPlaceX()) {
                            min.setX(aux2.get(p).getMajorityGraph().get(q).getPlaceX());
                        }
                        if (max.getX() < aux2.get(p).getMajorityGraph().get(q).getPlaceX()) {
                            max.setX(aux2.get(p).getMajorityGraph().get(q).getPlaceX());
                        }
                        if (min.getY() > aux2.get(p).getMajorityGraph().get(q).getPlaceY()) {
                            min.setY(aux2.get(p).getMajorityGraph().get(q).getPlaceY());
                        }
                        if (max.getY() < aux2.get(p).getMajorityGraph().get(q).getPlaceY()) {
                            max.setY(aux2.get(p).getMajorityGraph().get(q).getPlaceY());
                        }
                    }
                }
                //------------------------------------------------------------------------------------------------------------

                
                
                maps1 = new int[aux1.size()][size][size];
                maps2 = new int[aux2.size()][size][size];

                for (int p = 0; p < aux1.size(); p++) {
                    for (int q = 0; q < aux1.get(p).getMajorityGraph().size(); q++) {
                        aux1.get(p).getMajorityGraph().get(q).setName(tnn.get(tnn.indexOf(aux1.get(p).getMajorityGraph().get(q).getName()) + 1));
                    }
                }
                for (int p = 0; p < aux2.size(); p++) {
                    for (int q = 0; q < aux2.get(p).getMajorityGraph().size(); q++) {
                        aux2.get(p).getMajorityGraph().get(q).setName(tnn.get(tnn.indexOf(aux2.get(p).getMajorityGraph().get(q).getName()) + 1));
                    }
                }
                //trasnforma cada mig em uma matriz------------------------------------------------------------------------------------------------------------
                for (int p = 0; p < aux1.size(); p++) {
                    for (int lin = min.getX(); lin <= max.getX(); lin++) {
                        for (int col = min.getY(); col <= max.getY(); col++) {
                            maps1[p][lin][col] = 0;
                        }
                    }
                }
                for (int p = 0; p < aux2.size(); p++) {
                    for (int lin = min.getX(); lin <= max.getX(); lin++) {
                        for (int col = min.getY(); col <= max.getY(); col++) {
                            maps2[p][lin][col] = 0;
                        }
                    }
                }

                for (int p = 0; p < aux1.size(); p++) {
                    for (int q = 0; q < aux1.get(p).getMajorityGraph().size(); q++) {
                        maps1[p][aux1.get(p).getMajorityGraph().get(q).getPlaceX()][aux1.get(p).getMajorityGraph().get(q).getPlaceY()] = Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName()) + 5;
                        for (int r = 0; r < aux1.get(p).getMajorityGraph().get(q).getRoutes().size(); r++) {
                            for (int s = 1; s < aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().size() - 1; s++) {
                                if ((maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15) == 0) {
                                    maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] = Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName()) + 1;
                                } else if ((maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15) == 1 && (maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056) != Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName())) {
                                    maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()]
                                            = (maps1[p][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux1.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056) + (Integer.parseInt(aux1.get(p).getMajorityGraph().get(q).getName()) << 13) + 2;
                                }
                            }
                        }
                    }
                }

                for (int p = 0; p < aux2.size(); p++) {
                    for (int q = 0; q < aux2.get(p).getMajorityGraph().size(); q++) {
                        maps2[p][aux2.get(p).getMajorityGraph().get(q).getPlaceX()][aux2.get(p).getMajorityGraph().get(q).getPlaceY()] = Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName()) + 5;
                        for (int r = 0; r < aux2.get(p).getMajorityGraph().get(q).getRoutes().size(); r++) {
                            for (int s = 1; s < aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().size() - 1; s++) {
                                if ((maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15) == 0) {
                                    maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] = Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName()) + 1;
                                } else if ((maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 15) == 1 && (maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056) != Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName())) {
                                    maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()]
                                            = (maps2[p][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getX()][aux2.get(p).getMajorityGraph().get(q).getRoutes().get(r).getRoute().get(s).getY()] & 131056) + (Integer.parseInt(aux2.get(p).getMajorityGraph().get(q).getName()) << 13) + 2;
                                }
                            }
                        }
                    }
                }
                for (int p = 0; p < aux1.size(); p++) {
                    for (int q = 0; q < aux2.size(); q++) {
                        count = 0;
                        b = true;
                        for (int lin = min.getX(); lin <= max.getX(); lin++) {
                            for (int col = min.getY(); col <= max.getY(); col++) {
                                if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) < 3) {
                                    continue;
                                }
                                if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 5) {
                                    count++;
                                    continue;
                                }
                                if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 10) {// testa posições de gates
                                    if ((maps1[p][lin][col] & 131056) == (maps2[q][lin][col] & 131056)) {
                                        count++;
                                    } else {
                                        b = false;
                                    }
                                } else if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 3) {
                                    if ((maps1[p][lin][col] & 131056) == (maps2[q][lin][col] & 131056)) {
                                        continue;
                                    } else if ((maps1[p][lin][col] & 131056) == ((maps2[q][lin][col] & 1073610752) >> 13)) {
                                        continue;
                                    } else if (((maps1[p][lin][col] & 1073610752) >> 13) == (maps2[q][lin][col] & 131056)) {
                                        continue;
                                    } else if ((maps1[p][lin][col] & 1073610752) == (maps2[q][lin][col] & 1073610752)) {
                                        continue;
                                    } else {
                                        b = false;
                                    }
                                } else if ((maps1[p][lin][col] & 15) + (maps2[q][lin][col] & 15) == 4) {
                                    if ((maps1[p][lin][col] & 131056) == (maps2[q][lin][col] & 131056)) {
                                        if ((maps1[p][lin][col] & 1073610752) == (maps2[q][lin][col] & 1073610752)) {
                                            continue;
                                        } else {
                                            b = false;
                                        }
                                    } else if ((maps1[p][lin][col] & 131056) == ((maps2[q][lin][col] & 1073610752) >> 13)) {
                                        if (((maps1[p][lin][col] & 1073610752) >> 13) == (maps2[q][lin][col] & 131056)) {
                                            continue;
                                        } else {
                                            b = false;
                                        }
                                    } else {
                                        b = false;
                                    }
                                } else {
                                    b = false;
                                }
                            }
                        }
                        if (b && count == (mig1.getMajorityGraph().size() + mig2.getMajorityGraph().size() - intersectNodes.size())) {
                            if(result.size()<30000){
				result.add(new MajorityGraph(use.mergeIntersectMig(aux1.get(p), aux2.get(q))));
                            }
                            if(result.size()>10000){
                                for (int tt = 0; tt < result.size(); tt++) {
                                    for (int vv = 0; vv < result.get(tt).getMajorityGraph().size(); vv++) {
                                        result.get(tt).getMajorityGraph().get(vv).setName(tnn.get(tnn.indexOf(result.get(tt).getMajorityGraph().get(vv).getName()) - 1));
                                    }
                                }
                                return result;
                            }
                        }
                    }
                }

                for (int p = 0; p < aux1.size(); p++) {
                    for (int q = 0; q < aux1.get(p).getMajorityGraph().size(); q++) {
                        aux1.get(p).getMajorityGraph().get(q).setName(tnn.get(tnn.indexOf(aux1.get(p).getMajorityGraph().get(q).getName()) - 1));
                    }
                }
                
                if (result.size() > 0) {
                    //retorna os nomes originais
                    for (int p = 0; p < result.size(); p++) {
                        for (int q = 0; q < result.get(p).getMajorityGraph().size(); q++) {
                            result.get(p).getMajorityGraph().get(q).setName(tnn.get(tnn.indexOf(result.get(p).getMajorityGraph().get(q).getName()) - 1));
                        }
                    }
                    return result;
                }

            }
        }
        return result;
    }


    public void placeAndRoute(int level) {
        if (level <= use.getMig().getMaxLevel()) {
            boolean sol;
            Vector<Vector<Vector<Integer>>> v3;
            Vector<Vector<Integer>> v2;
            Vector<Integer> v1;
            v3 = new Vector<Vector<Vector<Integer>>>();
            for (int j = 0; j < use.getMig().nodesInLevel(level).size(); j++) {// conta o numero de solucoes para cada node no level atual
                v1 = new Vector();
                v2 = new Vector<Vector<Integer>>();
                if (use.possibilitiesNumber(use.getMig().nodesInLevel(level).get(j)) == 0) {
                    return;
                }

                //limitador do maximo de possibilidades para cada ponto
                if (use.possibilitiesNumber(use.getMig().nodesInLevel(level).get(j)) > maxPossibilities) {
                    v1.add(maxPossibilities);
                } else {
                    v1.add(use.possibilitiesNumber(use.getMig().nodesInLevel(level).get(j)));
                }
                //v1.add(use.possibilitiesNumber(use.getMig().nodesInLevel(level).get(j)));
                v2.add(v1);
                v3.add(v2);
            }
            if (use.getMig().nodesInLevel(level).size() == 0) {
                placeAndRoute(level + 1);//chamar recursivamente a funcao passando level+1
            }

            if (solutions.size() < numSolutions) {// se existe pelo menos uma solucao entao analisa a solucao para aquele level

                use.combination(v3);
                if (v3.size() > 0) {
                    for (int i = 0; i < v3.get(0).size(); i++) {// gera a solucao atual com base na combinacao
                        // aplica a j-esima solucao no nivel (posiciona e rotea)
                        for (int j = 0; j < v3.get(0).get(i).size(); j++) {// gera a solucao atual com base na combinacao
                            use.initNodeRoutes(use.getMig().nodesInLevel(level).get(j));// inicializa as rotas para cada node
                            
                            for (int l = 0; l < v3.get(0).get(i).get(j); l++) {// l usado para repeticao de nextSolution
                                use.nextSolution(use.getMig().nodesInLevel(level).get(j));
                            }
                            use.placeAndRouteNode(use.getMig().nodesInLevel(level).get(j));
                        }

                        if (use.getMig().levelIsPlaced(level)) {

                            if (use.getMig().migIsPlaced()) {

                                for (int l = 0; l < use.getMig().getMajorityGraph().size(); l++) {
                                    use.getMig().getMajorityGraph().get(l).removeRedundancyInRoutes();
                                }

                                //if ((solutions.size()+1)%100==0)
                                //System.out.printf("solution %d\n",solutions.size()+1);
                                //System.out.printf("total area:%s \n",use.getMig().totalArea());
                                //System.out.print("Routes:\n");
                                //System.out.print(solutions.size()+1+" ");
                                //use.getMig().printRoutes();
                                //use.print();
                                //printQCARoute(use.getMig());
                                //printPlaceMap(use.getMig());
                                //System.out.printf("-----------------------------------------------------------------------------------------------------------\n\n\n\n");
                                //System.out.println();
                                
                                solutions.add(new MajorityGraph(use.getMig()));

                            }
                            if (solutions.size() < numSolutions) {// se existe pelo menos uma solucao entao analisa a solucao para aquele level
                                placeAndRoute(level + 1);//chamar recursivamente a funcao passando level+1
                            }
                        }
                        for (int j = 0; j < v3.get(0).get(i).size(); j++) {// gera a solucao atual com base na combinacao
                            use.unplaceAndUnrouteNode(use.getMig().nodesInLevel(level).get(j));
                        }
                    }
                }
            }

        }
    }

    public void printQCARoute(MajorityGraph mig) {

        int x0 = 0;
        int x1 = 0;
        int x2 = 0;
        int y0 = 0;
        int y1 = 0;
        int y2 = 0;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                routeMap[i][j] = 0;
            }
        }

        for (int i = 0; i < mig.getMajorityGraph().size(); i++) {
            for (int j = 0; j < mig.getMajorityGraph().get(i).getRoutes().size(); j++) {
                for (int k = 1; k < mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size() - 1; k++) {

                    x0 = mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k - 1).getX();
                    x1 = mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX();
                    x2 = mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k + 1).getX();

                    y0 = mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k - 1).getY();
                    y1 = mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY();
                    y2 = mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k + 1).getY();

                    //de onde o fio esta vindo
                    if (x0 > x1) {
                        if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k - 1).isUsable()) {
                            routeMap[x1][y1] += 16;//botton
                        }
                    } else if (x0 < x1) {
                        if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k - 1).isUsable()) {
                            routeMap[x1][y1] += 1;//up
                        }
                    } else if (y0 > y1) {
                        if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k - 1).isUsable()) {
                            routeMap[x1][y1] += 4;//left
                        }
                    } else if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k - 1).isUsable()) {
                        routeMap[x1][y1] += 64;//right
                    }
                    //para onde o fio vai
                    if (x1 > x2) {
                        if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k + 1).isUsable()) {
                            routeMap[x1][y1] += 1;
                        }
                    } else if (x1 < x2) {
                        if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k + 1).isUsable()) {
                            routeMap[x1][y1] += 16;
                        }
                    } else if (y1 > y2) {
                        if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k + 1).isUsable()) {
                            routeMap[x1][y1] += 64;
                        }
                    } else if (mig.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k + 1).isUsable()) {
                        routeMap[x1][y1] += 4;
                    }

                }
            }
        }

    }

    public void printPlaceMap(MajorityGraph plot) {

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                placeMap[i][j] = " ";
            }
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (routeMap[i][j] != 0) //placeMap[i][j]="*";
                {
                    //placeMap[i][j]="*";
                    placeMap[i][j] = use.getBusy(i, j) + "";
                    //placeMap[i][j] = Integer.toString(routeMap[i][j]);
                } else {
                    placeMap[i][j] = "  ";
                }
            }
        }

        for (int i = 0; i < plot.getMajorityGraph().size(); i++) {
            if (plot.getMajorityGraph().get(i).isPlaced()) {
                placeMap[plot.getMajorityGraph().get(i).getPlaceX()][plot.getMajorityGraph().get(i).getPlaceY()] = plot.getMajorityGraph().get(i).getName();
            }
        }

        for (int i = 1; i < plot.getMajorityGraph().size(); i++) {
            if (plot.getMajorityGraph().get(i).getRoutes() != null) {
                if (plot.getMajorityGraph().get(i).isPlaced()) {
                    for (int j = 0; j < plot.getMajorityGraph().get(i).getRoutes().size(); j++) {
                        for (int k = 1; k < plot.getMajorityGraph().get(i).getRoutes().get(j).getRoute().size() - 1; k++)
                            ;//acrescenta asterisco como marca de rota //placeMap[plot.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getX()][plot.getMajorityGraph().get(i).getRoutes().get(j).getRoute().get(k).getY()]="*";
                    }
                }
            }
        }

        System.out.println();
        System.out.println();
        System.out.print("        ");
        for (int j = use.getMig().minPoint().getY(); j <= use.getMig().maxPoint().getY(); j++) {
            System.out.printf("%-6d ", j);
        }
        System.out.println();
        for (int i = use.getMig().minPoint().getX(); i <= use.getMig().maxPoint().getX(); i++) {
            System.out.printf("%-6d|", i);
            for (int j = use.getMig().minPoint().getY(); j <= use.getMig().maxPoint().getY(); j++) {
                System.out.printf("%-6s|", placeMap[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }

    /*
        // posiciona todos neighbors de node
    public void placeAndRouteNeighbors(String nodeName){
        
        if(use.getMig().getNode(nodeName)!=null){
            
            use.initNodeRoutes(nodeName);
            use.placeAndRouteNode(nodeName);
            
            
            boolean sol;
            Vector <Vector<Vector<Integer>>> v3;
            Vector <Vector<Integer>> v2;
            Vector <Integer>v1;

            Vector<String> unNeigh = new Vector<String>();
            
            
            v3=new Vector<Vector<Vector<Integer>>>();
            unNeigh=use.unplacedNeighbors(nodeName);
            
            
            
            for(int i=0;i<unNeigh.size();i++)
                for(int j=0;j<unNeigh.size();j++){
                    if(use.getMig().getNode(unNeigh.get(i)).getOutput().contains(unNeigh.get(j)) ){
                        unNeigh.remove(j);
                    }
                }
                         
            
            for (int j = 0; j < unNeigh.size(); j++){// conta o numero de solucoes para cada node no level atual
                v1 = new Vector();
                v2 = new Vector<Vector<Integer>>();
                v1.add(use.possibilitiesNumber(unNeigh.get(j)));
                v2.add(v1);
                v3.add(v2);
            }
            sol=true; // verifica se ha solucoes para todos os node do level, pelo menos 1 solução para cada node
            for(int l=0;l<v3.size();l++){
                for(int m=0;m<v3.get(l).size();m++){
                    if(v3.get(l).get(m).contains(0)){
                        sol=false;
                        l=v3.size();
                        break;
                    }
                }
            }
            
            
            
            if(sol){// se existe pelo menos uma solucao entao analisa a solucao para os vizinhos
                use.combination(v3);
                
                 
                if(v3.size()>0)
                for (int i = 0; i < v3.get(0).size(); i++) {// gera a solucao atual com base na combinacao
                    
                    for(int k=0;k<unNeigh.size();k++)// inicializa as rotas para cada node
                            use.initNodeRoutes(unNeigh.get(k));
                
                    // aplica a j-esima solucao no nivel (posiciona e rotea)
                    for (int j = 0; j < v3.get(0).get(i).size(); j++) {// gera a solucao atual com base na combinacao
                        for (int l = 0; l < v3.get(0).get(i).get(j); l++) {// l usado para repeticao de nextSolution
                            use.nextSolution(unNeigh.get(j));
                        }
                    }
                        
                    for(int k=0;k<unNeigh.size();k++)
                      use.placeAndRouteNode(unNeigh.get(k));
                    
                    for(int k=0;k<unNeigh.size();k++)
                        this.placeAndRouteNeighbors(unNeigh.get(k));
                   
                    if(use.getMig().migIsPlaced()){
                            
                          
                            
                            System.out.printf("----------------------------------solution %d-------------------------------------------------------------\n",solutions.size()+1);
                           // System.out.printf("total area:%s \n",use.getMig().totalArea());
                            use.getMig().printRoutes();
                            //this.printQCARoute(mig);
                            //this.printPlaceMap(mig);
                            use.print();
                            solutions.add(new MajorityGraph(use.getMig()));
                            System.out.printf("----------------------------------------------------------------------------------------------------------\n",solutions.size()+1);
                       
                           
                    }
                        
                    for(int k=0;k<unNeigh.size();k++)
                        use.unplaceAndUnrouteNode(unNeigh.get(k));
                        
                }
            }
        }
    }

    
     */
    
    boolean readConfig() {
        try {
	    Scanner scan;
	    if(config==""){
	      scan = new Scanner(new File(mig.getName()+".cfg"));
	      config=mig.getName();
            }else{
	      scan = new Scanner(new File(config));
	      config=mig.getName().replace(".cfg","");
            }
            String aux;
            while(scan.hasNextLine()){
                aux=scan.nextLine();
                if(aux.contains("<levelWeight>")){
                    aux=scan.nextLine();
                    use.getMig().getLevelWeight().clear();
                    while(scan.hasNextLine() && !aux.contains("</levelWeight>")){
                        for(int i=0;i<aux.split(" ").length;i++){
                            use.getMig().getLevelWeight().add(i, Integer.parseInt(aux.split(" ")[i]));
                        }
                        aux=scan.nextLine();
                    }
                }
                if(aux.contains("<partition>")){
                    aux=scan.nextLine();
                    partitions.clear();
                    while(scan.hasNextLine() && !aux.contains("</partition>")){
                        this.partitions.add(aux);
                        aux=scan.nextLine();
                    }
                }
                if(aux.contains("<maxSolutions>")){
                    aux=scan.nextLine();
                    while(scan.hasNextLine() && !aux.contains("</maxSolutions>")){
                        numSolution.clear();
                        for(int i=0;i<aux.split(" ").length;i++){
                            numSolution.add(i, Integer.parseInt(aux.split(" ")[i]));
                        }
                        aux=scan.nextLine();
                    }
                }
                if(aux.contains("<maxPossibilities>")){
                    aux=scan.nextLine();
                    maxPossibilitie.clear();
                    while(scan.hasNextLine() && !aux.contains("</maxPossibilities>")){
                        for(int i=0;i<aux.split(" ").length;i++){
                            maxPossibilitie.add(i, Integer.parseInt(aux.split(" ")[i]));
                        }
                        aux=scan.nextLine();
                    }
                }
            }
            
        } catch (Exception e) {
            // System.out.println(e);
            return false;
        }

        return true;
    }

    
    
}