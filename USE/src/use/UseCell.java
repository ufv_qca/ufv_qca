/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package use;

import java.util.Vector;
import majority.MajorityNode;

/**
 *
 * @author geraldo
 */
public class UseCell {
    private int clockZone;
    private int relativeLevel;
    private int relativeDistance;
    private int relativeDistance2;
    private int busy;
    private MajorityNode majorityNode;
    private Vector<Point> wiresHere; // nome de todos fios passando na celula
    
    
    public UseCell() {
        this.relativeLevel=0;
        this.relativeDistance=0;
        this.relativeDistance2=0;
        this.busy=0;
        this.wiresHere = new Vector<Point>();
    }

    public int getClockZone() {
        return clockZone;
    }

    public void setClockZone(int clockZone) {
        this.clockZone = clockZone;
    }

    public int getRelativeLevel() {
        return relativeLevel;
    }

    public void setRelativeLevel(int relativeLevel) {
        this.relativeLevel = relativeLevel;
    }

    public int getRelativeDistance() {
        return relativeDistance;
    }

    public void setRelativeDistance(int relativeDistance) {
        this.relativeDistance = relativeDistance;
    }

    public int getBusy() {
        return busy;
    }

    public void setBusy(int busy) {
        this.busy = busy;
    }

    public int getRelativeDistance2() {
        return relativeDistance2;
    }

    public void setRelativeDistance2(int relativeDistance2) {
        this.relativeDistance2 = relativeDistance2;
    }

    public MajorityNode getMajorityNode() {
        return majorityNode;
    }

    public void setMajorityNode(MajorityNode majorityNode) {
        this.majorityNode = majorityNode;
    }

    public Vector<Point> getWiresHere() {
        return wiresHere;
    }

    public void setWiresHere(Vector<Point> wires) {
        this.wiresHere = wires;
    }

    public boolean isWireHere(Point p){
        
        boolean b=false;
        for(int i=0;i<this.wiresHere.size();i++)
            if(p.equal(this.wiresHere.get(i)))
                b=true;
        return b;
    }
    
    public void removeWireHere(Point p){
        for(int i=0;i<this.wiresHere.size();i++)
            if(p.equal(this.wiresHere.get(i)))
                this.wiresHere.remove(i);
    }
    
    
    
}
