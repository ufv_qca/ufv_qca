/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package verilogGraph;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;
import majority.*;

/**
 *
 * @author geraldo
 */
public class VerilogGraph {
    private Vector<String> inputNode = new Vector<String>();
    private Vector<String> outputNode = new Vector<String>();
    private Vector<String> wireNode = new Vector<String>();
    private Vector<String> wireSubType = new Vector<String>();// para identificar and,or,inv,maj
    private Vector<String> wireNodeWithInput = new Vector<String>(); 
    private Vector<String> inversor = new Vector<String>(); 

    private static String graphName;
    
    public void init() {

        try {
            Scanner scan;
            if(graphName==""){
	      java.io.File file = FileChooser.getFile();
	      graphName=file.getPath().substring(0,file.getPath().length() - 2);
	      scan = new Scanner(file);
            }else{
	      scan = new Scanner(new java.io.File(graphName+".v"));
            }
            String word = new String();
            while (scan.hasNext()) {
                word = scan.next();
                if (word.contains("(")) {
                    while (!word.contains(")") && scan.hasNext()) {
                        word = scan.next();
                    }
                    break;
                }
            }
            while (scan.hasNext()) {
                word = scan.next();
                if (word.contains("input") && scan.hasNext()) {
                    word = scan.next();
                    inputNode.add(word.replace("(","").replace(")","").replace(",","").replace(";",""));
                    while (!word.contains(";") && scan.hasNext()) {
                        word = scan.next();
                        if(word.replace("(","").replace(")","").replace(",","").replace(";","").length()>0)
                            inputNode.add(word.replace("(","").replace(")","").replace(",","").replace(";",""));
                    }
                    break;
                }
            }
            while (scan.hasNext()) {
                word = scan.next();
                if (word.contains("output") && scan.hasNext()) {
                    word = scan.next();
                    outputNode.add(word.replace("(","").replace(")","").replace(",","").replace(";",""));
                    while (!word.contains(";") && scan.hasNext()) {
                        word = scan.next();
                        if(word.replace("(","").replace(")","").replace(",","").replace(";","").length()>0)
                            outputNode.add(word.replace("(","").replace(")","").replace(",","").replace(";",""));
                    }
                    break;
                }
            }
            while (scan.hasNext()) {
                word = scan.next();
                if (word.contains("wire") && scan.hasNext()) {
                    word = scan.next();
                    wireNode.add(word.replace("(","").replace(")","").replace(",","").replace(";",""));
                    while (!word.contains(";") && scan.hasNext()) {
                        word = scan.next();
                        if(word.replace("(","").replace(")","").replace(",","").replace(";","").length()>0)
                            wireNode.add(word.replace("(","").replace(")","").replace(",","").replace(";",""));
                    }
                    break;
                }
            }
            while (scan.hasNextLine()) {
                word = scan.nextLine();
                    while (word.contains("assign") && scan.hasNextLine()) {
                        wireSubType.add(word.substring(word.indexOf("assign ")+7,word.indexOf(" ", word.indexOf("assign ")+7)));
                        if(word.contains("~")){
                            for(int i=0; i < word.substring(word.indexOf("=")+1,word.length()).replace("(","").replace(")","").replace(";","").replace("&","").replace("|","").split(" ").length;i++){
                                if(word.substring(word.indexOf("=")+1,word.length()).replace("(","").replace(")","").replace(";","").replace("&","").replace("|","").split(" ")[i].contains("~")  && !inversor.contains(word.substring(word.indexOf("=")+1,word.length()).replace("(","").replace(")","").replace(";","").replace("&","").replace("|","").split(" ")[i]))
                                    inversor.add(word.substring(word.indexOf("=")+1,word.length()).replace("(","").replace(")","").replace(";","").replace("&","").replace("|","").split(" ")[i]);
                            }
                        }
                        if(word.contains("&") && !word.contains("|")){
                            wireSubType.add("and");
                        }else if(!word.contains("&") && word.contains("|")){
                            wireSubType.add("or");
                        }else{
                            wireSubType.add("maj");
                        }
                        
                        
                        
                        wireNodeWithInput.add(word.substring(0,word.indexOf(";")).replace("assign", "").replace("=", "").replace("~", "INV").replace("&", "").replace("|", "").replace("(", "").replace(")", ""));
                        word = scan.nextLine();
                    }
            }
             
            
            
            for(int i=0;i<inversor.size();i++){
                wireNode.add(inversor.get(i).replace("~","INV"));
                wireSubType.add(inversor.get(i).replace("~","INV"));
                wireSubType.add("inv");
                wireNodeWithInput.add(inversor.get(i).replace("~","INV")+" "+inversor.get(i).replace("~", ""));
                
            }
       
            Vector<String> vec;
            for(int i=0;i<wireNodeWithInput.size();i++){
                vec = new Vector<String>();
                for(int j=0;j<wireNodeWithInput.get(i).split(" ").length;j++){
                    if(!vec.contains(wireNodeWithInput.get(i).split(" ")[j]) && wireNodeWithInput.get(i).split(" ")[j].length()>0)
                        vec.add(wireNodeWithInput.get(i).split(" ")[j]);
                }
                wireNodeWithInput.set(i, vec.toString().replace(",", "").replace("[", "").replace("]", ""));
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void initMajority(MajorityGraph mig) {
        
        Vector<MajorityNode> nodes = new Vector<MajorityNode>();
        
        
        for(int i=0;i<inputNode.size();i++){
            if(wireSubType.indexOf(inputNode.get(i))!=-1)
                nodes.add(new MajorityNode(inputNode.get(i),"input",wireSubType.get(wireSubType.indexOf(inputNode.get(i))+1)));
            else
                nodes.add(new MajorityNode(inputNode.get(i),"input"));
        }
        
        for(int i=0;i<outputNode.size();i++){
            if(wireSubType.indexOf(outputNode.get(i))!=-1)
                nodes.add(new MajorityNode(outputNode.get(i),"output",wireSubType.get(wireSubType.indexOf(outputNode.get(i))+1)));
            else
                nodes.add(new MajorityNode(outputNode.get(i),"output"));
            
        }
        
        for(int i=0;i<wireNode.size();i++){
            if(wireSubType.indexOf(wireNode.get(i))!=-1)
                nodes.add(new MajorityNode(wireNode.get(i),"gate",wireSubType.get(wireSubType.indexOf(wireNode.get(i))+1)));
            else
                nodes.add(new MajorityNode(wireNode.get(i),"gate"));
        }
        
        
        // adiciona todos inputs de todos nodes
        for (int i = 0; i < wireNodeWithInput.size(); i++) {
            for (int j = 0; j < nodes.size(); j++) {
                if(wireNodeWithInput.get(i).split(" ")[0].equals(nodes.get(j).getName())){
                    for(int k=1;k<wireNodeWithInput.get(i).split(" ").length;k++){
                        nodes.get(j).addInput(wireNodeWithInput.get(i).split(" ")[k]);
                    }
                }
            }
        }
        // adiciona todos outputs de todos nodes
        for (int i = 0; i < nodes.size(); i++) {
            for (int j = 0; j < nodes.get(i).getInput().size(); j++) {
                for (int k = 0; k < nodes.size(); k++) {
                    if (nodes.get(i).getInput().get(j).equals(nodes.get(k).getName())) {
                        nodes.get(k).addOutput(nodes.get(i).getName());
                    }
                }
            }
        }
        
        for (int i = 0; i < nodes.size(); i++) {
            mig.addNode(nodes.get(i));
        }
        
        
    }
    
        
   public void exportDOT(MajorityGraph mig) {
                  
        try{
        String dot = "digraph {\n";
        for(int i=0;i<mig.getMajorityGraph().size();i++){
            if(mig.getMajorityGraph().get(i).getType().equals("input"))
                dot+="  "+mig.getMajorityGraph().get(i).getName()+" "+"[shape=invhouse, color=red]\n";
            if(mig.getMajorityGraph().get(i).getType().equals("output"))
                dot+="  "+mig.getMajorityGraph().get(i).getName()+" "+"[shape=house, color=blue]\n";
        }
        
        for(int i=0;i<mig.getMajorityGraph().size();i++)
            for(int j=0;j<mig.getMajorityGraph().get(i).getOutput().size();j++){
                if(mig.getNode(mig.getMajorityGraph().get(i).getOutput().get(j))!=null)
                    dot=dot+("      "+mig.getMajorityGraph().get(i).getName()+" -> "+mig.getMajorityGraph().get(i).getOutput().get(j)+";\n");
            }
       
        for(int i=1;i<=mig.getMaxLevel();i++){
            dot+="  { rank=same; ";
            for(int j=0;j<mig.nodesInLevel(i).size();j++){
                dot+= mig.nodesInLevel(i).get(j)+" ";
            }
            dot+="}\n";
        }
        
        dot+="}\n";    
        
        java.io.FileWriter fs = new java.io.FileWriter(graphName+".dot");
        fs.write(dot);
        fs.close();
        
        /*
        javax.swing.JFileChooser fileChooser = new javax.swing.JFileChooser();
        fileChooser.setFileFilter(new javax.swing.filechooser.FileNameExtensionFilter("DOT file", "dot", "dot"));
        
            if (fileChooser.showSaveDialog(fileChooser) == javax.swing.JFileChooser.APPROVE_OPTION) {
                java.io.File file = fileChooser.getSelectedFile();
                
                if(fileg.getPath().contains(".dot")){
                    java.io.FileWriter fs = new java.io.FileWriter(fileg.getPath());
                    fs.write(dot);
                    fs.close();
                }else{
                    java.io.FileWriter fs = new java.io.FileWriter(fileg.getPath()+".dot");
                    fs.write(dot);
                    fs.close();
                }
            }*/
        }catch(Exception ex){System.out.println(ex);}
    }
   
    public void exportGSpan(MajorityGraph mig) {
                  
        try{
        String gsp = "t # 0\n";
        for(int i=0;i<mig.getMajorityGraph().size();i++)
            gsp+="v "+i+" "+mig.getMajorityGraph().get(i).getName()+"\n";
        for(int i=0;i<mig.getMajorityGraph().size();i++)
            for(int j=0;j<mig.getMajorityGraph().get(i).getOutput().size();j++){
                gsp+="e "+ i+" ";
                for(int k=0;k<mig.getMajorityGraph().size(); k++)
                    if(mig.getMajorityGraph().get(k).getName().equals(mig.getMajorityGraph().get(i).getOutput().get(j)))
                        gsp+=k+" 0\n";
            }
        
        java.io.FileWriter fs = new java.io.FileWriter(graphName+".gsp");
        fs.write(gsp);
        fs.close();
        
        }catch(Exception ex){System.out.println(ex);}
    }
    

    
    public void init( MajorityGraph mig, String f) {
        graphName="";
        if(f!=""){
            graphName=f.substring(0,f.length() - 2);
        }
        VerilogGraph vg = new VerilogGraph();
        vg.init();
        vg.initMajority(mig);
        mig.setName(graphName);
        
        //mig.initLevel();
        mig.setLevel();
        
        //vg.exportDOT(mig);
        //vg.exportGSpan(mig);
    }

    public void makeDot(MajorityGraph mig, String name) {
        
        
        VerilogGraph vg = new VerilogGraph();
        graphName=name;
        vg.exportDOT(mig);
    }


}