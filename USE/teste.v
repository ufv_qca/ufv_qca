module top( x, y, z);
input x, y, z;
output A;
wire B, C;

assign B = ( x & y ) | ( y & z ) | ( x & z );
assign C = ( x & y ) | ( y & z ) | ( x & z );
assign A = ( x & B ) | ( x & C ) | ( B & C );



endmodule